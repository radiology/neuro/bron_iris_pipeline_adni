function [outcome]=do_classification(varargin)

% do_classification('data', 'features', <other options>)
%
% Data: 
%   'cbf'       - cerebral blood flow intensities
%   'gm'        - gray matter volumes
%   'wm_fa'     - fractional anisotropy in the wm
%   'gm_md'     - mean diffusivity in the gm
%   'wm_md'     - mean diffusivity in the wm
%   'concat'    - concatenation of cbf and gm features ([cbf gm]) 
%   'product'   - product of cbf and gm output probabilities (run 'cbf' and
%                 'gm' first
%   'mean'      - mean of cbf and gm output probabilities (run 'cbf' and
%                 'gm' first
%   'multipl'   - multiplication of the voxelwise features (cbf-modulated
%                 modulated gm map)
%   'svm'       - train an svm on cbf and gm output probabilities (run 
%                 'cbf' and 'gm' first
%   'age'       - classification on subject age (Featurs should be 'age' 
%                 too)
%   'scatter'   - make a scatter plot of cbf and gm output probabilities 
%                 run 'cbf' and 'gm' first
%
% Features: 
%   'vbm'       - voxel-wise features using modulated GM segmentations
%   'dbm'       - not used anymore, voxel-wise features using Jacobian of
%                 deformation field in GM or mask   
%   'region'    - features of 72 regions
%   'selection' - features of 28 regions known to be related to dementia
%   'lobe'      - features of 10 brain lobes
%   'hemisphere'- features of 2 hemisphere
%   'brain'     - one feature for the entire brain
%   'tbss'      - TBSS skeleton for feature extraction of FA and MD
%   'age'       - classification on subject age (Data should be 'age' too)
%
% Other options:
% data1: When a combination method (concat, product, mean, multipl,
% svm) is used, two or more types of data for combination should  be specified,
% default data1='gm,cbf'; options gm, cbf, wm_fa, gm_md, wm_md
% Note: the format is 'gm,cbf', no space after the comma!
%
% groups: For the ADNI data (study=Cuingnet_data) different groups can be
% compared. These are: AD, CN, MCIc and MCInc. Default='AD,CN', no space 
% after the comma!
%
% use_norm: Normalize CBF maps with mean CBF (default=0)
%
% use_old_subjects: Use subject set for MICCAI / ISMRM 2012 (20 patients, 
% 12 controls) (default=0)
%
% use_iris_subjects: Use only IRIS-study data
%
% use_kernel: Calculate kernel (inproduct) once, is faster for voxel-wise, 
% not possible when using feature selection (default=1)
%
% study: where to find data (default=IRIS)
%
% probability_est: which method for svm output probabilities 0:PRtools,
% 1:Libsvm (defaul=0)
%
% use_auc: Optimize C-parameter based on area under the ROC curve (AUC)
% instead of accuracy (default=1)
%
% use_hard_segmentation: Use hard GM segmentation instead of probabilistic 
% segmentation for calculating voxel-and ROI-wise features (default=0)
%
% use_mean_probability: Use mean probability of GM in a ROI to calculated
% ROI-wise features, instead of sum of GM tissue in a ROI to obtain the GM 
% volume (default=0) 
%
% use_random: Use random selection of folds for optimization of c-parameter
%
% confounder: Correct for confounders (age, sex, ICV)
%
%
% Example use:
% do_classification('cbf', 'region', 'use_forward_selection', '1',
% 'selection_method', 'maha')
%

%% Input Parser
p = inputParser;   % Create an instance of the class.

% Arguments
p.addOptional('data', '', @ischar);
p.addOptional('features', '', @ischar);

% Parameters
p.addParamValue( 'data1', 'gm,cbf', @ischar);
p.addParamValue( 'groups', 'AD,CN', @ischar);
p.addParamValue( 'use_norm', '0', @ischar);                                 
p.addParamValue( 'use_old_subjects', '0', @ischar); 
p.addParamValue( 'use_dti_subjects', '0', @ischar);
%%% by Sandrine %%%
p.addParamValue( 'use_iris_subjects', '0', @ischar);
%%% ----------- %%%
p.addParamValue( 'use_forward_selection', '0', @ischar);                    
p.addParamValue( 'selection_method', 'none', @ischar);                      
p.addParamValue( 'use_kernel', '1', @ischar);                              
p.addParamValue( 'study', 'IRIS', @ischar);                                 
p.addParamValue( 'probability_est', '0', @ischar);
p.addParamValue( 'use_auc', '1', @ischar);
p.addParamValue( 'pmap_analytic', '0', @ischar);
p.addParamValue( 'pmap_experimental', '0', @ischar);
p.addParamValue( 'ttest_randomized', '0', @ischar);
p.addParamValue( 'groupwise_ttest', '0', @ischar);
p.addParamValue( 'use_hard_segmentation', '0', @ischar);
p.addParamValue( 'use_mean_probability', '0', @ischar);
p.addParamValue( 'nfold', '0', @ischar);
p.addParamValue( 'max_iterations', '1', @ischar); %for randomized cross validation % default was 50
p.addParamValue( 'num_max_perms', '5000', @ischar);
p.addParamValue( 'use_random','0', @ischar );
p.addParamValue( 'nr_iterations','1', @ischar); %for c-value estimation
p.addParamValue( 'robustc','0', @ischar );
p.addParamValue( 'nfold_c', '5', @ischar);
p.addParamValue( 'histreg', '0', @ischar);
p.addParamValue( 'remove_single_voxels', '0', @ischar);
p.addParamValue( 'elastix_version', '4.5', @ischar);
p.addParamValue( 'no_mask', '0', @ischar);
p.addParamValue( 'uncertainty', '0', @ischar);
p.addParamValue( 'make_curve', '1', @ischar);
p.addParamValue( 'silent', '0', @ischar);
p.addParamValue( 'resample', '0', @ischar);
p.addParamValue( 'cmin','-5', @ischar);
p.addParamValue( 'fixed_c_perm', '1', @ischar);
p.addParamValue( 'sign_level', '0.05', @ischar);
p.addParamValue( 'no_exit', '0', @ischar);
p.addParamValue( 'cvalue', '', @ischar);
p.addParamValue( 'noicv', '0', @ischar);
p.addParamValue( 'confounder', '0', @ischar);
p.addParamValue( 'trainset', '0', @ischar);
p.addParamValue( 'ls_svc', '0', @ischar);

% Parse input
p.parse( varargin{:} );
clear varargin;

tic

% Extract settings from input
data   = p.Results.data;
features = p.Results.features;
data1   = str2cell(p.Results.data1);
groups   = str2cell(p.Results.groups);
use_norm = str2num(p.Results.use_norm);
use_old_subjects = str2num(p.Results.use_old_subjects);
%%% by Sandrine %%%
use_iris_subjects = str2num(p.Results.use_iris_subjects);
%%% ----------- %%%
use_dti_subjects = str2num(p.Results.use_dti_subjects);
use_forward_selection = str2num(p.Results.use_forward_selection);
selection_method = p.Results.selection_method;
use_kernel = str2num(p.Results.use_kernel);
study = p.Results.study;
probability_est = str2num(p.Results.probability_est);
use_auc = str2num(p.Results.use_auc);
pmap_analytic = str2num(p.Results.pmap_analytic);
pmap_experimental = str2num(p.Results.pmap_experimental);
ttest_randomized = str2num(p.Results.ttest_randomized);
groupwise_ttest = str2num(p.Results.groupwise_ttest);
use_hard_segmentation = str2num(p.Results.use_hard_segmentation);
use_mean_probability = str2num(p.Results.use_mean_probability);
nfold = str2num(p.Results.nfold);
max_iterations = str2num(p.Results.max_iterations);
num_max_perms=str2num(p.Results.num_max_perms);
use_random        = str2num(p.Results.use_random);
nr_iterations     = str2num(p.Results.nr_iterations);
robustc           = str2num(p.Results.robustc);
nfold_c           = str2num(p.Results.nfold_c);
histreg           = str2num(p.Results.histreg);
remove_single_voxels=str2num(p.Results.remove_single_voxels);
elastix_version=p.Results.elastix_version;
no_mask=str2num(p.Results.no_mask);
uncertainty=str2num(p.Results.uncertainty);
make_curve        = str2num(p.Results.make_curve);
silent            = str2num(p.Results.silent);
resample          = str2num(p.Results.resample);
cmin              = str2num(p.Results.cmin);
fixed_c_perm      = str2num(p.Results.fixed_c_perm);
sign_level        = str2num(p.Results.sign_level);
no_exit           = str2num(p.Results.no_exit);
cvalue            = p.Results.cvalue;
confounder         = str2num(p.Results.confounder);
trainset         = str2num(p.Results.trainset);
noicv             = str2num(p.Results.noicv);
ls_svc           = str2num(p.Results.ls_svc);
clear p;

%% Set directories
if exist('D:')==7 % On PC
    cluster=0;
    data_drive='D:';
    output_directory= fullfile('X:','Project','Features_paper');
    tbss_data=fullfile('V:','home','slacomme','tbss','stats');
    tmap_directory=fullfile('V:','tmap_temp');
else % Or cluster
    cluster=1;
    data_drive=fullfile('/scratch','ebron');
    tbss_data=fullfile('~','home', 'slacomme', 'tbss','stats');
    output_directory=fullfile('~','Project', 'Features_paper');
    tmap_directory=fullfile(data_drive,'tmap_temp');
end 

data_directory=['Features' filesep 'atlas_work_temp'];

%% Set output names according to specified options
name =[data ' - ' features ' (' features(1) '^{' data '})'];
if use_norm
    name=[name '_norm'];
end    
if use_old_subjects
    name=[name '_oldsubjects'];
end
%%% by Sandrine %%%
if use_iris_subjects
    name=[name '_IRISsubjects'];
end
%%% ----------- %%%
if use_dti_subjects
    name=[name '_DTIsubjects'];
end
if ismember(features,{'tbss'})
    use_dti_subjects=1;
end
% if use_forward_selection
%     use_kernel=0
% end
if probability_est
    name=[name '_LibsvmProbEst'];
end
if use_auc
    name=[name '_auc'];
end
if ismember(study,{'IRIS'}) 
    study1 = '';
else
    name = [name study];
end
if ~(length(data1)==2 && sum(ismember(data1,{'gm'})) && sum(ismember(data1,{'cbf'})))
    for l=1:length(data1)
        name = [name '_' data1{l}];
    end
end 
if ~(length(groups)==2 && sum(ismember(groups,{'AD'})) && sum(ismember(groups,{'CN'})))
    for l=1:length(groups)
        name = [name '_' groups{l}];
    end
end
if ~use_hard_segmentation
    if use_mean_probability
        name=[name '_prob'];
    else
        name=[name '_pv'];
    end
end
if nfold
    k=nfold;
    name=[name '_' num2str(k) '-fold'];
end
if num_max_perms~=5000
    name=[name '_' num2str(num_max_perms)];
end
if use_random
    name = [name '_random']
end
if nr_iterations~=1
    name = [name '_' num2str(nr_iterations) 'cviterations']
end
if robustc
    use_random=1;
    nr_iterations=3;
    cmin=-10;
    name = [name '_robustc']
end
if nfold_c==0
    name = [name '_cloo'];
end
if histreg
    name = [name '_histreg'];    
end
if remove_single_voxels
    name = [name '_removesinglevoxels']
end
if ismember(elastix_version,{'performance'})
    name = [name '_elastixperf']
    data_directory = [data_directory '_performance']
elseif ismember(elastix_version,{'4.6'})
    name = [name '_elastix46']
    data_directory = [data_directory '_4_6']
end
if no_mask
    name = [name '_nomask']
    data_directory = [data_directory '_nomask']
end
if uncertainty
    name = [name '_uncertainty' num2str(uncertainty)]
    nradded=uncertainty;
end
if ~fixed_c_perm
    name = [name '_permc']
end
if sign_level~=0.05
    name = [name '_p' num2str(sign_level)]
end
    
if ismember(data,{'wm_fa','wm_md'})
    dti=1;
else
    dti=0;
end

res='';
if resample<10 && resample>0
    res=['_' num2str(resample) 'mm'];
elseif resample==99
    res='_gauss';
elseif resample==98
    res='_gauss_2';
end
name = [name res]

if ~isempty(cvalue)
    cvalue=str2num(cvalue);
end

if noicv
    name = [name '_noicv']
end
if confounder
    name = [name '_confounder-age-sex']
    if confounder>1
        name = [name '-icv']
    end
    noicv=1;
end

if ls_svc
    name = [name '_ls']
end

if trainset
    name=[name 'train']
end
    
if ismember(study,{'process_challenge'})
    study='Cuingnet_data';
    test_data='process_challenge';
    data_directory=['Features' filesep 'atlas_work_temp_nomask'];
else
    test_data='';
end
display(['Run classification for ' name])

%% Load data
switch data
    case {'gm', 'wm'}    
        style={'b-  '};    
        f=features;        
        if ismember(features,{'dbm'}) 
            f='voxel';
            filename = fullfile(data_drive, study, data_directory, 'jacobian'); %voxel
        elseif ismember(features,{'vbm'}) 
            f='voxel';
            if use_hard_segmentation
                filename = fullfile(data_drive, study, data_directory, [data '_modulated']); %voxel            
            else
                filename = fullfile(data_drive, study, data_directory, [data '_prob_modulated' res]); %voxel
            end
        else 
            if ismember(features,{'selection'})
                f='region'
            end
            filename = getregionfilenames(data_drive, study, data_directory, f, data, use_hard_segmentation, use_mean_probability);
        end        
        
    case {'cbf','wm_fa','gm_md','wm_md'}
        style={'r-  '};
        f=features;
        if ismember(features,{'dbm','vbm'}) 
            f='voxel';
            if ismember(data,{'cbf'})
                data1=data;
            else 
                data1=['dti' upper(data(length(data)-2:length(data)))];
            end
            filename = fullfile(data_drive, study, data_directory, [data1 res]); %voxel
        elseif ismember(features, {'tbss'})
            f='tbss';
            data1=[upper(data(length(data)-2:length(data)))];
            filename = fullfile(tbss_data, ['Nifti' data1]);
        else   
            if ismember(features,{'selection'})
                f='region'
            end            
            filename = getregionfilenames(data_drive, study, data_directory, f, data, use_hard_segmentation, use_mean_probability);
        end
    case 'concat'
        style={'g-  '};
        f=features;
         if ismember(features,{'dbm'}) 
            f='voxel';
            %voxel
            filename=cell(length(data1),1);
            for l=1:length(data1)
                if ismember(data1{l},{'gm'})
                    type='jacobian';
                elseif ismember(data1{l},{'cbf'})
                    type='cbf';
                else 
                    type=['dti' upper(data1{l}(length(data1{l})-2:length(data1{l})))];
                end
                filename{l}=fullfile(data_drive, study, data_directory, type);
            end
            
%             filename_gm = fullfile(data_drive, study, data_directory, 'jacobian');          
%             filename_cbf = fullfile(data_drive, study, data_directory, 'cbf');
         elseif ismember(features,{'vbm'}) 
            f='voxel';
            %voxel
            filename=cell(length(data1),1);
            for l=1:length(data1)
                if ismember(data1{l},{'gm','wm'})
                    if use_hard_segmentation
                        type = [data1{l} '_modulated'];          
                    else
                        type = [data1{l} '_prob_modulated']; 
                    end 
                elseif ismember(data1{l},{'cbf'})
                    type='cbf';
                else 
                    type=['dti' upper(data1{l}(length(data1{l})-2:length(data1{l})))];
                end
                filename{l}=fullfile(data_drive, study, data_directory, type);
            end
            
         
%             filename_cbf = fullfile(data_drive, study, data_directory, 'cbf');            
         else
            if ismember(features,{'selection'})
                f='region'
            end        
            filename=cell(length(data1),1);
            for l=1:length(data1)
                filename{l}=getregionfilenames(data_drive, study, data_directory, f, data1{l}, use_hard_segmentation, use_mean_probability);
            end
%             filename_gm = getregionfilenames(data_drive, study, data_directory, f, data1, use_hard_segmentation, use_mean_probability);
%             filename_cbf = getregionfilenames(data_drive, study, data_directory, f, data2, use_hard_segmentation, use_mean_probability);           
         end   
    case 'multipl'
        style={'g-  '};  
        f=features;
         if ismember(features,{'vbm'}) 
            f='voxel';
            filename = fullfile(data_drive, study, data_directory, 'cbf_modulated_gm_prob'); %voxel
      
         else
            if ismember(features,{'selection'})
                f='region'
            end 
            filename=cell(length(data1),1);
            for l=1:length(data1)
                filename{l}=getregionfilenames(data_drive, study, data_directory, f, data1{l}, use_hard_segmentation, use_mean_probability);
            end
%             filename_gm = getregionfilenames(data_drive, study, data_directory, f, data1, use_hard_segmentation, use_mean_probability);
%             filename_cbf = getregionfilenames(data_drive, study, data_directory, f, data2, use_hard_segmentation, use_mean_probability);      
         end 
        
    case {'product','mean'}
        style={'p-  '};  
        if nfold
            aucs=zeros(50,1);
            for nr_iteration=1:50
                filename1 = fullfile(output_directory, [strrep(name, data, data1{1}) '_kernel_C'], ['outcome' num2str(nr_iteration) '.txt']);
                filename2 = fullfile(output_directory, [strrep(name, data, data1{2}) '_kernel_C'], ['outcome' num2str(nr_iteration) '.txt']);
                auc = productrule_fourfold(name, filename1, filename2, 1,data);
                aucs(nr_iteration)=auc;
            end
                
            base=data_directory; 
            
            result_file=fullfile(base, '4fold_iteration.txt');
            if ~exist(result_file,'file')
                fid=fopen(result_file,'w');
                fprintf(fid, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n','Name', 'Y', 'M', 'D', 'h', 'm', 's', 'Mean AUC', 'Std AUC', 'Min AUC', 'Max AUC');
                fclose(fid);
            end

            fid=fopen(result_file,'a');
            fprintf(fid, '%s\t%i\t%i\t%i\t%i\t%i\t%i\t%f\t%f\t%f\t%f\n',name, fix(clock),mean(aucs), std(aucs), min(aucs), max(aucs));
            fclose(fid);

            result={'Name', 'Mean AUC', 'Std AUC', 'Min AUC', 'Max AUC'; name, mean(aucs), std(aucs), min(aucs), max(aucs)}'
            
        else
            filename=cell(length(data1),1);
            for l=1:length(data1)
                filename{l} = fullfile(output_directory, [strrep(name, data, data1{l}) '_kernel'],'outcome.txt')
            end
%             filename2 = fullfile(output_directory, [strrep(name, data, data2) '_kernel'],'outcome.txt');    
            productrule(name, filename, data, 'data1', data1, 'use_dti_subjects',use_dti_subjects)
        end
    case 'svm'
        style={'p-  '};          
        filename1 = fullfile(output_directory, [strrep(name, data, data1{1}) '_kernel'],'outcome.txt');
        filename2 = fullfile(output_directory, [strrep(name, data, data1{2}) '_kernel'],'outcome.txt');
        scatter_posteriors(features, filename1, filename2, labelfile, output_directory, 'data1', data1, 'data2', data2, 'use_dti_subjects',use_dti_subjects, 'do_SVM',1,'do_scatter',0)   
    case 'age'
        style={'k:  '};
        filename = fullfile(data_drive, study, data_directory,'age.txt');
    case 'scatter'                
        filename1 = fullfile(output_directory, [strrep(name, data, data1{1}) '_kernel'],'outcome.txt');
        filename2 = fullfile(output_directory, [strrep(name, data, data1{2}) '_kernel'],'outcome.txt');
        scatter_posteriors(features, filename1, filename2, labelfile, output_directory, 'data1', data1{1}, 'data2', data1{2}, 'use_dti_subjects',use_dti_subjects)        
end

%load features
mask='';
display('Load features')
[a,train_index, label, info, mask, subject_label, confounders]=load_features(features,data, data_drive, study, filename, use_old_subjects, use_dti_subjects, dti, groups, data_directory, res, selection_method, use_forward_selection, data1, nfold_c, robustc, resample, use_hard_segmentation, study,'', noicv, confounder, trainset); 

if ~isempty(test_data)    
    train_index=(1:numel(label))';
    filename_test_data=strrep(filename,study,test_data);
    display('Load process_challenge train samples')
    [a_test,train_index_test, label_test, info_test, mask_test,  subject_label, confounders_test]=load_features(features,data, data_drive, test_data, filename_test_data, use_old_subjects, use_dti_subjects, dti, groups, data_directory, res, selection_method, use_forward_selection, data1, nfold_c, robustc, resample, use_hard_segmentation, study,'', noicv, confounder, trainset); 
    a=[a; a_test];
    confounders=[confounders; confounders_test];
    train_index_test=train_index_test+numel(label);
    train_index=[train_index; train_index_test];
    label=[label; label_test];
    if ismember(features,{'dbm','vbm'})
        display('Load process_challenge test samples')
        [a_test,train_index_test, label_test, info, mask_test,  subject_label, confounders_test]=load_features(features,data, data_drive, test_data, filename_test_data, use_old_subjects, use_dti_subjects, dti, groups, data_directory, res, selection_method, use_forward_selection, data1, nfold_c, robustc, resample, use_hard_segmentation, study,a, noicv, confounder, trainset);   
        if size(a,2)==size(a_test,2) %If previous step did not calculate kernel already
            a=[a; a_test];
            confounders=[confounders; confounders_test];
            [a,mu,sigma]=zscore(a);
        else % if a_test is complete kernel
            display('Test has kernel form')
            a=a_test;
            confounders=confounders_test;
            confounders=zscore(confounders);
            use_kernel=0;
            
            if confounder
                display('Correct for confounders')
                a=confounder_correction_by_kernel_regression(a,confounders);
            end
        end
        train_index_test=train_index_test+numel(label);
        train_index=[train_index; train_index_test];
        label=[label; label_test];
    end
    
    clear a_test, clear train_index_test; clear label_test, clear info_test, clear mask_test
else
    [a,mu,sigma]=zscore(a);
    subject_label(train_index,:)=[];
end
confounders=zscore(confounders);

%% Run classification
outcome=[];
if ismember(data,{'gm','wm','cbf','wm_md','gm_md','wm_fa','multipl','concat','age'})
    
    
    if uncertainty
        a=add_uncertainty_classification(a,name,label,train_index,nradded, mu, sigma,output_directory,'study', study,'use_kernel',use_kernel,'use_forward_selection',use_forward_selection, 'selection_method', selection_method, 'probability_est', probability_est, 'use_auc', use_auc);
        clear mu; clear sigma;
    else        
        
    %     a_norm=(a-mean(a(:)))/ std(a(:));
%         A=dataset(double(a),label)
        A_norm=dataset(double(a),label)
        clear a;clear mu; clear sigma;

        if nfold && ~ismember(study,{'Cuingnet_data'})
            outcome=kfold_crossvalidation(A_norm,k, name, style, output_directory, 'use_kernel',use_kernel,'use_forward_selection',use_forward_selection, 'selection_method', selection_method, 'probability_est', probability_est, 'use_auc', use_auc, 'max_iterations', max_iterations, 'svm_parameters', {cvalue});
        else    
            if size(train_index,1)==0 && resampletraintest==0
                % Using LOO crossvalidation
                [outcome]=crossvalidation(A_norm,0, name, style, output_directory, info, 'use_kernel',use_kernel,'use_forward_selection',use_forward_selection, 'selection_method', selection_method, 'probability_est', probability_est, 'use_auc', use_auc, 'num_max_perms',num_max_perms, 'nfold_c', nfold_c, 'use_random', use_random, 'nr_iterations', nr_iterations,'histreg',histreg,'mask', mask, 'remove_single_voxels',remove_single_voxels, 'make_curve', make_curve,'silent', silent, 'cmin', cmin, 'fixed_c_perm', fixed_c_perm, 'sign_level',sign_level,'svm_parameters', {cvalue}, 'ls_svc', ls_svc);

        %     a_norm_cbf=zscore(a_cbf);
        %     a_norm_gm=zscore(a_gm);
        %     A_cbf=dataset(double(a_norm_cbf),label)
        %     A_gm=dataset(double(a_norm_gm),label)
        %         outcome=crossvalidation_featsel_cbf(A_cbf,A_gm,0, name, style, 'use_kernel',use_kernel,'use_forward_selection',use_forward_selection, 'selection_method', selection_method) %'svm_parameters', {}

            else
                % Separate training and testing set
                if nfold>1
                    train_index=nfold;
                end
                
                [outcome, name]=traintest(A_norm, train_index, name, style, output_directory, info, 'use_kernel',use_kernel,'use_forward_selection',use_forward_selection, 'selection_method', selection_method, 'probability_est', probability_est, 'use_auc', use_auc, 'num_max_perms',num_max_perms, 'max_iterations', max_iterations, 'make_curve', make_curve,'mask', mask,'silent', silent, 'cmin', cmin, 'fixed_c_perm', fixed_c_perm, 'sign_level',sign_level,'svm_parameters', {cvalue}, 'ls_svc', ls_svc, 'tmap_directory', tmap_directory, 'confounders', confounders);            
            end
        end
    end
    save_outcome_file(output_directory,name, outcome, subject_label)
end

% pmap=[pmap; pmap_i];
if cluster && ~no_exit
    display('Exit Matlab')
    exit
end
end


%% Functions
    %% Load features
function [a,train_index, label, info, mask, subjects_label, confounders]=load_features(features,data, data_drive, study, filename, use_old_subjects, use_dti_subjects, dti, groups, data_directory, res, selection_method, use_forward_selection, data1, nfold_c, robustc, resample, use_hard_segmentation, mask_study, train_vector, noicv, confounder, trainset)   
    switch features
        case {'dbm','vbm'}
            if ~ismember(data,{'product','mean','svm','scatter'})
                a=[];
                if trainset && ismember(study,{'process_challenge'})
                    labelfile = fullfile(data_drive, study, 'Features', 'labels_train.txt');
                else
                    % File with subject names
                    labelfile = fullfile(data_drive, study, 'Features', 'labels.txt');
                end

                % Applies if images are resampled or gaussian smoothed
                if resample>10
                    res=''
                end

                % Read subject image files
                subjects=tdfread(labelfile);
                [subjects.label, label, train_index, remove_list]=get_labels(subjects.label,'use_dti_subjects',use_dti_subjects,'groups', groups)                
                
                % If we do process_challenge we want to load the train set first
                if ismember(study,{'process_challenge'}) && ~ismember(study,{mask_study}) && ~use_forward_selection
                    %load only train
                    if isempty(train_vector) %TRAIN
                       subjects.label=subjects.label(train_index,:);
                       label=label(train_index,:);      
                       train_index=(1:size(label,1))';
                    elseif confounder % TEST, but we cannot calculate kernel now, because all data is needed for confounder correction
                       subjects.label(train_index,:)=[];
                       label(train_index,:)=[]; 
                       train_index=[];                                              
                    else %TEST, we calculate kernel now to have to all info on test data stored and save memory
                       subjects.label(train_index,:)=[];
                       label(train_index,:)=[]; 
                       train_index=[];
                       
                       linearKernel=@(X,Y)X*Y';
                       [train_vector,mu,sigma]=zscore(train_vector);
                       a=linearKernel(train_vector,train_vector);
                       
                       
                    end
                    
                end
                
                subjects_label=subjects.label;
                
                % Get mask names
                maskfile_vote = fullfile(data_drive, mask_study, data_directory, ['gm_vote_mask_no_cerebellum' res '.nii.gz']);
                maskfile_or = fullfile(data_drive, mask_study, data_directory, ['gm_or_mask_no_cerebellum' res '.nii.gz']);
                maskfile_brain = fullfile(data_drive, mask_study, data_directory, ['seg_no_cerebellum_brainstem' res '.nii.gz']);
                maskfile_wm_vote = fullfile(data_drive, mask_study, data_directory, ['wm_vote_mask_no_cerebellum' res '.nii.gz']);            

                % Load masks 
                mask_vote=load_nii(maskfile_vote);

                % For manual ROI selection 
                if ismember(selection_method,{'roi'})
                    atlas = fullfile(data_drive, mask_study, data_directory, 'seg_template.nii.gz');
                    atlas = manual_roi_selection(atlas, use_forward_selection);
                else
                    atlas = ones(size(mask_vote));
                end

                mask_vote.img=mask_vote.img.*atlas;

                if ismember(features,{'vbm'}) && use_hard_segmentation
                    mask_or=load_nii(maskfile_or);
                    mask_or.img=mask_or.img.*atlas;                
                elseif ismember(features,{'vbm'})
                    mask_brain=load_nii(maskfile_brain);
                    mask_brain.img=mask_brain.img.*atlas; 
                end
                if ismember(data,{'wm_md','wm_fa'})
                    mask_wm_vote=load_nii(maskfile_wm_vote);
                    mask_wm_vote.img=mask_wm_vote.img.*atlas; 
                end
                if ismember(data,{'concat'})
                    for l=1:length(data1)
                        mask_wm_vote=[];
                        if ismember(data1{l},{'wm_md','wm_fa'})
                            if isempty(mask_wm_vote)
                                mask_wm_vote=load_nii(maskfile_wm_vote);
                                mask_wm_vote.img=mask_wm_vote.img.*atlas;
                            end
                        end
                    end
                end              

                dti_var=0;
                for i=1:size(label,1)
                    subject_label=strtrim(subjects.label(i,:));              
                    if use_dti_subjects==1 && ( ismember(subject_label,{'IRIS_015'}) || dti_var)
                        dti_var=1;                    
                        subject_label=strtrim(subjects.label(i+1,:))
                    else
                        subject_label=strtrim(subjects.label(i,:))
                    end

                    if ismember(data,{'concat'}) 

                        imagefile=cell(length(data1),1);
                        image=cell(length(data1),1);
                        image_s=cell(length(data1),1);
                        values=cell(length(data1),1);
                        for l=1:length(data1)
                            imagefile{l}=fullfile(filename{l}, [subject_label '.nii.gz']);
                            if exist(imagefile{l},'file')~=2
                                imagefile{l}=fullfile(filename{l}, [subject_label '.nii']);
                            end
                            image{l}=load_nii(imagefile{l});
                            image_s{l}=image{l}.img;

                            if ismember(data1{l},{'gm'})
                                if ismember(features,{'vbm'}) && use_hard_segmentation                    
                                    values{l}=image_s{l}(find(mask_or.img==1));
                                elseif ismember(features,{'vbm'})
                                    values{l}=image_s{l}(find(mask_brain.img==1));
                                else                    
                                    values{l}=image_s{l}(find(mask_vote.img==1));
                                end
                            elseif ismember(data1{l},{'cbf','gm_md'})
                                values{l}=image_s{l}(find(mask_vote.img==1));   
                            else
                                values{l}=image_s{l}(find(mask_wm_vote.img==1)); 
                            end                                
                        end             

                    else    
                        imagefile=fullfile(filename, [subject_label '.nii.gz']);
                        if exist(imagefile,'file')~=2
                            imagefile=fullfile(filename, [subject_label '.nii']);
                        end
                        image=load_nii(imagefile);
                        image_s  = image.img;
                        if ismember(features,{'vbm'}) && ismember(data,{'gm'}) && use_hard_segmentation 
                            values=image_s(find(mask_or.img==1));
                        elseif ismember(features,{'vbm'}) && ismember(data,{'gm'})
                            values=image_s(find(mask_brain.img==1));
                        elseif ismember(data,{'cbf','gm_md', 'multipl'})                
                            values=image_s(find(mask_vote.img==1));
                        else
                            values=image_s(find(mask_wm_vote.img==1));
                        end               
                    end             
                    if noicv && ismember(data,{'concat','gm'})
                        icvfolder = fullfile(data_drive, study, data_directory, 'icv');
                        icvfile=fullfile(icvfolder, [subject_label '.txt']);
                        fid=fopen(icvfile,'r');
                        icv=fscanf(fid,'%i');
                        fclose(fid);
                        if ismember(data,{'gm'})
                            values=values./icv;
                        else
                            for l=1:length(data1)
                                if ismember(data1{l},{'gm'})
                                   values{l}=values{l}./icv; 
                                end
                            end
                        end
                    end
                    if ismember(data,{'concat'})
    %                     a=[a; [values_cbf; values]'];

                        values1=[];
                        for l=1:length(data1)
                            values1=[values1; values{l}];
                        end
                         a=[a; values1'];
    %                 elseif ismember(data, {'multipl'})
    %                     values1=[];
    %                     for l=1:length(data1)
    %                         values1=[values1; values{l}];
    %                     end
    %                      a=[a.*values1']
                    else                        
                        if ~isempty(train_vector) && ~confounder                          
                            values=linearKernel(train_vector,(values'-mu)./sigma);   
                        end
                        a=[a; values'];
                    end
                    size(a)
                end     
            end
            clear atlas;
        case 'tbss' % Modified by Sandrine
            labelfile = fullfile(data_drive, study, 'Features', 'labels.txt');
            subjects=tdfread(labelfile);
            [subjects.label, label, train_index]=get_labels(subjects.label,'use_dti_subjects',use_dti_subjects,'groups', groups);
            dti_var=0;
            a=[];
            for i=1:size(label,1)
                if use_iris_subjects==1
                    subject_label=strtrim(subjects.label(i+9,:))
                else
                    subject_label=strtrim(subjects.label(i,:))
                end
                if use_iris_subjects==1 && (ismember(subject_label,{'FIAT_004','FIAT_005','FIAT_006','FIAT_007','FIAT_009','FIAT_011','FIAT_012','FIAT_013','FIAT_014'}))
                    subject_label=strtrim(subjects.label(i+9,:))
                    if use_dti_subjects==1 && ( ismember(subject_label,{'IRIS_015'}) || dti_var)
                        dti_var=1;                    
                        subject_label=strtrim(subjects.label(i+1,:))                    
                    end

                elseif use_dti_subjects==1 && ( ismember(subject_label,{'IRIS_015'}) || dti_var)
                    dti_var=1;                    
                    if use_iris_subjects==1
                        subject_label=strtrim(subjects.label(i+10,:))
                    else
                        subject_label=strtrim(subjects.label(i+1,:))
                    end 

                end
                imagefile=fullfile(filename, [subject_label '.nii.gz']);
                if exist(imagefile,'file')~=2
                    imagefile=fullfile(filename, [subject_label '.nii']);
                end
                image=load_nii(imagefile);
                values=nonzeros(image.img);
                a=[a; values'];
                size(a) 
            end
        case 'age'
            if exist('filename','var')   
                [a,label] = age_txt(filename,use_old_subjects);
            end
        otherwise %region, selection, lobe, hemisphere, brain
            subjects_label=[];
            if ismember(data,{'gm','wm', 'cbf','wm_md','gm_md','wm_fa'})
                if ismember(features,{'selection'})
                    [a,label, train_index, subjects_label] = selection_region_txt(filename,use_old_subjects, use_dti_subjects);
                else
                    [a,label,train_index, subjects_label] = open_region_txt(filename,use_old_subjects,use_dti_subjects,dti);
                end
            end
            if ismember(data,{'cbf','wm_md','gm_md','wm_fa'}) && use_norm
                a=a{2};
            elseif ismember(data,{'cbf','wm_md','gm_md','wm_fa'})
                a=a{1};
            elseif ismember(data,{'concat'})   
                a=[];
                for l=1:length(data1)
                    [a1,label,train_index,subjects_label]=open_region_txt(filename{l},use_old_subjects,use_dti_subjects, dti);
                    a1=usenormalizedfeatures(a1,use_norm);
                    a=[a, a1];
                end                       
            elseif ismember(data,{'multipl'})
                a=[];
                for l=1:length(data1)
                    [a1,label,train_index,subjects_label]=open_region_txt(filename{l},use_old_subjects,use_dti_subjects,dti);
                    a1=usenormalizedfeatures(a1,use_norm);
                    if isempty(a)
                        a=ones(size(a1));
                    end
                    a=a.*a1;
                end             
            end
    end

    if nfold_c==0
        nfold_c = size(a,1);
    end

    if ismember(features,{'vbm'}) && ismember(data,{'gm'}) && use_hard_segmentation 
        mask=mask_or;
    elseif ismember(features,{'vbm'}) && ismember(data,{'gm'})
        mask=mask_brain;
    elseif ismember(features,{'vbm'})                    
        mask=mask_vote;
    else
        mask=struct;
    end  

    if use_old_subjects
        subjectset='old';
    elseif use_dti_subjects
        subjectset='dti';
    elseif use_dti_subjects
        subjectset='iris only';
    else
        subjectset=sprintf('%s_', groups{:});
    end
    info={data, features, cell2str(data1), subjectset, study, selection_method,robustc};

    % resampletraintest=1;
    % if resampletraintest && ismember(study,{'Cuingnet_data'})
    %     train_index=[];
    % end
   if confounder
        [ages, genders] = read_age_gender(subjects_label, fullfile(data_drive, study));
        confounders=[ages,genders];
        if confounder>1
            [icvs] = read_icv(subjects_label, fullfile(data_drive, study, data_directory));
            confounders=[ages,genders,icvs];
        end
        
   else 
        confounders=[];
   end    
end


function filename1 = getregionfilenames(data_drive, study, data_directory, f, data1, use_hard_segmentation, use_mean_probability)
            
    %region
    if ismember(data1,{'gm','wm'})
        if use_hard_segmentation
            filename1=fullfile(data_drive, study, data_directory, [f '_' data1 '_volume.txt']); %region 
        elseif use_mean_probability
            filename1=fullfile(data_drive, study, data_directory, [f '_' data1 '_probabilities.txt']); %region               
        else
            filename1=fullfile(data_drive, study, data_directory, [f '_' data1 '_probabilistic_volume.txt']); %region 
        end
    else
        filename1= [fullfile(data_drive, study, data_directory, [f '_' data1 '_values_0.txt']); fullfile(data_drive, study, data_directory, [f '_' data1 '_values_1.txt'])];
    end
end

function a=usenormalizedfeatures(a,use_norm)
    if iscell(a)
        if use_norm
            a=a{2};
        else
            a=a{1};
        end
    end
end

function c_cell=str2cell(c_str)
    c_cell=cell(1);
    i=0;
    while ~isempty(c_str)
        [a,c_str]=strtok(c_str,',');
        i=i+1;
        c_cell{i}=a;
    end
    c_cell;
end

function c_str=cell2str(c_cell)
    c_str=c_cell{1}
    for i=2:numel(c_cell)
        c_str=[c_str ',' c_cell{i}];
    end
end