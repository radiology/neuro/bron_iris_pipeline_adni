function [subjects, label, train_index, remove_list] = get_labels(varargin)
    
p = inputParser;   % Create an instance of the class.

p.addOptional('subjects', [], @ischar);

% Parameters
p.addParamValue( 'use_dti_subjects', 0, @isnumeric);

%%% by Sandrine %%%
p.addParamValue( 'use_iris_subjects', 0, @isnumeric);
%%% ----------- %%%
p.addParamValue( 'groups', {'AD','CN'}, @iscell);
% Parse input
p.parse( varargin{:} );

% Extract settings from input
subjects = p.Results.subjects;
use_dti_subjects = p.Results.use_dti_subjects;
use_iris_subjects = p.Results.use_iris_subjects;
groups = p.Results.groups;

temp_label=zeros(size(subjects,1),1)-1;
% If train set CADDementia used for training
if size(subjects,1)>30 && exist('train.txt','file')
     textfile=tdfread('train.txt')
     
     
     for i=1:size(subjects,1)
         subjects(i,:)
         
         index=find(ismember(textfile.ID,subjects(i,:),'rows'));
         if ~isempty(index)
             temp_label(i)=textfile.output(index);
         end
     end
     
end

if use_dti_subjects
    none_dti=find(ismember(subjects,'IRIS_015','rows'));
    subjects(none_dti,:)=[];
end

%%% by Sandrine %%%
if use_iris_subjects
    none_iris=find(ismember(subjects,'FIAT_004','rows'));
    none_iris=[none_iris,find(ismember(subjects,'FIAT_005','rows'))];
    none_iris=[none_iris,find(ismember(subjects,'FIAT_006','rows'))];
    none_iris=[none_iris,find(ismember(subjects,'FIAT_007','rows'))];
    none_iris=[none_iris,find(ismember(subjects,'FIAT_009','rows'))];
    none_iris=[none_iris,find(ismember(subjects,'FIAT_011','rows'))];
    none_iris=[none_iris,find(ismember(subjects,'FIAT_012','rows'))];
    none_iris=[none_iris,find(ismember(subjects,'FIAT_013','rows'))];
    none_iris=[none_iris,find(ismember(subjects,'FIAT_014','rows'))];
    subjects(none_iris,:)=[];
end
%%% ----------- %%%
patients=[find(sum(ismember(subjects(:,1:6),'IRIS_0'),2)==6); find(sum(ismember(subjects(:,1:3),'AD_'),2)==3); find(temp_label==2)];
controls=[find(sum(ismember(subjects(:,1:6),'IRIS_9'),2)==6); find(sum(ismember(subjects(:,1:6),'IRIS_8'),2)==6); find(sum(ismember(subjects(:,1:6),'FIAT_0'),2)==6); find(sum(ismember(subjects(:,1:3),'CN_'),2)==3); find(temp_label==0)];
mcic=[find(sum(ismember(subjects(:,1:5),'MCIc_'),2)==5); find(temp_label==1)];
mcinc=[find(sum(ismember(subjects(:,1:6),'MCInc_'),2)==6)];

% label=num2str(zeros(size(subjects,1),1));
% label(:)='';
label=repmat(blanks(1), size(subjects,1), 1);
label(controls)='C';
label(patients)='P';
label(mcic)='M';
if ismember('MCI',groups)
    label(mcinc)='M';
else
    label(mcinc)='N';
end

label_new=[];
remove_list=[];
for ii=1:numel(label)
    i=label(ii);
    if ~((i==blanks(1)) || ((i=='P') && ismember('AD', groups)) || ((i=='C') && ismember('CN', groups)) || ((i=='M') && ismember('MCIc', groups)) || ((i=='N') && ismember('MCInc', groups)) || ((i=='N' || i=='M') && (ismember('MCI', groups))))
        remove_list=[remove_list; ii];
    end
end


subjects(remove_list,:)=[];
label(remove_list,:)=[];



train_index=[ find(sum(ismember(subjects(:,4:8),'train'),2)==5); find(sum(ismember(subjects(:,6:10),'train'),2)==5); find(sum(ismember(subjects(:,7:11),'train'),2)==5)];
% If train set CADDementia used for training
if size(subjects,1)>30
    train_index=[train_index; find(sum(ismember(subjects(:,1:5),'train'),2)==5)];
end
    
% remove_list2=[];
% if max(temp_label)~=-1
%     for ii=1:numel(train_index)
%         i=label(train_index(ii));
%         if ~((i==blanks(1)) || ((i=='P') && ismember('AD', groups)) || ((i=='C') && ismember('CN', groups)) || ((i=='M') && ismember('MCIc', groups)) || ((i=='N') && ismember('MCInc', groups)) || ((i=='N' || i=='M') && (ismember('MCI', groups))))
%             remove_list2=[remove_list2; ii];
%         end
%     end
% end
% 
% train_index(remove_