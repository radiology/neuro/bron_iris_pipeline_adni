function K_corrected=confounder_correction_by_kernel_regression(K,X)
% See Abdulkadir et al. (2014)

[n,n1]=size(K);

if n~=n1
    display('Error: K is not a square matrix.')
    K_corrected=[];
    return 
end

I=eye(n);
R=I-X*(X'*X)^(-1)*X';
K_corrected=R*K*R';