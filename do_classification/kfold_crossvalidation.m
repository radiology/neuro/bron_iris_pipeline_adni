function [outcome, Acc, Sens, Spec] = kfold_crossvalidation(varargin)
warning('off','MATLAB:nearlySingularMatrix')
warning('off','MATLAB:singularMatrix')

p = inputParser;   % Create an instance of the class.

% Arguments
p.addOptional('A', [], @(in) isa( in, 'dataset' ) || isempty( in ) || iscell( in ) );
p.addOptional('nfold', [], @isnumeric);
p.addOptional('name', [], @ischar);
p.addOptional('style', [], @iscell);
p.addOptional('base','',@ischar);

% Parameters
p.addParamValue( 'use_forward_selection', 0, @isnumeric);
p.addParamValue( 'selection_method', 'maha', @ischar);
p.addParamValue( 'input_matrix', [], @isnumeric);
p.addParamValue( 'use_kernel', 0, @isnumeric);
p.addParamValue( 'svm_parameters', {}, @iscell);
p.addParamValue( 'probability_est', 0, @isnumeric);
p.addParamValue( 'use_auc', 0, @isnumeric );
p.addParamValue( 'max_iterations', 50, @isnumeric);

% Parse input
p.parse( varargin{:} );

% Extract settings from input
A   = p.Results.A;
nfold = p.Results.nfold;
name = p.Results.name;
style = p.Results.style;
base = p.Results.base;
use_forward_selection = p.Results.use_forward_selection;
selection_method = p.Results.selection_method;
input_matrix = p.Results.input_matrix;
use_kernel = p.Results.use_kernel;
svm_parameters = p.Results.svm_parameters;
probability_est = p.Results.probability_est;
use_auc           = p.Results.use_auc;
max_iterations =p.Results.max_iterations;

if use_forward_selection
    name=[name '_featsel_' selection_method];
end
if input_matrix
    name=[name '_inputmatrix'];
end    
if use_kernel
    name=[name '_kernel'];
end
if ~isempty(svm_parameters)
    name=[name '_C' num2str(svm_parameters{1})];
end


ntrain=size(+A,1);
nrfeat=size(+A,2);
nrclass=2;

%Make fold matrix for crossvalidation
if nfold==0
    nfold=ntrain; %Use leave-one-out crossvalidation if nfold=0
end

fold=0:ceil(ntrain/nrclass/nfold):ntrain/nrclass;
fold(nfold+1)=ntrain/nrclass;
foldmat(1:ntrain/nrclass,1:nfold)=0;

for s=1:1:nfold 
    foldmat(fold(s)+1:fold(s+1),s)=1;
end

s = RandStream.create('mt19937ar','seed',sum(100^2));
RandStream.setDefaultStream(s);


Accs=zeros(max_iterations,1);
Senss=zeros(max_iterations,1);
Specs=zeros(max_iterations,1);
aucs=zeros(max_iterations,1);
for nr_iteration=1:max_iterations
    class=struct([]);
    foldmat_class=struct([]);
    for n=1:nrclass;
        class{n}=seldat(A,n);  
        foldmat_class{n}=foldmat(randperm(max(size(foldmat))),:);
    end

    A_new=[class{1}; class{2}];
    foldmat_new=[foldmat_class{1}; foldmat_class{2}]


    %Start crossvalidation
    roc_e=[];
    Error_crossval=[];
    Est_labels=[];
    Real_labels=[];
    outcome = [];
    gamma = [];

    test_subset=1:ntrain;
    nrtest=length(test_subset);
    wsel_array=cell(nrtest,1);
    thresh=[];
    c=[];

    nr_sel_feat=29;

    direc=[base, filesep, name];
    if exist(direc,'dir')~=7
        mkdir(direc)
    end
    filetemp=[direc, filesep, 'outcome_temp.txt'];
    file_c=[direc, filesep, 'c'];

    if use_kernel
        linearKernel=@(X)X*X';
        KernelA=linearKernel(+A_new);
    end

    %Table with results
    featselresults = struct('Y', {}, 'R', {}); %creates an empty structure with fields
    numselfeatures =[ ];

    for i=1:1:nfold %
        i
        % Create train set
    %     setA=[1:test_subset(i)-1, test_subset(i)+1:ntrain];
        setA=find(~foldmat_new(:,i));
        setTest=find(foldmat_new(:,i));

        if use_kernel
            TrainKernelA = KernelA(setA,setA);
            TestKernelA = KernelA(setTest,setA);

            labels=getlabels(A_new);
            TrainLabels=labels(setA);
            TestLabels=labels(setTest);

            train=dataset(TrainKernelA, TrainLabels);
            test= dataset(TestKernelA, TestLabels);
        else
            train=A_new(setA,:);   
            test=A_new(setTest,:);
        end

        if use_forward_selection  

            train_original=train;
            test_original=test;

            switch selection_method
                case 'maha'
                    [wsel,r]=featself(train_original,'maha-s')
                case 'svm'
                    [wsel,r]=featself(train_original,libsvmc2([],'linear',{},'svm_parameters',{.1}))
                case 'svm-auc'
                    [wsel,r]=featsellr_auc(train_original,libsvmc2([],'linear',{},'svm_parameters',{.1}),[],1,0)                
                case 'eucl'
                    [wsel,r]=featself(train_original,'eucl-s')
                case 'maha-lr'
                    [wsel,r]=featsellr(train_original,'maha-s',[],2,1)
                case 'svm-lr'
                    [wsel,r]=featsellr(train_original,libsvmc2([],'linear',{},'svm_parameters',{.1}),[],2,1)
                case 'svm-lr-auc'
                    [wsel,r]=featsellr_auc(train_original,libsvmc2([],'linear',{},'svm_parameters',{.1}),[],2,1)                
                case 'eucl-lr'
                    [wsel,r]=featsellr(train_original,'eucl-s',[],2,1)
            end

            % Save Y and R in a structure
            featselresults(i).Y=wsel;
            featselresults(i).R=r;

            %Save the number of features selected in a table
            numselfeatures(i)=numel(+wsel);

            file=[direc, filesep, num2str(i), '.txt']
            fid=fopen(file,'w')
            for j=1:size(r,1)
                for k=1:size(r,2)
                    fprintf(fid, '%f ',r(j,k));
                end
                fprintf(fid, '\n');
            end
            fprintf(fid, '%f', length(wsel.data{1}))

            fclose(fid);

            wsel_array{i}=wsel.data{1}
            train=train_original*wsel;
            test=test_original*wsel;
        end

        if input_matrix~=[] % (ttest selection)
            train_original=train;
            test_original=test;

            Ti_stat=zeros(ntrain,nrfeat);
            min_ti=zeros(nrfeat,1);
    %         for j=1:nrfeat % Features
            min_ti=min(input_matrix([1:i-1, i+1:ntrain],:));
    %         end
            [region, output, order]=getfeaturelist_nocerebellum(min_ti);
    %         [region, output, order]=getfeaturelist(min_ti);


            wsel=mapping('featsel','fixed', order(1:nr_sel_feat),zeros(1,1),nrfeat,nr_sel_feat);
            wsel.name='Ttest FeatSel';
            wsel_array{i}=wsel.data{1};
            train=train_original*wsel;
            test=test_original*wsel;
        end

        if use_kernel
            w = libsvmc2(train, 'precomputed', {}, 'probability_est', probability_est, 'svm_parameters', svm_parameters, 'use_auc', use_auc);
        else
            w = libsvmc2(train, 'linear', {}, 'probability_est',probability_est, 'svm_parameters', svm_parameters, 'use_auc', use_auc);
        end

        thresh=[thresh  w.data{1}.rho];
        c=[c w.data{2}.used_svm_parameters];


    %     % Test performance on testing set   
        o=libsvmm2(test, w);
        +o;
        outcome=[outcome; o];

        fid=fopen(filetemp,'a');
        fprintf(fid, '%f ',+o(1));
        fclose(fid);

        Error_crossval=[Error_crossval; testd(o,1)];
        Est_labels=[Est_labels; labeld(o)];
        Real_labels=[Real_labels; getlabels(test)];    
    %     Error_crossval=[Error_crossval; testd(test*w,1)];
    %     Est_labels=[Est_labels; labeld(test*w)];
    %     Real_labels=[Real_labels; getlabels(test)];
        gamma=[gamma w.data{2}.used_kernel_parameters];
    end
    +outcome;
    thresh';
    c';

    save(file_c, 'c');

    if use_forward_selection
        save([direc, filesep, selection_method, '_RY.mat'], 'featselresults' );
        save([direc, filesep, selection_method, '_nrfeatures.mat'], 'numselfeatures' );
    end
    %Make ROC-curve
    if exist('style')==1
        roc_e=roc_thr(outcome,2);
        [maxsum,x]=max((1-roc_e.error)+(1-roc_e.xvalues));
        save([direc, filesep, 'roc_e'], 'roc_e' );
        specmax=1-roc_e.error(x);
        sensmax=1-roc_e.xvalues(x);
        plot_roc_sens( roc_e, name, style);
    end

    if or(use_forward_selection==1 ,input_matrix~=[])
    %Make histogram
        x=[];
        for i=1:length(wsel_array)
           x=[x wsel_array{i}];
        end
        x'
        f=hist(x,nrfeat) 

        [region_o, output, order]=getfeaturelist_nocerebellum(f)
    %     [region_o, output, order]=getfeaturelist(f);

        region_o;

        file=[direc, filesep, 'regions.txt'];
        fid=fopen(file,'w');
        for j=1:size(region_o,1)
            fprintf(fid, '%s ',char(region_o(j)));
        end
        fclose(fid);

        pos = get(0, 'ScreenSize');
        figure('Position',pos),bar(output)
        set(gca, 'XTick', 1:nrfeat, 'XTickLabel', region_o)
        xticklabel_rotate
        title(['Frequency of selected regions in classification of dementia based on ' name])

        figure('Position',pos),bar(output(1:nrfeat/2))
        set(gca, 'XTick', 1:nrfeat/2, 'XTickLabel', region_o(1:nrfeat/2))
        xticklabel_rotate
        title(['Frequency of selected regions in classification of dementia based on ' name])

        l=[];
        for i=1:nrtest
            l=[l; length(wsel_array{i})];
        end
        mean(l);
        std(l);

    end

    %Area under the curve (AUC)
    auc=1-testauc(outcome);

    %Where did it go wrong?
    Error = Error_crossval;

    %Print estimated and real labels
    [Est_labels'; Real_labels']

    %Calculate Error
    Acc=1-(sum(Error_crossval)/size(Error_crossval,1));

    %Calculate Sensitivity and Specificity
    conmat=confmat(Real_labels,Est_labels)
    cmat(1:2,1:2)=0;
    cmat(1:size(conmat,1),1:size(conmat,2))=conmat;

    Sens=cmat(2,2)/(cmat(2,2)+cmat(2,1)); %TP/(TP+FP)
    Spec=cmat(1,1)/(cmat(1,1)+cmat(1,2)); %TN/(TN+FN)

    % Accuracy(nr_sel_feat)=Acc;
    % Sensitivity(nr_sel_feat)=Sens;
    % Specificity(nr_sel_feat)=Spec;

    file=[direc, filesep, 'outcome' num2str(nr_iteration) '.txt'];
    fid=fopen(file,'w');
    for j=1:size(outcome,1)
        fprintf(fid, '%f ',outcome(j));
    end
    fclose(fid);

%     file=[direc, filesep, 'real_labels' num2str(nr_iteration) '.txt'];
%     fid=fopen(file,'w');
%     for j=1:size(Real_labels,1)
%         fprintf(fid, '%s ',Real_labels(j));
%     end
%     fclose(fid);
% 
%     file=[direc, filesep, 'estimated_labels' num2str(nr_iteration) '.txt'];
%     fid=fopen(file,'w');
%     for j=1:size(Est_labels,1)
%         fprintf(fid, '%s ',Est_labels(j));
%     end
%     fclose(fid);

    result_file=fullfile(base, 'results_4fold.txt');
    if ~exist(result_file,'file')
        fid=fopen(result_file,'w');
        fprintf(fid, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n','Name', 'Iteration', 'Predefined kernel', 'Y', 'M', 'D', 'h', 'm', 's', 'Accuracy', 'Sensitivity', 'Specificity', 'Area Under Curve', 'Max Sens+Spec', 'Max Sens', 'Max Spec');
        fclose(fid);
    end

    fid=fopen(result_file,'a');
    fprintf(fid, '%s\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n',name, nr_iteration, use_kernel, fix(clock),Acc, Sens, Spec, auc, maxsum, sensmax, specmax);
    fclose(fid);

    result={'Name', 'Feature selection', 'Predefined kernel', 'Clock', 'Accuracy', 'Sensitivity', 'Specificity', 'Area Under Curve', 'Max Sens+Spec', 'Max Sens', 'Max Spec'; name, use_forward_selection, use_kernel,fix(clock),Acc, Sens, Spec, auc, maxsum, sensmax, specmax}'

    Accs(nr_iteration)=Acc;
    Senss(nr_iteration)=Sens;
    Specs(nr_iteration)=Spec;
    aucs(nr_iteration)=auc
end

result_file=fullfile(base, '4fold_iteration.txt');
if ~exist(result_file,'file')
    fid=fopen(result_file,'w');
    fprintf(fid, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n','Name', 'Y', 'M', 'D', 'h', 'm', 's', 'Mean AUC', 'Std AUC', 'Min AUC', 'Max AUC');
    fclose(fid);
end

fid=fopen(result_file,'a');
fprintf(fid, '%s\t%i\t%i\t%i\t%i\t%i\t%i\t%f\t%f\t%f\t%f\n',name, fix(clock),mean(aucs), std(aucs), min(aucs), max(aucs));
fclose(fid);

result={'Name', 'Mean AUC', 'Std AUC', 'Min AUC', 'Max AUC'; name, mean(aucs), std(aucs), min(aucs), max(aucs)}'
