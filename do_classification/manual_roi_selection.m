function new_binary_atlas=manual_roi_selection(atlas,use_forward_selection)

atlas_image=load_nii(atlas);
image=atlas_image.img;

switch use_forward_selection
    case 1 %Cingulate gyrus: 24,25,26,27,76,77,80,81
        binary_atlas=ismember(image,[24,25,26,27,76,77,80,81]);
    case 2 %Hippocampus: 1,2,3,4
        binary_atlas=ismember(image,[1,2,3,4]);        
    case 3 %Parahippocampal gyrus: 9,10
        binary_atlas=ismember(image,[9,10]);
    case 4 %Fusiform gyrus: 15,16
        binary_atlas=ismember(image,[15,16]);     
    case 5 %Superior parietal gyrus (incl. Precuneus): 62,63
        binary_atlas=ismember(image,[62,63]);
    case 6 %Medial/inferior temporal gyrus: 13,14
        binary_atlas=ismember(image,[13,14]);        
    case 7 %Temporal lobe: 5,6,7,8,11,12,13,14,15,16,30,31,82,83
        binary_atlas=ismember(image,[5,6,7,8,11,12,13,14,15,16,30,31,82,83]);
    case 8 %Hippocampus and parahippocampal gyrus: 1,2,3,4,9,10
        binary_atlas=ismember(image,[1,2,3,4,9,10]);
    case 9 %Temporal lobe, hippocampus and parahippocampal gyrus: 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,30,31,82,83
        binary_atlas=ismember(image,[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,30,31,82,83]);          
end
new_binary_atlas=uint8(binary_atlas);
end

