function significant_features=randomized_ttest(varargin)
%Trains SVM using the specified list file and
%saves P-map as well as model
%needs libsvm and NIFTI loaders pre installed 

savehist=1

p = inputParser;   % Create an instance of the class.
% Arguments
p.addOptional('A', [], @(in) isa( in, 'dataset' ) || isempty( in ) || iscell( in ) );
p.addOptional('name', [], @ischar);
p.addOptional('template_directory',[],@ischar);

% Parameters
p.addParamValue( 'num_max_perms', 5000, @isnumeric);
p.addParamValue( 'i_featsel', 0, @isnumeric);
p.addParamValue( 'sign_level', 0.05, @isnumeric);
p.addParamValue( 'save_file', 0, @isnumeric);
p.addParamValue( 'mask', struct, @isstruct);
p.addParamValue( 'tmap_directory', [], @ischar);

% Parse input
p.parse( varargin{:} );
clear varargin

% Extract settings from input
A   = p.Results.A;
name = p.Results.name;
template_directory=p.Results.template_directory;
num_max_perms=p.Results.num_max_perms;
i_featsel=p.Results.i_featsel;
sign_level=p.Results.sign_level;
save_file=p.Results.save_file;
mask=p.Results.mask;
tmap_directory=p.Results.tmap_directory;
clear p

if i_featsel<1
    savehist=0;
else
    savehist=1;
end


name=regexprep(name, ['_p' num2str(sign_level)], '');
output_directory=fullfile(template_directory, 'Tmaps')
if ~exist(output_directory,'dir')
    mkdir(output_directory)
end
if isempty(tmap_directory)
    tmap_directory=output_directory
end

histfile=fullfile(tmap_directory,['histt_' name '_' num2str(i_featsel)]);
tfile=fullfile(tmap_directory,['t_' name '_' num2str(i_featsel) '.mat']);

name
ctr=1;

if exist([histfile num2str(num_max_perms) '.mat'], 'file') && exist(tfile, 'file')
   load(tfile);
   arr=zeros(size(t_star));
   for i=1:num_max_perms
    load([histfile num2str(i) '.mat']);
    arr=arr + (abs(t(:))'>abs(t_star)); %%% NOG NAAR KIJKEN!!!
   end
  clear t_list; clear t_star;
  tmap=(arr/num_max_perms)';
  clear arr;
else
    if exist(output_directory)~=7
       mkdir(output_directory)
    end

    labels=A.nlab;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    groups1=cell(2,1);
    for lab=1:2
        groups1{lab}=A(find(A.nlab==lab),:);
    end 
    [h,r,ci,stats]=ttest2(groups1{1}.data,groups1{2}.data,'alpha',sign_level);
    t_star=stats.tstat;

    %Serial permutations 
    arr=zeros(size(t_star));

    s = RandStream.create('mt19937ar','seed',sum(100^2));
    RandStream.setGlobalStream(s);
    reset(s);

%     A_new=A;
%     if savehist
%         t_list=zeros(length(t_star),num_max_perms);
%     end
    i_robust=0;
    for perm=1:num_max_perms
        if ~mod(perm,1000)
            perm
        end

        %%%%%%%%%%%%%%%%%%%%%Randomly permute%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        s = RandStream.create('mt19937ar','seed',100^2+perm);
        RandStream.setGlobalStream(s);
        labels=labels(randperm(max(size(labels))));
        A.nlab=labels;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        groups1=cell(2,1);
        for lab=1:2
            groups1{lab}=A(find(A.nlab==lab),:);
        end 
        [h,r,ci,stats]=ttest2(groups1{1}.data,groups1{2}.data,'alpha',sign_level);
        clear groups;
        clear h; clear r; clear ci;
        t=stats.tstat;
        clear stats;
        

        %Count number of times absolute value of t exceeds that of t_star at every
        %voxel. Note that this gives us our p-values

        arr=(abs(t)>abs(t_star))+arr;
        
        if savehist
            t=single(t);
            save( [histfile num2str(perm) '.mat'], 't');
        end
        
        save( fullfile(output_directory, [name '_arrt']), 'arr');
        save( fullfile(output_directory, [name '_permt']), 'perm');
    end
    tmap=(arr/num_max_perms)';
    clear arr;

    if savehist
        t_star=single(t_star);
        save( tfile, 't_star');
        
    end
    clear t; clear t_star;
end

 tmap(find(tmap==0))=0.001;
 significant_features=find(tmap<sign_level);
 if length(significant_features)<=1;
     significant_features=find(tmap==min(tmap));
     sorted=sort(tmap);
     significant_features=[significant_features find(tmap==sorted(2))];
 end
 
 save( fullfile(output_directory, [name '_tmap']), 'tmap');
 
 if save_file   
     if size(A,2)>72
         img=zeros(size(mask.img));
         img(find(mask.img==1))=tmap<sign_level;
         
         tmp_nii=mask;
         tmp_nii.img=double(tmp_nii.img);
         tmp_nii.img(find(mask.img==1))=double(1000*tmap);
         tmp_nii.img=unxform_nii(tmp_nii, tmp_nii.img);
         tmp_nii.hdr=tmp_nii.original.hdr;
         tmp_nii.original=[];
         tmp_nii.hdr.dime.datatype=16;  
    %      tmp_nii=load_untouch_nii(imgList{1}{ctr});
    %      x=reshape(O{1},maxa-mina+1,maxb-minb+1,maxc-minc+1);
    %      tmp_nii.img=zeros(size(tmp_nii.img));
    %      tmp_nii.img(mina:maxa, minb:maxb, minc:maxc)=1000*x;
         save_untouch_nii(tmp_nii,fullfile(output_directory, ['Tmap_exp_' name '_' num2str(i_featsel) '.nii.gz']));
%      else
%          if size(A,2)==72
%              seg='seg';
%              nr=83
%          elseif size(A,2)==28
%              seg='selection'
%              nr=83
%          elseif size(A,2)==10
%              seg='lobe';
%              nr=11
%          elseif size(A,2)==2
%              seg='hemisphere';
%              nr=3
%          elseif size(A,2)==1
%              seg='brain';
%              nr=2
%          end
% 
%          segname=seg;
%          if ismember(seg,'selection')
%              segname='seg';
%          end    
% 
%          template=fullfile(template_directory, [segname '_template.nii.gz']);
%          tmp_nii=load_untouch_nii(template);
% 
%          switch seg
%              case 'seg'
%                 remove_list=[17    18     19    44    45    46    47    48    49    74    75];
%              case {'lobe', 'hemisphere', 'brain'}
%                 remove_list=size(A,2)+1;
%              case 'selection'
%                 remove_list=[3 4 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 60 61 64 65 66 67 68 69 70 71 72 73 74 75 78 79];
%          end
% 
%          img=double(tmp_nii.img);
% 
%          tmp_nii.hdr.dime.datatype=16;
%          count=0;
%          for i=1:nr
%              i_count=i-count;
%              ind=find(img==i);
%              if  ~isempty(find(remove_list==i))
%                  img(ind)=0;
%                  count=count+1;
%              else
%                  img(ind)=1000*pmap(i_count);
%              end         
%          end      
% 
%          tmp_nii.img=double(img);
% 
% 
%          save_untouch_nii(tmp_nii,fullfile(output_directory, ['Tmap_experimental_ROI' name '.nii.gz'])); %
     end
 end


