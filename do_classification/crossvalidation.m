function [outcome, Acc, Sens, Spec] = crossvalidation(varargin)
warning('off','MATLAB:nearlySingularMatrix')
warning('off','MATLAB:singularMatrix')

p = inputParser;   % Create an instance of the class.

% Arguments
p.addOptional('A', [], @(in) isa( in, 'dataset' ) || isempty( in ) || iscell( in ) );
p.addOptional('nfold', [], @isnumeric);
p.addOptional('name', [], @ischar);
p.addOptional('style', [], @iscell);
p.addOptional('base','',@ischar);
p.addOptional('info','',@iscell);


% Parameters
p.addParamValue( 'use_forward_selection', 0, @isnumeric);
p.addParamValue( 'selection_method', 'maha', @ischar);
p.addParamValue( 'input_matrix', [], @isnumeric);
p.addParamValue( 'use_kernel', 0, @isnumeric);
p.addParamValue( 'svm_parameters', {}, @iscell);
p.addParamValue( 'probability_est', 0, @isnumeric);
p.addParamValue( 'use_auc', 0, @isnumeric );
p.addParamValue( 'num_max_perms', 5000, @isnumeric);
p.addParamValue( 'use_random',0, @isnumeric );
p.addParamValue( 'nr_iterations',1, @isnumeric);
p.addParamValue( 'nfold_c', 5, @(in) isnumeric( in ) && floor( in ) == in && ( in > 1 || in < 0 ) );
p.addParamValue( 'histreg',0, @isnumeric);
p.addParamValue( 'mask',struct, @isstruct);
p.addParamValue( 'remove_single_voxels',0,@isnumeric);
p.addParamValue( 'make_curve', 1, @isnumeric);
p.addParamValue( 'silent', 0, @isnumeric);
p.addParamValue( 'cmin',-5, @isnumeric);
p.addParamValue( 'fixed_c_perm', 1, @isnumeric);
p.addParamValue( 'sign_level', 0.05, @isnumeric);
p.addParamValue( 'ls_svc',0, @isnumeric);
% Parse input
p.parse( varargin{:} );
clear varargin;

% Extract settings from input
A   = p.Results.A;
nfold = p.Results.nfold;
name = p.Results.name;
style = p.Results.style;
base = p.Results.base;
info = p.Results.info;
use_forward_selection = p.Results.use_forward_selection;
selection_method = p.Results.selection_method;
input_matrix = p.Results.input_matrix;
use_kernel = p.Results.use_kernel;
svm_parameters = p.Results.svm_parameters;
probability_est = p.Results.probability_est;
use_auc           = p.Results.use_auc;
num_max_perms = p.Results.num_max_perms;
use_random        = p.Results.use_random;
nr_iterations     = p.Results.nr_iterations;
nfold_c           = p.Results.nfold_c;
histreg           = p.Results.histreg;
mask              = p.Results.mask;
remove_single_voxels=p.Results.remove_single_voxels;
make_curve        = p.Results.make_curve;
silent            = p.Results.silent;
cmin              = p.Results.cmin;
fixed_c_perm      = p.Results.fixed_c_perm;
sign_level        = p.Results.sign_level;
ls_svc            = p.Results.ls_svc;
clear p;

if use_forward_selection
    name=[name '_featsel_' selection_method];
end
if ismember(selection_method,{'roi'})
    name=[name num2str(use_forward_selection)];
end
if input_matrix
    name=[name '_inputmatrix'];
end    
if ~(isempty(svm_parameters) || isempty(svm_parameters{1}))
    name=[name '_C' num2str(svm_parameters{1})];
end
if use_kernel
    name=[name '_kernel'];
end



ntrain=size(+A,1);
nrfeat=size(+A,2);

%Make fold matrix for crossvalidation
if nfold==0
    nfold=ntrain; %Use leave-one-out crossvalidation if nfold=0
end

% fold=0:ceil(ntrain/nfold):ntrain;
% fold(nfold+1)=ntrain;
% foldmat(1:ntrain,1:nfold)=0;

% for s=1:1:nfold 
%     foldmat(fold(s)+1:fold(s+1),s)=1;
% end


%Start crossvalidation
roc_e=[];
Error_crossval=[];
Est_labels=[];
Real_labels=[];
outcome = [];
gamma = [];

test_subset=1:ntrain;
nrtest=length(test_subset);
wsel_array=cell(nrtest,1);
thresh=[];
c=[];

nr_sel_feat=29;

direc=[base, filesep, name];
if exist(direc,'dir')~=7
	mkdir(direc)
end
filetemp=[direc, filesep, 'outcome_temp.txt'];
file_c=[direc, filesep, 'c'];
file_sv=[direc, filesep, 'sv'];
file_sv_featsel=[direc, filesep, 'sv_featsel'];
file_kernel=[direc, filesep, 'kernel']

if use_kernel
    linearKernel=@(X)X*X';
    KernelA=linearKernel(+A);
end

%Table with results
featselresults = struct('Y', {}, 'R', {}); %creates an empty structure with fields
numselfeatures =[ ];
  
pmapi=zeros(71,64);
sv=cell(64,1);
sv_featsel=cell(64,1);
for i=1:1:nrtest %
    i
    if i==5
        i;
    end
    % Create train set
    setA=[1:test_subset(i)-1, test_subset(i)+1:ntrain];
    
    if ~use_kernel
        train=A(setA,:);   
        test=A(test_subset(i),:);
    end
    
    if use_forward_selection && ~ismember(selection_method,{'roi'}) 
        
        train=A(setA,:);   
        test=A(test_subset(i),:);

        train_original=train;
        test_original=test;
      
        switch selection_method
            case 'maha'
                [wsel,r]=featself(train_original,'maha-s')
            case 'svm'
                [wsel,r]=featself(train_original,libsvmc2([],'linear',{},'svm_parameters',{.1}))
            case 'svm-auc'
                [wsel,r]=featsellr_auc(train_original,libsvmc2([],'linear',{},'svm_parameters',{.1}),[],1,0)                
            case 'eucl'
                [wsel,r]=featself(train_original,'eucl-s')
            case 'maha-lr'
                [wsel,r]=featsellr(train_original,'maha-s',[],2,1)
            case 'svm-lr'
                [wsel,r]=featsellr(train_original,libsvmc2([],'linear',{},'svm_parameters',{.1}),[],2,1)
            case 'svm-lr-auc'
                [wsel,r]=featsellr_auc(train_original,libsvmc2([],'linear',{},'svm_parameters',{.1}),[],2,1)                
            case 'eucl-lr'
                [wsel,r]=featsellr(train_original,'eucl-s',[],2,1)               
        end


        
        % Save Y and R in a structure
        featselresults(i).Y=wsel;        

    
        
        if ~ismember(selection_method,{'pmap'})            
            featselresults(i).R=r;
            
            file=[direc, filesep, num2str(i), '.txt'];
            fid=fopen(file,'w');
            fprintf(fid, '%f', length(wsel.data{1}));    
            for j=1:size(r,1)
                for k=1:size(r,2)
                    fprintf(fid, '%f ',r(j,k));
                end
                fprintf(fid, '\n');
            end
            fclose(fid);            
        end
        
        wsel_array{i}=wsel.data{1};     
        
        %Save the number of features selected in a table
        numselfeatures(i)=numel(+wsel); 
        
        if use_kernel && use_forward_selection          
            Asel=A*wsel;
            KernelA=linearKernel(+Asel);
        else
            train=train_original*wsel;
            test=test_original*wsel;
        end
    else
        numselfeatures(i)=nrfeat;
    end
    
    if use_kernel
        TrainKernelA = KernelA(setA,setA);
        TestKernelA = KernelA(test_subset(i),setA);
    
        labels=getlabels(A);
        TrainLabels=labels(setA);
        TestLabels=labels(test_subset(i));
        
        train=dataset(TrainKernelA, TrainLabels);
        test= dataset(TestKernelA, TestLabels);
    end        
        
    if input_matrix~=[] % (ttest selection)
        train_original=train;
        test_original=test;
              
        Ti_stat=zeros(ntrain,nrfeat);
        min_ti=zeros(nrfeat,1);
%         for j=1:nrfeat % Features
        min_ti=min(input_matrix([1:i-1, i+1:ntrain],:));
%         end
        [region, output, order]=getfeaturelist_nocerebellum(min_ti);
%         [region, output, order]=getfeaturelist(min_ti);
        
        
        wsel=mapping('featsel','fixed', order(1:nr_sel_feat),zeros(1,1),nrfeat,nr_sel_feat);
        wsel.name='Ttest FeatSel';
        wsel_array{i}=wsel.data{1};
        train=train_original*wsel;
        test=test_original*wsel;
    end
        % NOTE: for c-parameter search range might be extended
        % (2^-10:2^17) for better results. Search range for first paper:
        % 2^-5:2^17             
        if ls_svc
            svm_type=5;
        else
            svm_type=0;
        end
        if use_kernel                 
            w = libsvmc2_kfold(train, 'precomputed', {}, 'probability_est', probability_est, 'svm_parameters', svm_parameters, 'nfold', nfold_c, 'use_auc', use_auc, 'use_random', use_random, 'nr_iterations', nr_iterations, 'cmin', cmin, 'svm_type', svm_type);
        else           
            w = libsvmc2_kfold(train, 'linear', {}, 'probability_est',probability_est, 'svm_parameters', svm_parameters, 'nfold', nfold_c, 'use_auc', use_auc, 'use_random', use_random, 'nr_iterations', nr_iterations, 'cmin', cmin, 'svm_type', svm_type);
        end

        thresh=[thresh  w.data{1}.rho];
        c=[c w.data{2}.used_svm_parameters];
        sv{i}= w.data{1}.SVs;


    %     % Test performance on testing set   
        o=libsvmm2(test, w, 'svm_type', svm_type);
        +o;
        outcome=[outcome; o];

        fid=fopen(filetemp,'a');
        fprintf(fid, '%f ',+o(1));
        fclose(fid);

        Error_crossval=[Error_crossval; testd(o,1)];
        Est_labels=[Est_labels; labeld(o)];
        Real_labels=[Real_labels; getlabels(test)];    
    %     Error_crossval=[Error_crossval; testd(test*w,1)];
    %     Est_labels=[Est_labels; labeld(test*w)];
    %     Real_labels=[Real_labels; getlabels(test)];
        gamma=[gamma w.data{2}.used_kernel_parameters];
end
+outcome;
thresh';
c';

save(file_c, 'c');
save(file_sv, 'sv');

if use_forward_selection
    save([direc, filesep, selection_method, '_RY.mat'], 'featselresults' );
    save([direc, filesep, selection_method, '_nrfeatures.mat'], 'numselfeatures' );
%     save(file_sv_featsel,'sv_featsel');
end

if or(use_forward_selection==1 ,input_matrix~=[])
%Make histogram
    x=[];
    for i=1:length(wsel_array)
       x=[x wsel_array{i}];
    end
    x'
    f=hist(x,nrfeat) 
    
    [region_o, output, order]=getfeaturelist_nocerebellum(f)
%     [region_o, output, order]=getfeaturelist(f);
    
    region_o;
    
    file=[direc, filesep, 'regions.txt'];
    fid=fopen(file,'w');
    for j=1:size(region_o,1)
        fprintf(fid, '%s ',char(region_o(j)));
    end
    fclose(fid);

    pos = get(0, 'ScreenSize');
    figure('Position',pos),bar(output)
    set(gca, 'XTick', 1:nrfeat, 'XTickLabel', region_o)
    xticklabel_rotate
    title(['Frequency of selected regions in classification of dementia based on ' name])

    figure('Position',pos),bar(output(1:nrfeat/2))
    set(gca, 'XTick', 1:nrfeat/2, 'XTickLabel', region_o(1:nrfeat/2))
    xticklabel_rotate
    title(['Frequency of selected regions in classification of dementia based on ' name])
    
    l=[];
    for i=1:nrtest
        l=[l; length(wsel_array{i})];
    end
    mean(l);
    std(l);
    
end

%Area under the curve (AUC)
auc=1-testauc(outcome);

%Where did it go wrong?
Error = Error_crossval;

%Print estimated and real labels
if ~silent
    [Est_labels'; Real_labels']
end

%Calculate Error
Acc=1-(sum(Error_crossval)/size(Error_crossval,1));

%Calculate Sensitivity and Specificity
conmat=confmat(Real_labels,Est_labels);
if ~silent
    conmat
end
cmat(1:2,1:2)=0;
cmat(1:size(conmat,1),1:size(conmat,2))=conmat;

Sens=cmat(2,2)/(cmat(2,2)+cmat(2,1)); %TP/(TP+FP)
Spec=cmat(1,1)/(cmat(1,1)+cmat(1,2)); %TN/(TN+FN)

% Accuracy(nr_sel_feat)=Acc;
% Sensitivity(nr_sel_feat)=Sens;
% Specificity(nr_sel_feat)=Spec;

file=[direc, filesep, 'outcome.txt'];
fid=fopen(file,'w');
for j=1:size(outcome,1)
	fprintf(fid, '%f ',outcome(j));
end
fclose(fid);

file=[direc, filesep, 'real_labels.txt'];
fid=fopen(file,'w');
for j=1:size(Real_labels,1)
	fprintf(fid, '%s ',Real_labels(j));
end
fclose(fid);

file=[direc, filesep, 'estimated_labels.txt'];
fid=fopen(file,'w');
for j=1:size(Est_labels,1)
	fprintf(fid, '%s ',Est_labels(j));
end
fclose(fid);

%Make ROC-curve
if exist('style')
    roc_e=roc_thr(outcome,2);
    [maxsum,x]=max((1-roc_e.error)+(1-roc_e.xvalues));
    save([direc, filesep, 'roc_e'], 'roc_e' );
    specmax=1-roc_e.error(x);
    sensmax=1-roc_e.xvalues(x);
    if make_curve
        plot_roc_sens( roc_e, [name ' ( AUC=' num2str(auc*100,3) ' )'], style);
    end
end

nrfeat=median(numselfeatures);

result_file=fullfile(base, 'results.txt');
if ~exist(result_file,'file')
    fid=fopen(result_file,'w');
    fprintf(fid, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n','Name', 'Number of features', 'Predefined kernel', 'Y', 'M', 'D', 'h', 'm', 's', 'Accuracy', 'Sensitivity', 'Specificity', 'Area Under Curve', 'Max Sens+Spec', 'Max Sens', 'Max Spec','Data','Features','Data1','Subjectset','Study','Selection method','Robust C');
    fclose(fid);
end

fid=fopen(result_file,'a');
fprintf(fid, '%s\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%s\t%s\t%s\t%s\t%s\t%s\t%i\n',name, nrfeat, use_kernel,fix(clock),Acc, Sens, Spec, auc, maxsum, sensmax, specmax, info{:});
fclose(fid);

if ~silent
    result={'Name', 'Number of features', 'Predefined kernel', 'Clock', 'Accuracy', 'Sensitivity', 'Specificity', 'Area Under Curve', 'Max Sens+Spec', 'Max Sens', 'Max Spec'; name, nrfeat, use_kernel,fix(clock),Acc, Sens, Spec, auc, maxsum, sensmax, specmax}'
end

