function [a,label,train_index, subjects] = selection_region_txt(filename,use_old_subjects, use_dti_subjects)

if size(filename,1)==2
    A1=tdfread1(filename(1,:));
    A2=tdfread1(filename(2,:));
    fn=fieldnames(A1)';
    for f=fn
        f=char(f);  
        diff=size(A1.(f)',1)-size(A2.(f)',1);        
        if diff<0   
            blank=blanks(size(A1.(f),1))';
            for i=1:abs(diff)
                A1.(f)=[A1.(f) blank];
            end
        elseif diff>0
            blank=blanks(size(A2.(f),1))';
            for i=1:abs(diff)
                A2.(f)=[A2.(f) blank];
            end
        end
        A.(f)=[A1.(f)' A2.(f)']';                      
    end
    
else
    A=tdfread1(filename);
end

[indx,indy]=find(sum(ismember(A.Subject,'Subject'),2)<=6);
A=structfun( @(M) M(indx,:) ,A,'Uniform',0);

selection_list=   {'Hippocampus left',
                'Anterior temporal lobe, medial part left',
                'Anterior temporal lobe, lateral part left',
                'Cingulate gyrus, anterior (supragenual) part left',
                'Cingulate gyrus, posterior part left',
                'Middle frontal gyrus left',
                'Posterior temporal lobe left',
                'Remainder of parietal lobe left (including supramarginal and angular gyrus)', 
                'Inferior frontal gyrus left',
                'Superior frontal gyrus left',
                'Superior parietal gyrus left',
                'Subgenual anterior cingulate gyrus left',
                'Pre-subgenual anterior cingulate gyrus left',
                'Superior temporal gyrus, anterior part left',
                'Hippocampus right',
                'Anterior temporal lobe, medial part right',
                'Anterior temporal lobe, lateral part right',
                'Cingulate gyrus, anterior (supragenual) part right',
                'Cingulate gyrus, posterior part right',
                'Middle frontal gyrus right'
                'Posterior temporal lobe right',
                'Remainder of parietal lobe right (including supramarginal and angular gyrus)',
                'Inferior frontal gyrus right',
                'Superior frontal gyrus right',
                'Superior parietal gyrus right',
                'Subgenual anterior cingulate gyrus right',
                'Pre-subgenual anterior cingulate gyrus right',
                'Superior temporal gyrus, anterior part right'};
    
                
[indx,indy]=find(ismember(A.ROI_name,selection_list));
A=structfun( @(M) M(indx,:) ,A,'Uniform',0);

if use_old_subjects
    old_subjects=   {'FIAT_004','FIAT_005','FIAT_007','FIAT_009','FIAT_011','IRIS_902','IRIS_903','IRIS_906','IRIS_907','IRIS_908','IRIS_910','IRIS_911','IRIS_001','IRIS_002','IRIS_003','IRIS_004','IRIS_005','IRIS_009','IRIS_010','IRIS_012','IRIS_014','IRIS_015','IRIS_017','IRIS_018','IRIS_019','IRIS_020','IRIS_021','IRIS_022','IRIS_023','IRIS_024','IRIS_025','IRIS_026'};                                

    [indx,indy]=find(ismember(A.Subject,old_subjects)==1);
    A=structfun( @(M) M(indx,:) ,A,'Uniform',0);
end

if use_dti_subjects
    none_dti_subjects={'IRIS_015'};                                

    [indx,indy]=find(sum(ismember(A.Subject,none_dti_subjects),2)==0);
    A=structfun( @(M) M(indx,:) ,A,'Uniform',0);
end

features=unique(A.ROI_name,'rows');
nrfeat=size(features,1);
subjects=unique(A.Subject,'rows');
nrsub=size(subjects,1);

if any(strcmp('Volume',fieldnames(A)))
    A.Volume=str2num(A.Volume);
    a=reshape(A.Volume,nrfeat,nrsub)';
elseif any(strcmp('Mean_probability',fieldnames(A)))
    A.Mean_probability=str2num(A.Mean_probability);
    a=reshape(A.Mean_probability,nrfeat,nrsub)';    
elseif any(strcmp('Mean',fieldnames(A)))
    A.Mean=str2num(A.Mean);
    a_1=reshape(A.Mean,nrfeat,nrsub)';
    A.Normalized_mean=str2num(A.Normalized_mean);
    a_norm=reshape(A.Normalized_mean,nrfeat,nrsub)';
    a={a_1, a_norm};
end

[label, train_index]=get_labels(subjects);
