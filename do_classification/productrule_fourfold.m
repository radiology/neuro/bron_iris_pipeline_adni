function auc = productrule_fourfold(name, filename_cbf, filename_gm, max_iterations, data)
% outcome_cbf=[0.497701 0.400437 0.667803 0.674178 0.754082 0.308722 0.230679 0.422467 0.533200 0.430776 0.486989 0.468598 0.389762 0.418964 0.236025 0.337825 0.305053 0.338370 0.459140 0.475995 0.479335 0.441411 0.264780 0.286940 0.392398 0.470024 0.695935 0.284381 0.701557 0.654990 0.422924 0.486878 ]';
% outcome_gm=[0.663424 0.654992 0.596282 0.381523 0.666511 0.044422 0.385813 0.476932 0.578912 0.428391 0.505657 0.426142 0.402233 0.090965 0.136251 0.303873 0.514178 0.366951 0.285246 0.108960 0.044696 0.218895 0.447720 0.302435 0.664142 0.558682 0.585698 0.601021 0.633735 0.641742 0.313060 0.354992 ]';

if exist('D:')==7
    base='C:\Users\Esther Bron\Documents\Project\Features_paper';
else
    base=fullfile('~','Project', 'Features_paper');
end

filename_cbf
fid=fopen(filename_cbf,'r');
outcome_cbf = fscanf(fid,'%f');
fclose(fid);

filename_gm
fid=fopen(filename_gm,'r');
outcome_gm = fscanf(fid,'%f');
fclose(fid);

outcome_cbf1=[outcome_cbf 1-outcome_cbf];
outcome_gm1=[outcome_gm 1-outcome_gm];

use_forward_selection=0;
use_kernel=0;

switch data
    case 'product'
        x=outcome_cbf1.*outcome_gm1;
    case 'mean'
        x=(outcome_cbf1+outcome_gm1)/2;
end
data=x./[sum(x')' sum(x')'];

outcome=do_classification('gm','region','use_auc','0', 'four_fold', '1', 'max_iterations', num2str(max_iterations),'no_exit','1');
outcome.data=data;

%Make ROC-curve
roc_e=roc_thr(outcome,2);
plot_roc_sens( roc_e, name, {'r-  '});

auc=1-testauc(outcome);
conmat=confmat(getlabels(outcome),labeld(outcome));
cmat(1:2,1:2)=0;
cmat(1:size(conmat,1),1:size(conmat,2))=conmat;

Acc=((cmat(1,1)+cmat(2,2))/(cmat(2,2)+cmat(2,1)+cmat(1,1)+cmat(1,2)))
Sens=cmat(2,2)/(cmat(2,2)+cmat(2,1)) %TP/(TP+FP)
Spec=cmat(1,1)/(cmat(1,1)+cmat(1,2)) %TN/(TN+FN)

result_file=fullfile(base, 'results.txt');
if ~exist(result_file,'file')
    fid=fopen(result_file,'w');
    fprintf(fid, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n','Name', 'Feature selection', 'Predefined kernel', 'Y', 'M', 'D', 'h', 'm', 's', 'Accuracy', 'Sensitivity', 'Specificity', 'Area Under Curve');
    fclose(fid);
end

fid=fopen(result_file,'a');
fprintf(fid, '%s\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%f\t%f\t%f\t%f\n',name, use_forward_selection, use_kernel,fix(clock),Acc, Sens, Spec, auc);
fclose(fid);

result={'Name', 'Feature selection', 'Predefined kernel', 'Clock', 'Accuracy', 'Sensitivity', 'Specificity', 'Area Under Curve'; name, use_forward_selection, use_kernel,fix(clock),Acc, Sens, Spec, auc}'