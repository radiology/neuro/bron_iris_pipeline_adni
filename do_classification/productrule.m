function [auc,outcome] = productrule(varargin)
% outcome_cbf=[0.497701 0.400437 0.667803 0.674178 0.754082 0.308722 0.230679 0.422467 0.533200 0.430776 0.486989 0.468598 0.389762 0.418964 0.236025 0.337825 0.305053 0.338370 0.459140 0.475995 0.479335 0.441411 0.264780 0.286940 0.392398 0.470024 0.695935 0.284381 0.701557 0.654990 0.422924 0.486878 ]';
% outcome_gm=[0.663424 0.654992 0.596282 0.381523 0.666511 0.044422 0.385813 0.476932 0.578912 0.428391 0.505657 0.426142 0.402233 0.090965 0.136251 0.303873 0.514178 0.366951 0.285246 0.108960 0.044696 0.218895 0.447720 0.302435 0.664142 0.558682 0.585698 0.601021 0.633735 0.641742 0.313060 0.354992 ]';

p = inputParser;   % Create an instance of the class.

% Arguments
p.addOptional('name', [], @ischar);
% p.addOptional('filename_cbf', [], @ischar);
% p.addOptional('filename_gm', [], @ischar);
p.addOptional('filename', [], @iscell);
p.addOptional('data','',@ischar);

% Parameters
p.addParamValue( 'data1', {'gm','cbf'}, @iscell);
% p.addParamValue( 'data2', 'cbf', @ischar);
p.addParamValue( 'study', 'IRIS', @ischar);  
p.addParamValue( 'use_dti_subjects', 0, @isnumeric);

% Parse input
p.parse( varargin{:} );

% Extract settings from input
name = p.Results.name;
% filename_cbf = p.Results.filename_cbf;
% filename_gm = p.Results.filename_gm;
filename = p.Results.filename;
data = p.Results.data;
data1 = p.Results.data1;
% data2 = p.Results.data2;
use_dti_subjects = p.Results.use_dti_subjects;
study=p.Results.study;

if exist('D:')==7
    base='C:\Users\Esther Bron\Documents\Project\Features_paper';
else
    base=fullfile('~','Project', 'Features_paper');
end
% filename_cbf=regexprep(filename_cbf, ['_' data1 '_' data2], '')
% filename_gm=regexprep(filename_gm, ['_' data1 '_' data2], '')

datastr=[];
for l=1:length(data1)
    datastr=[datastr '_' data1{l}];
end

outcome0=cell(length(data1),1);
outcome1=cell(length(data1),1);
for l=1:length(data1)
    filename{l}=regexprep(filename{l}, datastr, '');
    fid=fopen(filename{l},'r');
    outcome0{l} = fscanf(fid,'%f');
    outcome1{l}=[outcome0{l} 1-outcome0{l}];
    fclose(fid);
end

% fid=fopen(filename_cbf,'r');
% outcome_cbf = fscanf(fid,'%f');
% fclose(fid);
% 
% fid=fopen(filename_gm,'r');
% outcome_gm = fscanf(fid,'%f');
% fclose(fid);

% outcome_cbf1=[outcome_cbf 1-outcome_cbf];
% outcome_gm1=[outcome_gm 1-outcome_gm];

use_forward_selection=0;
use_kernel=0;

x=[];
for l=1:length(data1)
    switch data
        case 'product'
            if isempty(x)
                x=ones(size(outcome1{l}));
            end
            x=x.*outcome1{l};
        case 'mean'
            if isempty(x)
                x=zeros(size(outcome1{l}));
            end
            x=x+outcome1{l};
    end
end

if ismember(data,{'mean'})
    x=x/length(data1);
end
    

% switch data
%     case 'product'
%         x=outcome_cbf1.*outcome_gm1;
%     case 'mean'
%         x=(outcome_cbf1+outcome_gm1)/2;
% end
data=x./[sum(x')' sum(x')'];

outcome=do_classification('gm','region','study',study,'use_dti_subjects',num2str(use_dti_subjects),'make_curve','0','silent','1','no_exit','1');
outcome.data=data;

auc=1-testauc(outcome);
conmat=confmat(getlabels(outcome),labeld(outcome));
cmat(1:2,1:2)=0;
cmat(1:size(conmat,1),1:size(conmat,2))=conmat;

conmat

Acc=((cmat(1,1)+cmat(2,2))/(cmat(2,2)+cmat(2,1)+cmat(1,1)+cmat(1,2)));
Sens=cmat(2,2)/(cmat(2,2)+cmat(2,1)); %TP/(TP+FP)
Spec=cmat(1,1)/(cmat(1,1)+cmat(1,2)); %TN/(TN+FN)

result_file=fullfile(base, 'results.txt');
if ~exist(result_file,'file')
    fid=fopen(result_file,'w');
    fprintf(fid, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n','Name', 'Feature selection', 'Predefined kernel', 'Y', 'M', 'D', 'h', 'm', 's', 'Accuracy', 'Sensitivity', 'Specificity', 'Area Under Curve');
    fclose(fid);
end

fid=fopen(result_file,'a');
fprintf(fid, '%s\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%f\t%f\t%f\t%f\n',name, use_forward_selection, use_kernel,fix(clock),Acc, Sens, Spec, auc);
fclose(fid);

result={'Name', 'Feature selection', 'Predefined kernel', 'Clock', 'Accuracy', 'Sensitivity', 'Specificity', 'Area Under Curve'; name, use_forward_selection, use_kernel,fix(clock),Acc, Sens, Spec, auc}'

%Make ROC-curve
roc_e=roc_thr(outcome,2);
plot_roc_sens( roc_e, [name ' ( AUC= ' num2str(auc*100,3) ' )'], {'r-  '});