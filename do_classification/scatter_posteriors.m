function scatter_posteriors(varargin)
% outcome_cbf=[0.497701 0.400437 0.667803 0.674178 0.754082 0.308722 0.230679 0.422467 0.533200 0.430776 0.486989 0.468598 0.389762 0.418964 0.236025 0.337825 0.305053 0.338370 0.459140 0.475995 0.479335 0.441411 0.264780 0.286940 0.392398 0.470024 0.695935 0.284381 0.701557 0.654990 0.422924 0.486878 ]';
% outcome_gm=[0.663424 0.654992 0.596282 0.381523 0.666511 0.044422 0.385813 0.476932 0.578912 0.428391 0.505657 0.426142 0.402233 0.090965 0.136251 0.303873 0.514178 0.366951 0.285246 0.108960 0.044696 0.218895 0.447720 0.302435 0.664142 0.558682 0.585698 0.601021 0.633735 0.641742 0.313060 0.354992 ]';

p = inputParser;   % Create an instance of the class.

% Arguments
p.addOptional('clas_type', [], @ischar);
p.addOptional('filename_cbf', [], @ischar);
p.addOptional('filename_gm', [], @ischar);
p.addOptional('labelfile',[],@ischar);
p.addOptional('output_directory',[],@ischar);

% Parameters
p.addParamValue( 'data1', 'gm', @ischar);
p.addParamValue( 'data2', 'cbf', @ischar);
p.addParamValue( 'use_dti_subjects', 0, @isnumeric);
p.addParamValue( 'do_scatter', 1, @isnumeric);
p.addParamValue( 'do_SVM', 0, @isnumeric);

% Parse input
p.parse( varargin{:} );

% Extract settings from input
clas_type = p.Results.clas_type;
filename_cbf = p.Results.filename_cbf;
filename_gm = p.Results.filename_gm;
labelfile = p.Results.labelfile;
output_directory = p.Results.output_directory;
data1 = p.Results.data1;
data2 = p.Results.data2;
use_dti_subjects = p.Results.use_dti_subjects;
do_scatter=p.Results.do_scatter;
do_SVM=p.Results.do_SVM;

filename_cbf=regexprep(filename_cbf, ['_' data1 '_' data2], '')
filename_gm=regexprep(filename_gm, ['_' data1 '_' data2], '')

fid=fopen(filename_cbf,'r');
outcome_cbf = fscanf(fid,'%f');
fclose(fid);

fid=fopen(filename_gm,'r');
outcome_gm = fscanf(fid,'%f');
fclose(fid);

outcome_cbf1=[outcome_cbf 1-outcome_cbf];
outcome_gm1=[outcome_gm 1-outcome_gm];

subjects=tdfread1(labelfile);
label=get_labels(subjects.label, 'use_dti_subjects', use_dti_subjects);
patients=subjects.label(label=='P',:);
score=certainty_scores(patients);
s=num2str(score);

size_score=score;

for i=1:length(score)
    switch score(i)
        case 0 % no label
            size_score(i)=1;
        case 1 % probable AD
            size_score(i)=5;
        case 2 % possible AD
            size_score(i)=3;
        case 3 % AD/FTD
            size_score(i)=1;
        case 4 % possible FTD
            size_score(i)=3;
    end
end
        
score

if do_scatter
    figure, a=scatter(outcome_gm1(find(ismember(label,'P'))',2),outcome_cbf1(find(ismember(label,'P'))',2),30*size_score+5,score,'filled')
    ach=get(a,'children');
    colormap(autumn(6))
%     ch = colorbar('YLim',[0 5],...                        &# The axis limits
%               'YTick',[.5:4/5:5.5],...                    %# The tick locations
%               'YTickLabel',{'No label','Probably AD','Possibly AD','AD/FTD','Possibly FTD','Probably FTD'});  %# The tick labels
    
    
%     colorbar('YTick', [5/12:5/6:6-5/12])
%     colorbar('YTickLabel',{'No label','Probably AD','Possibly AD','AD/FTD','Possibly FTD','Probably FTD'})
    hold on
    b=scatter(outcome_gm1(find(ismember(label,'C'))',2),outcome_cbf1(find(ismember(label,'C'))',2),'sb','filled')
    
    y=1-[0:.01:1];
    hold on
    c=plot(0:0.01:1,y,'g','LineWidth',2)
    
    if ismember(clas_type,'vbm')
        clas_type='voxel';
    end
    xlabel([clas_type ' - ' data1 ' posterior probabilities'],'FontSize',16)
    ylabel([clas_type ' - ' data2 ' posterior probabilities'],'FontSize',16)
    if use_dti_subjects
        l=legend([ach([28 30 29 31 27 ]); b; c],{'Probable FTD','Possible FTD', 'AD/FTD', 'Possible AD', 'Probable AD','Controls','y=1-x (product rule)'},'Location','EastOutside')
    else        
        l=legend([ach([29 31 30 32 28 ]); b; c],{'Probable FTD','Possible FTD', 'AD/FTD', 'Possible AD', 'Probable AD','Controls','y=1-x (product rule)'},'Location','EastOutside') %, 'Unlabeled patient'
    end
    
    msizes=[5 3 1 3 5];
    M = findobj(l,'type','patch') % Find objects of type 'patch'
    for i=1:5
    set(M(i),'MarkerSize', sqrt(30*msizes(6-i)+5) )
    end
    
    set(gcf, 'Position', [200 200 1000 600])
    set(gca, 'Position', [0.1300    0.1100    1.3*0.4271    0.8150])


end

if do_SVM
    a=[outcome_gm1(:,2) outcome_cbf1(:,2)]
    % hold off
    name='SVM on posterior probabilities'
    style={'g-  '};
    a_norm=zscore1(a);
    A=dataset(double(a_norm),label)
    outcome=crossvalidation(A,0, name, style, output_directory, 'use_kernel',1)
 end

% use_forward_selection=0;
% use_kernel=0;
% 
% x=outcome_cbf1.*outcome_gm1;
% data=x./[sum(x')' sum(x')'];
% 
% outcome=do_classification(1,1,0,0,0);
% outcome.data=data;
% 
% %Make ROC-curve
% roc_e=roc_thr(outcome,2);
% plot_roc_sens( roc_e, name, {'r-  '});
% 
% auc=1-testauc(outcome);
% conmat=confmat(getlabels(outcome),labeld(outcome));
% cmat(1:2,1:2)=0;
% cmat(1:size(conmat,1),1:size(conmat,2))=conmat;
% 
% Acc=((cmat(1,1)+cmat(2,2))/(cmat(2,2)+cmat(2,1)+cmat(1,1)+cmat(1,2)))
% Sens=cmat(2,2)/(cmat(2,2)+cmat(2,1)) %TP/(TP+FP)
% Spec=cmat(1,1)/(cmat(1,1)+cmat(1,2)) %TN/(TN+FN)
% 
% base='C:\Users\Esther Bron\Documents\Project\Features';
% result_file=fullfile(base, 'results.txt');
% if ~exist(result_file,'file')
%     fid=fopen(result_file,'w');
%     fprintf(fid, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n','Name', 'Feature selection', 'Predefined kernel', 'Y', 'M', 'D', 'h', 'm', 's', 'Accuracy', 'Sensitivity', 'Specificity', 'Area Under Curve');
%     fclose(fid);
% end
% 
% fid=fopen(result_file,'a');
% fprintf(fid, '%s\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%f\t%f\t%f\t%f\n',name, use_forward_selection, use_kernel,fix(clock),Acc, Sens, Spec, auc);
% fclose(fid);
% 
% result={'Name', 'Feature selection', 'Predefined kernel', 'Clock', 'Accuracy', 'Sensitivity', 'Specificity', 'Area Under Curve'; name, use_forward_selection, use_kernel,fix(clock),Acc, Sens, Spec, auc}'