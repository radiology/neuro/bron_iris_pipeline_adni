function score=certainty_scores(patient_cell)

A=tdfread1('C:\Users\Esther Bron\Documents\Data\IRIS_001-040_voorlopige_zekerheidsscores_20121030.txt');
score=zeros(size(patient_cell,1),1);
for i=1:size(patient_cell,1)
    patient=patient_cell(i,:);
    patientnr=str2num(patient(length(patient)-2:length(patient)));
    indx=find(ismember(A.IRIS_,patientnr));
    score(i)=A.Zekerheidsscore(indx);
end