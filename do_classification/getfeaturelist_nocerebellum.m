function [region_o, output,order]=getfeaturelist_nocerebellum(f)

file=['C:\Users\Esther Bron\Documents\Project\Regions_features', filesep, 'regions.txt'];
    fid=fopen(file,'r');
    t=fscanf(fid, '%s');
    fclose(fid);
    region=cell(83,1);
    count=0;
    for j=1:length(t)
        if ismember(t(j),'A':'Z')
            count=count+1;
        end
        region{count}=[region{count} t(j)];
    end
%     region2={region{1:8},region{10:35},region{37:48},region{50:75},region{77:80}}';
    
    region2=region
    region2{9}=[];  % Cerebellum left
    region2{49}=[]; % Cerebellum right
    region2{22}=[]; % Lateral ventricle, frontal horn, central part and occipital horn left
    region2{23}=[]; % Lateral ventricle, temporal horn left
    region2{62}=[]; % Lateral ventricle, frontal horn, central part and occipital horn right
    region2{63}=[]; % Lateral ventricle, temporal horn right
    region2{36}=[]; % Substantial nigra left
    region2{76}=[]; % Substantial nigra right
    region2{81}=[]; % Brainstem, spans the midline
    region2{82}=[]; % Corpus callosum
    region2{83}=[]; % Third ventricle
    region3=region2(~cellfun('isempty', region2))
    
%     region3=cell(36,1);
%     for j=1:36
%         region3{j}=['|L-R| (' region2{j}(1:length(region2{j})-4) ')'];
%     end
%     region4=cell(36,1);
%     for j=1:36
%         region4{j}=['|L-R|/(L+R) (' region2{j}(1:length(region2{j})-4) ')'];
%     end;

%     region_new=region2{:}';
    [output,order]=sort(f,'descend');
    
    region_o={region3{order}}';