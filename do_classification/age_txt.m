function [a,label] = age_txt(filename,use_old_subjects)

if size(filename,1)==2
    A1=tdfread1(filename(1,:));
    A2=tdfread1(filename(2,:));
    fn=fieldnames(A1)';
    for f=fn
        f=char(f);
        if strcmp(f,'Median')
            A.(f)=[A1.(f)(:,1:11)' A2.(f)(:,1:11)']';
        else
            A.(f)=[A1.(f)' A2.(f)']';                    
        end        
    end
else
    A=tdfread1(filename);
end

if use_old_subjects
    old_subjects=   {'FIAT_004','FIAT_005','FIAT_007','FIAT_009','FIAT_011','IRIS_902','IRIS_903','IRIS_906','IRIS_907','IRIS_908','IRIS_910','IRIS_911','IRIS_001','IRIS_002','IRIS_003','IRIS_004','IRIS_005','IRIS_009','IRIS_010','IRIS_012','IRIS_014','IRIS_015','IRIS_017','IRIS_018','IRIS_019','IRIS_020','IRIS_021','IRIS_022','IRIS_023','IRIS_024','IRIS_025','IRIS_026'};                                

    [indx,indy]=find(ismember(A.Subject,old_subjects)==1);
    A=structfun( @(M) M(indx,:) ,A,'Uniform',0);
end

nrfeat=1;
subjects=unique(A.Subject,'rows');
nrsub=size(subjects,1);

if any(strcmp('Age',fieldnames(A)))
    a=reshape(A.Age,nrfeat,nrsub)';
end

label=get_labels(subjects);
