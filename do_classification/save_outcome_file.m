function save_outcome_file(base,name, outcome, label)

direc=[base, filesep, name];
file=[direc, filesep, 'outcome_file.txt']
textfile.ID=label;
textfile.output=labeld(outcome);
textfile.pCN = zeros(size(outcome,1),1);
textfile.pMCI = zeros(size(outcome,1),1);
textfile.pAD = zeros(size(outcome,1),1);
o=+outcome;
for i=1:2
    if ismember(outcome.featlab(i),{'A'})
       textfile.pAD=o(:,i);
    elseif ismember(outcome.featlab(i),{'C'}) 
       textfile.pCN=o(:,i);
    elseif ismember(outcome.featlab(i),{'M','N'}) 
       textfile.pMCI=o(:,i);
    end
end
tdfwrite(file,textfile);
