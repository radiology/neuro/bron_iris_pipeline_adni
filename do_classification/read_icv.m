function icvs=read_icv(subject_label, directory)
%Read intracranial volume

icvfolder = fullfile(directory, 'icv');
icvs=zeros(length(subject_label),1);
for j=1:size(subject_label,1)
	subject=strtrim(subject_label(j,:));
	icvfile=fullfile(icvfolder, [subject '.txt']);
    fid=fopen(icvfile,'r');
    icv=fscanf(fid,'%i');
    fclose(fid);
    icvs(j)=icv;
end