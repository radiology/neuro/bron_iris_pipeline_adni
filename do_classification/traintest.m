function [outcome, name, Acc, Sens, Spec] = traintest(varargin)
warning('off','MATLAB:nearlySingularMatrix')
warning('off','MATLAB:singularMatrix')

p = inputParser;   % Create an instance of the class.

% Arguments
p.addOptional('A', [], @(in) isa( in, 'dataset' ) || isempty( in ) || iscell( in ) );
p.addOptional('train_index', [], @isnumeric);
p.addOptional('name', [], @ischar);
p.addOptional('style', [], @iscell);
p.addOptional('base','',@ischar);
p.addOptional('info','',@iscell);

% Parameters
p.addParamValue( 'use_forward_selection', 0, @isnumeric);
p.addParamValue( 'selection_method', 'maha', @ischar);
p.addParamValue( 'input_matrix', [], @isnumeric);
p.addParamValue( 'use_kernel', 0, @isnumeric);
p.addParamValue( 'probability_est', 0, @isnumeric);
p.addParamValue( 'use_auc', 0, @isnumeric);
p.addParamValue( 'svm_parameters', {}, @iscell);
p.addParamValue( 'num_max_perms', 5000, @isnumeric);
p.addParamValue( 'use_random',0, @isnumeric );
p.addParamValue( 'max_iterations',1, @isnumeric);
p.addParamValue( 'nfold_c', 5, @(in) isnumeric( in ) && floor( in ) == in && ( in > 1 || in < 0 ) );
p.addParamValue( 'histreg',0, @isnumeric);
p.addParamValue( 'mask',struct, @isstruct)
p.addParamValue( 'make_curve',1,@isnumeric);
p.addParamValue( 'silent', 0, @isnumeric);
p.addParamValue( 'cmin',-5, @isnumeric);
p.addParamValue( 'fixed_c_perm', 1, @isnumeric);
p.addParamValue( 'sign_level', 0.05, @isnumeric);
p.addParamValue( 'ls_svc',0, @isnumeric);
p.addParamValue( 'tmap_directory', [], @ischar);
p.addParamValue( 'confounders', [], @isnumeric);

% Parse input
p.parse( varargin{:} );

% Extract settings from input
A   = p.Results.A;
train_index = p.Results.train_index;
name = p.Results.name;
style = p.Results.style;
base = p.Results.base;
info = p.Results.info;
use_forward_selection = p.Results.use_forward_selection;
selection_method = p.Results.selection_method;
input_matrix = p.Results.input_matrix;
use_kernel = p.Results.use_kernel;
probability_est = p.Results.probability_est;
use_auc = p.Results.use_auc;
svm_parameters = p.Results.svm_parameters;
num_max_perms = p.Results.num_max_perms;
use_random        = p.Results.use_random;
max_iterations     = p.Results.max_iterations;
nfold_c           = p.Results.nfold_c;
histreg           = p.Results.histreg;
mask              = p.Results.mask;
make_curve        = p.Results.make_curve;
silent            = p.Results.silent;
cmin              = p.Results.cmin;
fixed_c_perm      = p.Results.fixed_c_perm;
sign_level        = p.Results.sign_level;
ls_svc            = p.Results.ls_svc;
tmap_directory    = p.Results.tmap_directory;
confounders       = p.Results.confounders;
clear varargin
clear p

if use_forward_selection
    name=[name '_featsel_' selection_method];
end
if ismember(selection_method,{'roi'})
    name=[name num2str(use_forward_selection)];
end
if input_matrix
    name=[name '_inputmatrix'];
end    
if use_kernel
    name=[name '_kernel'];
end
if ~(isempty(svm_parameters) || isempty(svm_parameters{1}))
    name=[name '_C' num2str(svm_parameters{1})];
end

ntrain=size(+A,1);
nrfeat=size(+A,2);
nrclass=numel(A.lablist{1});

if length(train_index)==1
    nfold=train_index;
else 
    nfold=1;
end

if nfold>1
    A=seldat(A,1:nrclass); 
    
    classsize=struct([]);
    foldmat=struct([]);
    for n=1:nrclass
        classsize{n}=sum(A.nlab==n);
    
        fold=0:ceil(classsize{n}/nfold):classsize{n};
        fold(nfold+1)=ceil(classsize{n});
        foldmat{n}(1:ceil(classsize{n}),1:nfold)=0;

        for s=1:1:nfold 
            foldmat{n}(fold(s)+1:fold(s+1),s)=1;
        end
    end

    s = RandStream.create('mt19937ar','seed',sum(100^2));
    RandStream.setGlobalStream(s);
    reset(s); 
end

if max_iterations>0
    Accs=zeros(max_iterations,1);
    Senss=zeros(max_iterations,1);
    Specs=zeros(max_iterations,1);
    aucs=zeros(max_iterations,1);
end
    
for nr_iteration=1:max_iterations
    if nfold>1
        foldmat_class=struct([]);  
        for n=1:nrclass; 
            foldmat_class{n}=foldmat{n}(randperm(max(size(foldmat{n}))),:);
        end
        foldmat_class=[foldmat_class{1}; foldmat_class{2}];
    end 
        
    % Start crossvalidation
    roc_e=[];
    Error_crossval=[];
    Est_labels=[];
    Real_labels=[];
    outcome = [];
    gamma = [];

    test_subset=1:ntrain; 

    nrtest=length(test_subset);
    wsel_array=cell(nrtest,1);
    thresh=[];
    c=[];

    % nr_sel_feat=29;

    direc=[base, filesep, name];
    if exist(direc,'dir')~=7
        mkdir(direc)
    end

    if use_kernel
        linearKernel=@(X)X*X';
        KernelA=linearKernel(+A);
    end

    %Table with results
    featselresults = struct('Y', {}, 'R', {}); %creates an empty structure with fields
    numselfeatures =[ ];
    
  %         % Use crossvalidation -> sampling N different train/test sets.
%         if isempty(train_index)
%             for class=1:nrclass
%                class_subset=find(A.nlab==class); 
% 
%                % Resample train_index
%                class_subset=class_subset(randperm(max(size(class_subset))));
%                train_index=sort([train_index; class_subset(1:ceil(length(class_subset)/2))]);
%             end
%         end

    for i=1:1:nfold %
        i

        filetemp=[direc, filesep, 'outcome_temp_' num2str(i) '.txt'];
        file_c=[direc, filesep, 'c_' num2str(i)];
        file_sv=[direc, filesep, 'sv_' num2str(i)];
    %     file_sv_featsel=[direc, filesep, 'sv_featsel_' num2str(i)];
%         file_test_index=[direc, filesep, 'test_index_' num2str(i)];

        
        % Create train set
        if nfold>1
            setA=find(~foldmat_class(:,i));
            setTest=find(foldmat_class(:,i));
        else           
            setA=train_index;
%             test_index=test_subset;
            test_subset(train_index)=[];
            
        end

        if ~use_kernel
            train=A(setA,:);   
            if nfold>1
                test=A(setTest,:);
            else
                test=A(test_subset,:);
            end
        end

        if use_forward_selection && ~ismember(selection_method,{'roi'})

            train=A(setA,:);   
            test=A;
            test(setA,:)=[];

            train_original=train;
            test_original=test;

            switch selection_method
                case 'maha'
                    [wsel,r]=featself(train_original,'maha-s')
                case 'svm'
                    [wsel,r]=featself(train_original,libsvmc2([],'linear',{},'svm_parameters',{.1}))
                case 'svm-auc'
                    [wsel,r]=featsellr_auc(train_original,libsvmc2([],'linear',{},'svm_parameters',{.1}),[],1,0)                
                case 'eucl'
                    [wsel,r]=featself(train_original,'eucl-s')
                case 'maha-lr'
                    [wsel,r]=featsellr(train_original,'maha-s',[],2,1)
                case 'svm-lr'
                    [wsel,r]=featsellr(train_original,libsvmc2([],'linear',{},'svm_parameters',{.1}),[],2,1)
                case 'svm-lr-auc'
                    [wsel,r]=featsellr_auc(train_original,libsvmc2([],'linear',{},'svm_parameters',{.1}),[],2,1)                
                case 'eucl-lr'
                    [wsel,r]=featsellr(train_original,'eucl-s',[],2,1)
                case 'pmap'
                case 'ttest'
                case 'tmap'
                case 'rfe'
             end


            featselresults(i).Y=wsel;          

            if ~ismember(selection_method,{'pmap','tmap','rfe'})            
                featselresults(i).R=r;

                file=[direc, filesep, num2str(i), '.txt']
                fid=fopen(file,'w')
                fprintf(fid, '%f', length(wsel.data{1}))    
                for j=1:size(r,1)
                    for k=1:size(r,2)
                        fprintf(fid, '%f ',r(j,k));
                    end
                    fprintf(fid, '\n');
                end
                fclose(fid);            
            end

            wsel_array{i}=wsel.data{1};

            train=train_original*wsel;
            test=test_original*wsel;

            %Save the number of features selected in a table
            numselfeatures(i)=numel(+wsel);

            if use_kernel && use_forward_selection          
                Asel=A*wsel;
                KernelA=linearKernel(+Asel);
            else
                train=train_original*wsel;
                test=test_original*wsel;
            end        
        else
            numselfeatures(i)=nrfeat;
        end

        if use_kernel
            if ~isempty(confounders)
                display('Correct for confounders')
                KernelA=confounder_correction_by_kernel_regression(KernelA,confounders);
            end
            TrainKernelA = KernelA(setA,setA);
            if nfold>1
                TestKernelA = KernelA(setTest,setA);
            else
                TestKernelA = KernelA(:,setA);
                TestKernelA(setA,:)=[];
            end

            labels=getlabels(A);
            TrainLabels=labels(setA);
            if nfold>1
                TestLabels=labels(setTest);
            else
                TestLabels=labels;
                TestLabels(setA)=[];
            end

            train=dataset(TrainKernelA, TrainLabels);
            test= dataset(TestKernelA, TestLabels);
        end  

        if input_matrix~=[] % (ttest selection)
            train_original=train;
            test_original=test;

            Ti_stat=zeros(ntrain,nrfeat);
            min_ti=zeros(nrfeat,1);
    %         for j=1:nrfeat % Features
            min_ti=min(input_matrix([1:i-1, i+1:ntrain],:));
    %         end
            [region, output, order]=getfeaturelist_nocerebellum(min_ti);
    %         [region, output, order]=getfeaturelist(min_ti);

            wsel=mapping('featsel','fixed', order(1:nr_sel_feat),zeros(1,1),nrfeat,nr_sel_feat);
            wsel.name='Ttest FeatSel';
            wsel_array{i}=wsel.data{1};
            train=train_original*wsel;
            test=test_original*wsel;
        end

        if ls_svc
            svm_type=5;
        else
            svm_type=0;
        end
        if use_kernel
            w = libsvmc2_kfold(train, 'precomputed', {}, 'probability_est', probability_est, 'svm_parameters', svm_parameters, 'nfold', nfold_c, 'use_auc', use_auc, 'use_random', use_random, 'nr_iterations', max_iterations, 'cmin', cmin, 'svm_type', svm_type);
        else
            w = libsvmc2_kfold(train, 'linear', {}, 'probability_est',probability_est, 'svm_parameters', svm_parameters, 'nfold', nfold_c, 'use_auc', use_auc, 'use_random', use_random, 'nr_iterations', max_iterations, 'cmin', cmin, 'svm_type', svm_type);
        end

        thresh=[thresh  w.data{1}.rho];
        c=[c w.data{2}.used_svm_parameters];
        sv= w.data{1}.SVs;

    %     % Test performance on testing set   
        o=libsvmm2(test, w);
        +o;
        outcome=[outcome; o];

        fid=fopen(filetemp,'a');
        fprintf(fid, '%f ',+o(1));
        fclose(fid);

%         Error_crossval=[Error_crossval; testd(o,1)];
%         Est_labels=[Est_labels; labeld(o)];
%         Real_labels=[Real_labels; getlabels(test)];    
    %     Error_crossval=[Error_crossval; testd(test*w,1)];
    %     Est_labels=[Est_labels; labeld(test*w)];
    %     Real_labels=[Real_labels; getlabels(test)];
        gamma=[gamma w.data{2}.used_kernel_parameters];
    end
    +outcome;
    thresh';
    c';

    save(file_c, 'c');
    save(file_sv, 'sv');
%     save(file_test_index, 'test_index');

    if use_forward_selection
        save([direc, filesep, selection_method, '_RY.mat'], 'featselresults' );
        save([direc, filesep, selection_method, '_nrfeatures.mat'], 'numselfeatures' );
    end

    if or(use_forward_selection==1 ,input_matrix~=[])
    %Make histogram
        x=[];
        for i=1:length(wsel_array)
           x=[x wsel_array{i}];
        end
        x'
        f=hist(x,nrfeat) 

        [region_o, output, order]=getfeaturelist_nocerebellum(f)
    %     [region_o, output, order]=getfeaturelist(f);

        region_o;

        file=[direc, filesep, 'regions.txt'];
        fid=fopen(file,'w');
        for j=1:size(region_o,1)
            fprintf(fid, '%s ',char(region_o(j)));
        end
        fclose(fid);

        pos = get(0, 'ScreenSize');
        figure('Position',pos),bar(output)
        set(gca, 'XTick', 1:nrfeat, 'XTickLabel', region_o)
        xticklabel_rotate
        title(['Frequency of selected regions in classification of dementia based on ' name])

        figure('Position',pos),bar(output(1:nrfeat/2))
        set(gca, 'XTick', 1:nrfeat/2, 'XTickLabel', region_o(1:nrfeat/2))
        xticklabel_rotate
        title(['Frequency of selected regions in classification of dementia based on ' name])

        l=[];
        for i=1:nrtest
            l=[l; length(wsel_array{i})];
        end
        mean(l);
        std(l);

    end

    Error_crossval=testd(outcome,1);
    Est_labels=labeld(outcome);
    Real_labels=getlabels(outcome);
    
    %Area under the curve (AUC)
    auc=1-testauc(outcome);

    %Where did it go wrong?
    Error = Error_crossval;

    %Print estimated and real labels
    if ~silent
        [Est_labels'; Real_labels']
    end

    %Calculate Error
    Acc=1-(sum(Error_crossval)/size(Error_crossval,1));

    %Calculate Sensitivity and Specificity
    try         
        conmat=confmat(Real_labels,Est_labels);
        if ~silent
            conmat
        end
        cmat(1:2,1:2)=0;
        cmat(1:size(conmat,1),1:size(conmat,2))=conmat;
    catch
        cmat=ones(2);
    end

    Sens=cmat(2,2)/(cmat(2,2)+cmat(2,1)); %TP/(TP+FP)
    Spec=cmat(1,1)/(cmat(1,1)+cmat(1,2)); %TN/(TN+FN)

    % Accuracy(nr_sel_feat)=Acc;
    % Sensitivity(nr_sel_feat)=Sens;
    % Specificity(nr_sel_feat)=Spec;

    file=[direc, filesep, 'outcome.txt'];
    fid=fopen(file,'w');
    for j=1:size(outcome,1)
        fprintf(fid, '%f ',outcome(j));
    end
    fclose(fid);

    file=[direc, filesep, 'real_labels.txt'];
    fid=fopen(file,'w');
    for j=1:size(Real_labels,1)
        fprintf(fid, '%s ',Real_labels(j));
    end
    fclose(fid);

    file=[direc, filesep, 'estimated_labels.txt'];
    fid=fopen(file,'w');
    for j=1:size(Est_labels,1)
        fprintf(fid, '%s ',Est_labels(j));
    end
    fclose(fid);

    %Make ROC-curve
    if min(ismember(Real_labels,'ACPMN')) %All test samples should have a Real Label to make an ROC curve
       if exist('style') 
            roc_e=roc_thr(outcome,2);
            [maxsum,x]=max((1-roc_e.error)+(1-roc_e.xvalues));
            save([direc, filesep, 'roc_e'], 'roc_e' );
            specmax=1-roc_e.error(x);
            sensmax=1-roc_e.xvalues(x);
            if make_curve
                plot_roc_sens( roc_e, [name ' ( AUC=' num2str(auc*100,3) ' )'], style);
            end
       end
        
        nrfeat=median(numselfeatures);

        if nfold>1
            result_file=fullfile(base, ['results_' num2str(nfold) 'fold.txt']);
        else
            result_file=fullfile(base, 'results.txt');
        end
        if ~exist(result_file,'file')
            fid=fopen(result_file,'w');
            fprintf(fid, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n','Name', 'Number of features', 'Predefined kernel', 'Y', 'M', 'D', 'h', 'm', 's', 'Accuracy', 'Sensitivity', 'Specificity', 'Area Under Curve', 'Max Sens+Spec', 'Max Sens', 'Max Spec','Data','Features','Data1','Subjectset','Study','Selection method','Robust C');
            fclose(fid);
        end

        fid=fopen(result_file,'a');
        fprintf(fid, '%s\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%s\t%s\t%s\t%s\t%s\t%s\t%i\n',name, nrfeat, use_kernel,fix(clock),Acc, Sens, Spec, auc, maxsum, sensmax, specmax, info{:});
        fclose(fid);

        if ~silent
            result={'Name', 'Number of features', 'Predefined kernel', 'Clock', 'Accuracy', 'Sensitivity', 'Specificity', 'Area Under Curve', 'Max Sens+Spec', 'Max Sens', 'Max Spec'; name, nrfeat, use_kernel,fix(clock),Acc, Sens, Spec, auc, maxsum, sensmax, specmax}'
        end

        if nfold>1
            Accs(nr_iteration)=Acc;
            Senss(nr_iteration)=Sens;
            Specs(nr_iteration)=Spec;
            aucs(nr_iteration)=auc
        end
    end
    
end

if nfold>1
    result_file=fullfile(base, [num2str(nfold) 'fold_iteration.txt']);
    if ~exist(result_file,'file')
        fid=fopen(result_file,'w');
        fprintf(fid, '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n','Name', 'Y', 'M', 'D', 'h', 'm', 's', 'Mean AUC', 'Std AUC', 'CV AUC', 'Min AUC', 'Max AUC', 'Mean Accuracy', 'Std Accuracy', 'CV Accuracy', 'Min Accuracy', 'Max Accuracy');
        fclose(fid);
    end

    fid=fopen(result_file,'a');
    fprintf(fid, '%s\t%i\t%i\t%i\t%i\t%i\t%i\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n',name, fix(clock),mean(aucs), std(aucs), std(aucs)/mean(aucs), min(aucs), max(aucs),mean(Accs), std(Accs), std(Accs)/mean(Accs), min(Accs), max(Accs));
    fclose(fid);

    result={'Name', 'Mean AUC', 'Std AUC', 'Min AUC', 'Max AUC', 'Mean Accuracy', 'Std Accuracy', 'Min Accuracy', 'Max Accuracy'; name, mean(aucs), std(aucs), min(aucs), max(aucs),mean(Accs), std(Accs), min(Accs), max(Accs)}'
end
