function [a,label, train_index, subjects] = open_region_txt(filename,use_old_subjects,use_dti_subjects, dti)

if size(filename,1)==2
    A1=tdfread1(filename(1,:));
    A2=tdfread1(filename(2,:));
    fn=fieldnames(A1)';
    for f=fn
%         f=char(f);
%         if strcmp(f,'Median')
%             A.(f)=[A1.(f)(:,1:11)' A2.(f)(:,1:11)']';
%         else
%             minA=min(size(A1.(f),2),size(A2.(f),2));
%             A.(f)=[A1.(f)(:,1:minA)' A2.(f)(:,1:minA)']';                    
%         end        
        f=char(f);  
        diff=size(A1.(f)',1)-size(A2.(f)',1);
        blank=blanks(size(A1.(f),1))';
        if diff<0   
            blank=blanks(size(A1.(f),1))';
            for i=1:abs(diff)
                A1.(f)=[A1.(f) blank];
            end
        elseif diff>0
            blank=blanks(size(A2.(f),1))';
            for i=1:abs(diff)
                A2.(f)=[A2.(f) blank];
            end
        end
        A.(f)=[A1.(f)' A2.(f)']';  
    end
else
    A=tdfread1(filename);
end

[indx,indy]=find(sum(ismember(A.Subject,'Subject'),2)<=6);
A=structfun( @(M) M(indx,:) ,A,'Uniform',0);

remove_list=   {'Cerebellum right',
                'Cerebellum left',
                'Brainstem (spans the midline)',                  
                'Lateral ventricle, frontal horn, central part, and occipital horn right',     
                'Lateral ventricle, frontal horn, central part, and occipital horn left',      
                'Lateral ventricle, temporal horn right',
                'Lateral ventricle, temporal horn left',
                'Third ventricle',
                'Rest'};
            
remove_list_wm={'Corpus callosum',
                'Substantia nigra left', 
                'Substantia nigra right'};       
                
[indx,indy]=find(sum(ismember(A.ROI_name,remove_list),2)==0);
A=structfun( @(M) M(indx,:) ,A,'Uniform',0);

if ~dti
    [indx,indy]=find(sum(ismember(A.ROI_name,remove_list_wm),2)==0);
    A=structfun( @(M) M(indx,:) ,A,'Uniform',0);
end

if use_old_subjects
    old_subjects=   {'FIAT_004','FIAT_005','FIAT_007','FIAT_009','FIAT_011','IRIS_902','IRIS_903','IRIS_906','IRIS_907','IRIS_908','IRIS_910','IRIS_911','IRIS_001','IRIS_002','IRIS_003','IRIS_004','IRIS_005','IRIS_009','IRIS_010','IRIS_012','IRIS_014','IRIS_015','IRIS_017','IRIS_018','IRIS_019','IRIS_020','IRIS_021','IRIS_022','IRIS_023','IRIS_024','IRIS_025','IRIS_026'};                                

    [indx,indy]=find(ismember(A.Subject,old_subjects)==1);
    A=structfun( @(M) M(indx,:) ,A,'Uniform',0);
end

%%% by Sandrine %%%
% if use_iris_subjects
%     iris_subjects={'IRIS_001','IRIS_002','IRIS_003','IRIS_004','IRIS_005','IRIS_006','IRIS_008','IRIS_009','IRIS_010','IRIS_012','IRIS_014','IRIS_017','IRIS_018','IRIS_019','IRIS_020','IRIS_021','IRIS_022','IRIS_023','IRIS_024','IRIS_025','IRIS_026','IRIS_028','IRIS_029','IRIS_030','IRIS_031','IRIS_032','IRIS_033','IRIS_034','IRIS_035','IRIS_036','IRIS_038','IRIS_801','IRIS_802','IRIS_803','IRIS_804','IRIS_805','IRIS_806','IRIS_807','IRIS_901','IRIS_902','IRIS_903','IRIS_906','IRIS_907','IRIS_908','IRIS_909','IRIS_910','IRIS_911','IRIS_913','IRIS_915','IRIS_916','IRIS_918','IRIS_919','IRIS_920','IRIS_921'}; 
%     
%     [indx,indy]=find(ismember(A.Subject,iris_subjects)==1);
%     A=structfun( @(M) M(indx,:) ,A,'Uniform',0);
% end
%%% ----------- %%%

if use_dti_subjects
    none_dti_subjects={'IRIS_015'};                                

    [indx,indy]=find(sum(ismember(A.Subject,none_dti_subjects),2)==0);
    A=structfun( @(M) M(indx,:) ,A,'Uniform',0);
end

features=unique(A.ROI_name,'rows');
nrfeat=size(features,1);
subjects=unique(A.Subject,'rows');
nrsub=size(subjects,1);


if any(strcmp('Volume',fieldnames(A)))
    A.Volume=str2num(A.Volume);
    a=reshape(A.Volume,nrfeat,nrsub)';
elseif any(strcmp('Mean_probability',fieldnames(A)))
    A.Mean_probability=str2num(A.Mean_probability);
    a=reshape(A.Mean_probability,nrfeat,nrsub)';
elseif any(strcmp('Mean',fieldnames(A)))
    A.Mean=str2num(A.Mean);
    a_1=reshape(A.Mean,nrfeat,nrsub)';
    A.Normalized_mean=str2num(A.Normalized_mean);
    a_norm=reshape(A.Normalized_mean,nrfeat,nrsub)';
    a={a_1, a_norm};
end

[subjects, label, train_index, remove_list]=get_labels(subjects);
a(remove_list,:)=[];
