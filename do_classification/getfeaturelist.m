function [region_o, output,order]=getfeaturelist(f)

file=['C:\Users\Esther Bron\Documents\Project\Features', filesep, 'regions.txt'];
    fid=fopen(file,'r')
    t=fscanf(fid, '%s');
    region=cell(83,1);
    count=0;
    for j=1:length(t)
        if ismember(t(j),'A':'Z')
            count=count+1;
        end
        region{count}=[region{count} t(j)];
    end
    region2={region{1:35},region{37:75},region{77:80}}';
    region3=cell(37,1);
    for j=1:39
        region3{j}=['|L-R| (' region2{j}(1:length(region2{j})-4) ')'];
    end
    region4=cell(37,1);
    for j=1:39
        region4{j}=['|L-R|/(L+R) (' region2{j}(1:length(region2{j})-4) ')'];
    end

    region_new={region2{:}, region3{:}, region4{:}}';
    
    [output,order]=sort(f,'descend');
    region_o={region_new{order}}';