function [ages, genders] = read_age_gender(subject_label, directory)

% directory=fullfile(data_drive, study)
possible_filenames={'age_gender.txt','train.txt','test.txt'};
ages=zeros(length(subject_label),1);
genders=zeros(length(subject_label),1);
for i=1:numel(possible_filenames)
    filename=possible_filenames{i};
    if ~exist(fullfile(directory, filename),'file')
        continue
    end
    content=tdfread(fullfile(directory, filename));

    if ~isempty(content)
        for j=1:size(subject_label,1)
            subject=strtrim(subject_label(j,:));
            subject_split=strsplit(subject,'_');
            filename_split=strsplit(filename,'.');
            if ismember(subject_split{1},{'train','test'}) && ~ismember(subject_split{1},filename_split)
                continue
            end

            for i_infile=1:length(content.ID)
                subject_infile=strtrim(content.ID(i_infile,:));
                if length(subject)~=length(subject_infile) 
                    continue
                elseif subject==subject_infile
                    age=round(content.Age(i_infile));
                    if isfield(content, 'Gender')
                        gender=content.Gender(i_infile,:);
                        if ismember(strtrim(gender),{'Male'})
                            sex=1;
                        else 
                            sex=0;
                        end
                    elseif isfield(content, 'Sex')
                        sex=content.Sex(i_infile,:);
                    end
                        
                    break
                end
            end
            
            if ~exist('age','var')
                error(['Error: no age for subject ' subject])
            else
                ages(j)=age;
                genders(j)=sex;
%                 display([subject ': ' num2str(sex) ' - ' num2str(age)])
            end
            
        end
        
    end
    
end
