#!/bin/env python
#
# Calculate features 
# construct_voxelwise_features.py
# Call from other python script with: construct_voxelwise_features.main( data_dir, cc, threads, work_subdir, performance, nomask, use_hard_segmentation )
# Or from command line with:  python construct_voxelwise_features.py input_dir use_hard_segmentation


# July 2013-August 2014
# Esther Bron - e.bron@erasmusmc.nl 
#
import os
import subprocess
import re
import sys
import datetime
import time
import shutil
import math
import numpy
import operator
from pylab import *
from bigr.clustercontrol import ClusterControl
from optparse import OptionParser

use_cluster = True
check = False

def get_image_corners( bin_bbox, filename ):
    command = [bin_bbox, '-in', filename]
    proc = subprocess.Popen( command, stdout=subprocess.PIPE )
    result = proc.communicate()[0]

    min_index = re.search( "MinimumIndex = \[(-?[\d]*), (-?[\d]*), (-?[\d]*)\]", result).groups()
    max_index = re.search( "MaximumIndex = \[(-?[\d]*), (-?[\d]*), (-?[\d]*)\]", result).groups()

    min_index = map(lambda x: float(x), min_index )
    max_index = map(lambda x: float(x), max_index )
        
    return get_all_mixtures( min_index, max_index )	
	
def get_all_mixtures( in1, in2 ):
    if len(in1) is not len(in2):
        raise ValueError('Length of arrays to mix must be equal')

    N = len( in1 )
    paired = (in1, in2)
    
    output = []
    for k in range(0, int( math.pow(2, N) ) ):
        suboutput = []
        
        for m in range(0, N):
            suboutput.append( paired[ (k >> m) & 1 ][m] )

        output.append( suboutput )

    return output	

def main( data_dir, cc, threads, work_subdir, performance, nomask, use_hard_segmentation ):	
    
	# Store data dir
#	data_dir = os.path.abspath(os.path.expanduser(sys.argv[1]))
	print "Data dir: " + data_dir
	t1w_dir = os.path.join( data_dir, 'T1w' )
	cbf_dir = os.path.join( data_dir, 'CBF_pvc' )
	feature_dir = os.path.join( data_dir, 'Features' )
	hammers_dir = os.path.join( data_dir, 'Hammers' )
	
	if not os.path.exists( feature_dir ):
		os.mkdir( feature_dir )	
	
	# Read and filter input files
	input_files = [];
	for filename in os.listdir( t1w_dir ):
		if re.match('.*.nii.gz', filename ):
			input_files.append( filename )
			
	input_files.sort()
	
	#for k, filename in enumerate( input_files ):
	#    print( '%03d: %s' % (k, filename ) )
	
	lists = [];		
	for i, filename in enumerate( input_files ):
			(filebase,ext)=os.path.splitext(filename) 
			(filebase,ext)=os.path.splitext(filebase) 
			#(iris, filebase)=filebase.split('_')
			print( '%03d: %s' % (i, filebase ) )
			lists.append( filebase )
			
	work_dir = os.path.join(feature_dir, work_subdir)
	
	if nomask:
		work_dir = os.path.join(feature_dir, work_subdir +'_nomask')
	
	if not os.path.exists( work_dir ):
		os.mkdir( work_dir )	
		
	# Creating a run_id
	print 'Hashing %s' % sys.argv[0] + os.path.join( data_dir, work_subdir )
	run_id = (sys.argv[0] + os.path.join( data_dir, work_subdir ) + 'saltnpepper' ).__hash__()
	print 'Hash tag for this session: %d' % run_id
	run_id = str( abs( run_id ) )
	run_id = 'ip_fvox' +  run_id[0:3]
	
	if (len( sys.argv ) >= 4):
	    force = bool( int( sys.argv[3]  ) )
	    print( 'Force taken from command line: ' + str(force) )
	else:
	    force = False
	
	#force = True
	force_next = False
	
	# Set binaries
	bin_threshold  = 'pxthresholdimage'
	bin_imoperator = 'pxbinaryimageoperator'
	bin_nonzero = 'pxcountnonzerovoxels'
	bin_convert = 'pxcastconvert'
	bin_stat = 'pxstatisticsonimage'
	bin_deformation = 'pxdeformationfieldoperator'
	bin_combine		  = 'pxcombinesegmentations'
	bin_transformix   = 'transformix'
	bin_bbox          = 'pxcomputeboundingbox'
	bin_cropimage	= 'pxcropimage'
	bin_unary = 'pxunaryimageoperator'
	pxnaryimoperator = 'pxnaryimageoperator'
	pxbinaryimageoperator = 'pxbinaryimageoperator'
	
	# Step 0: Write labels
	print('# Step 0: Write labels')
	
	if force_next:
	    force = True
	
	labels_file = os.path.join ( feature_dir, 'labels.txt')	
	labels_txt='label\n'
	for i in range(0, len( input_files )): #Loop over all subjects
		k =  lists[i] 	
		labels_txt += k + '\n'	
			
	f_output = open(labels_file, 'w')	
	print labels_txt	
	f_output.write(labels_txt)	
	f_output.close()	
	
	
	# # Step 1: Copy Jacobian to feature folder and make nifti
	# print('# Step 1: Copy Jacobian to feature folder and make nifti')
	
	# jac_dir = os.path.join ( work_dir, 'jacobian')
	# if not os.path.exists( jac_dir ):
		# os.mkdir( jac_dir ) 
	
	# command_list=[]
	# for i in range(0, len( input_files )): #Loop over all subjects
		# k =  lists[i] 	
	
		# jacobian = os.path.join( t1w_dir, work_subdir, 'mean_space_mask_' + k, 'spatialJacobian.nii.gz')
		# result  = os.path.join(jac_dir, k + '.nii.gz')
		
		# if force or (not os.path.exists( result ) ):
			# convert_command = '%s -in %s -out %s' % (bin_convert, jacobian, result)
			# command_list.append(convert_command)
			
	# # Send jobs		
	# if command_list:		
		# job_id = cc.send_job_array( command_list, 'day', '00:05:00', run_id)		
	
		# # Wait until registrations are all done
		# cc.wait(job_id)		
			
	# Step 2: Write intercranial volume
	print('# Step 2: Write intercranial volume')
	
	if force_next:
	    force = True
	
	icv_dir = os.path.join ( work_dir, 'icv')
	if not os.path.exists( icv_dir ):
		os.mkdir( icv_dir)		
		
	command_list=[]	
	icvs=[]
	
	if use_hard_segmentation:
		mod_dir = os.path.join ( work_dir, 'gm_modulated')
		jac_dir = os.path.join ( work_dir, 'jacobian')
		if not os.path.exists( jac_dir ):
			os.mkdir( jac_dir )
	else:
		mod_dir = os.path.join ( work_dir, 'gm_prob_modulated')
		
	if not os.path.exists( mod_dir ):
		os.mkdir( mod_dir )  	
	
	if use_hard_segmentation:	
		for i in range(0, len( input_files )): #Loop over all subjects
			k =  lists[i]
			
			output_file_subject = os.path.join ( icv_dir, k + '.txt') 
			result  = os.path.join(jac_dir, k + '.nii.gz')	
				
			if force or (not os.path.exists( output_file_subject ) or not os.path.exists( result ) ):
				# Calculate intercranial volume
				brain_mask = os.path.join( hammers_dir, work_subdir, k, 'brain_mask.nii.gz')
				
				if not os.path.exists(brain_mask):
					print 'WARNING: ' + brain_mask + 'does not exist!'
					brain_mask = os.path.join(t1w_dir, 'brain_mask_bet', k + '_mask.nii.gz')
					print 'WARNING: Using ' + brain_mask + ' instead for estimation of ICV'
					
				if os.path.exists(brain_mask):
					nonzero_command = '%s -in %s' % ( bin_nonzero, brain_mask)
					icv = os.popen(nonzero_command).read()
					[p1,icv,p3,p4]=icv.split(None);			
						
					output_string_icv = str(icv) + '\n'	
					
					f_output = open(output_file_subject, 'w')		
					f_output.write(output_string_icv)	
					f_output.close()	
				
					if nomask:
						jacobian = os.path.join( t1w_dir, work_subdir, 'mean_space_' + k, 'spatialJacobian.nii.gz')	
					else:
						jacobian = os.path.join( t1w_dir, work_subdir, 'mean_space_mask_' + k, 'spatialJacobian.nii.gz')	
				
					factor = math.pow(0.7, 3)
					minus_command = '%s -in %s -ops TIMES -arg %f -out %s' % (bin_unary, jacobian, factor, result)
					command_list.append(minus_command)
				else:
					print 'ERROR: ' + brain_mask + 'does not exist!!'				
		
			# Send jobs		
			if command_list:	
				print command_list	
				job_id = cc.send_job_array( command_list, 'hour', '00:20:00', run_id, '20G')				
			
				# Wait until registrations are all done
				cc.wait(job_id)	
				command_list=[]
		
	command_list=[]	
	for i in range(0, len( input_files )): #Loop over all subjects
		k =  lists[i]
		
		result  = os.path.join(mod_dir, k + '.nii.gz')	
			
		if force or not os.path.exists( result ):
			if use_hard_segmentation:
				jacobian = os.path.join(jac_dir, k + '.nii.gz')
				if nomask:
					gm_mask = os.path.join(t1w_dir, work_subdir, 'spm_template_space_nomask', k, 'result.nii.gz')
				else:
					gm_mask = os.path.join(t1w_dir, work_subdir, 'spm_template_space', k, 'result.nii.gz')
			else:
				if nomask: 
					jacobian = os.path.join( t1w_dir, work_subdir, 'mean_space_' + k, 'spatialJacobian.nii.gz')
					gm_mask = os.path.join(t1w_dir, work_subdir, 'spm_prob_template_space_nomask', k, 'result.nii.gz')	
				else:
					jacobian = os.path.join( t1w_dir, work_subdir, 'mean_space_mask_' + k, 'spatialJacobian.nii.gz')
					gm_mask = os.path.join(t1w_dir, work_subdir, 'spm_prob_template_space', k, 'result.nii.gz')	
						
				
				
			minus_command = '%s -in %s %s -out %s -ops TIMES' % (pxbinaryimageoperator, jacobian, gm_mask, result)
			command_list.append(minus_command)		
	
	# Send jobs		
	if command_list:		
		job_id = cc.send_job_array( command_list, 'week', '00:20:00', run_id)				
	
		# Wait until registrations are all done
		cc.wait(job_id)		
		command_list=[]
	
	return	
	
	# Step 5: Remove cerebellum and brain stem from GM masks
	print('# Step 5: Remove cerebellum and brain stem from GM masks')
	
	# 17	Cerebellum right
	# 18	Cerebellum left
	# 19	Brainstem (spans the midline)
	
	seg = os.path.join( feature_dir, work_subdir, 'seg_template.nii.gz');	
	brain_mask_no_cerebellum  = os.path.join( work_dir,'seg_no_cerebellum_brainstem.nii.gz');		
	temp1  = os.path.join( work_dir, 'temp1.nii.gz');
	temp2  = os.path.join( work_dir,'temp2.nii.gz');	
	
	command_list =[];		
	if force or (not os.path.exists( temp1) ):
		thresh_command = '%s -in %s -out %s  -inside 1 -outside 0 -t1 %s -t2 %s' % ( bin_threshold, seg, temp1, str(1), str(16))
		command_list.append(thresh_command)		
	if force or (not os.path.exists( temp2) ):
		thresh_command = '%s -in %s -out %s  -inside 1 -outside 0 -t1 %s -t2 %s' % ( bin_threshold, seg, temp2, str(20), str(83))
		command_list.append(thresh_command)	
	
	# Send jobs		
	if command_list:		
		job_id = cc.send_job_array( command_list, 'day', '00:10:00', run_id)		
	
		# Wait until registrations are all done
		cc.wait(job_id)		
		
	command_list =[];	
	if force or (not os.path.exists( brain_mask_no_cerebellum ) ):
		minus_command = '%s -in %s %s -out %s -ops ADDITION' % ( bin_imoperator, temp1, temp2, brain_mask_no_cerebellum)
		command_list.append(minus_command)
	
	# Send jobs		
	if command_list:		
		job_id = cc.send_job_array( command_list, 'day', '00:10:00', run_id)		
	
		# Wait until registrations are all done
		cc.wait(job_id)	
		
		
	# mask =  os.path.join( feature_dir, 'brain_mask_template.nii.gz');	
	# brain_mask_no_cerebellum =  os.path.join( feature_dir, 'brain_mask_no_cerebellum.nii.gz');	
	
	# command_list =[];	
	# if force or (not os.path.exists( brain_mask_no_cerebellum ) ):
		# minus_command = '%s -in %s %s -out %s -ops MINUS' % ( bin_imoperator, mask, cerebellum, brain_mask_no_cerebellum)
		# command_list.append(minus_command)
	
	# # Send jobs		
	# if command_list:		
		# job_id = cc.send_job_array( command_list, 'day', '00:10:00', run_id)		
	
		# # Wait until registrations are all done
		# cc.wait(job_id)	
		
	gm_vote =  os.path.join( work_dir, 'gm_vote_mask.nii.gz');	
	gm_or =  os.path.join( work_dir, 'gm_or_mask.nii.gz');		
	
	gm_vote_no_cerebellum =  os.path.join( work_dir, 'gm_vote_mask_no_cerebellum.nii.gz');	
	gm_or_no_cerebellum =  os.path.join( work_dir, 'gm_or_mask_no_cerebellum.nii.gz');
		
	command_list =[];	
	if force or (not os.path.exists( gm_vote_no_cerebellum ) ):
		times_command = '%s -in %s %s -out %s -ops TIMES' % ( bin_imoperator, gm_vote, brain_mask_no_cerebellum, gm_vote_no_cerebellum)
		command_list.append(times_command)
	if force or (not os.path.exists( gm_or_no_cerebellum ) ):
		times_command = '%s -in %s %s -out %s -ops TIMES' % ( bin_imoperator, gm_or, brain_mask_no_cerebellum, gm_or_no_cerebellum)
		command_list.append(times_command)
	
	# Send jobs		
	if command_list:		
		job_id = cc.send_job_array( command_list, 'day', '00:10:00', run_id)		
	
		# Wait until registrations are all done
		cc.wait(job_id)		
		
	force = False
	
	if os.path.exists(cbf_dir):
		# Step 3: Copy CBF to feature folder and make nifti
		print('# Step 3: Copy CBF to feature folder and make nifti')
		
		result_dir = os.path.join ( work_dir, 'cbf')
		if not os.path.exists( result_dir ):
			os.mkdir( result_dir ) 
		
		command_list=[]
		for i in range(0, len( input_files )): #Loop over all subjects
			k =  lists[i] 	
		
			cbf = os.path.join(cbf_dir, work_subdir, 'mean_space_cbf_cbfmap', k, 'result.nii.gz')
			result  = os.path.join(result_dir, k + '.nii.gz')
			
			if force or (not os.path.exists( result ) ):
				convert_command = '%s -in %s -out %s' % (bin_convert, cbf, result)
				command_list.append(convert_command)
				
		# Send jobs		
		if command_list:		
			job_id = cc.send_job_array( command_list, 'day', '00:05:00', run_id)		
		
			# Wait until registrations are all done
			cc.wait(job_id)
		    
		# Step 4: Multipy CBF with modulated GM
		print('# Step 4: Multipy CBF with modulated GM')
		
		if use_hard_segmentation:
		    result_dir = os.path.join ( work_dir, 'cbf_modulated_gm')
		else:    
		    result_dir = os.path.join ( work_dir, 'cbf_modulated_gm_prob')
		if not os.path.exists( result_dir ):
		    os.mkdir( result_dir ) 
		
		command_list=[]
		for i in range(0, len( input_files )): #Loop over all subjects
		    k =  lists[i]     
		
		    cbf = os.path.join(work_dir, 'cbf', k + '.nii.gz')
		    if use_hard_segmentation:
		        gm = os.path.join(work_dir, 'gm_modulated', k + '.nii.gz')
		    else:
		        gm = os.path.join(work_dir, 'gm_prob_modulated', k + '.nii.gz')
		    result  = os.path.join(result_dir, k + '.nii.gz')
		    
		    if force or (not os.path.exists( result ) ):
		        mult_command = 'fslmaths %s -mul %s %s' % ( cbf, gm, result)
		        command_list.append(mult_command)
		        
		# Send jobs        
		if command_list:        
		    job_id = cc.send_job_array( command_list, 'day', '00:10:00', run_id)        
		
		    # Wait until registrations are all done
		    cc.wait(job_id)    
			
if __name__ == '__main__':
    # Parse input arguments
    parser = OptionParser(description="Calculate features ", usage="Usage: python %prog input_dir use_hard_segmentation. Use option -h for help information.")
    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("wrong number of arguments")
        
    data_dir=args[0]    
    use_hard_segmentation=args[1]
        
    cc = ClusterControl()    
    work_subdir ='atlas_work_temp';
    threads=1
    performance=False
    nomask=True
    
    main( data_dir, cc, threads, work_subdir, performance, nomask, use_hard_segmentation )  