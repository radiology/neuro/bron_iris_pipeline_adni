#!/bin/env python
import os
import math
from bigr.environmentmodules import EnvironmentModules
from bigr.clustercontrol import ClusterControl

env = EnvironmentModules()
cc = ClusterControl()

env.unload('elastix')
env.load('mcr/R2013b')
print env.loaded_modules

os.chdir(os.path.join(os.curdir, 'do_classification' ))


# Data lists
data_list=['gm','gm','gm','gm']
study_list=['process_challenge', 'process_challenge','process_challenge','process_challenge']
groups_list=['AD,CN','AD,MCI','CN,MCI','MCIc,MCInc']
data_time=[230,230,230,200]
data_memory=[15,15,20,10]

# Methods lists
use_forward_selection_list=['0']
selection_method_list=['']
method_time=[1]
method_memory=[1]
iteration_time=1;

# Skip these
dont_run = []

confounder_list=['0', '1', '2']
trainset='0'

job_iterator=0
for i,data in enumerate(data_list):
	study=study_list[i]
	groups=groups_list[i]
	if study=='IRIS':
		method_time_new=[m *2.5 for m in method_time]
	else:
		method_time_new=method_time
	for j,confounder in enumerate(confounder_list):		
		
#		if not (groups=='AD,MCI' or groups=='CN,MCI'):
#			continue
		if groups=='MCIc,MCInc':
			continue
		
		
		# Estimate runtime
		time=data_time[i]*method_time_new[0]
		print time
		hours=math.floor(time/60)
		mins=time % 60
		print mins
		
		time_format='%02d'%hours + ':' +  '%02d'%mins + ':00'
		print time_format
		
		# Estimate required memory
		memory=int(data_memory[i]*method_memory[0])
		if int(confounder):
			memory=memory*3

		print memory
	
		
		# Generate job_id		
		run_id='CEXP_' + data + '_' + study + '_' + groups		
		
		if int(confounder):
			run_id=run_id + '_confounder' + confounder		

		# Skip?
		print '%3d'%job_iterator + ' : ' + run_id
		if job_iterator in dont_run:
			print run_id + ' is skipped.'
			continue
				
		# Run
		matlab_command = [data,'vbm', 'study', study, 'groups', groups, 'cvalue', '100000','confounder', confounder, 'trainset', trainset]
		
		print matlab_command
		
		if hours > 23:
			job_id = cc.send_job('./do_classification', matlab_command, 'week', time_format, run_id, str(memory) + 'G' )
		elif hours:
			job_id = cc.send_job('./do_classification', matlab_command, 'day', time_format, run_id, str(memory) + 'G' )
			
		else:				
			job_id = cc.send_job('./do_classification', matlab_command, 'hour', time_format, run_id, str(memory) + 'G' )
		print job_id
	
