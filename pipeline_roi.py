#!/bin/env python

# Pipeline for processing data for classification
# Step 1: Make brain masks & N3 non-uniformity correction
# Step 2: Do SPM tissue segmentation
# Step 3: Do registrations of Hammers atlas and make ROI labeling
# Step 4: Calculate features for ROI interest

# Extra:
# Step 5: Do classification

# Prerequisites 
# - Elastix
# - Matlab
# - itktools
# - SPM
# - FSL (bet)

# Folder structure
# Project folder (scripts/pipeline are ran from here)
#	T1w
#		subjects
#			*.nii.gz
#		brain_mask_bet (made in step 1)
#		nuc (made in step 1)
#		spm (made in step 2)
#	Hammers (made in step 3)
#	Features (made in step 4)

# September 2011 - August 2014
# Esther Bron - e.bron@erasmusmc.nl 

import os
import subprocess
import re
import sys
import datetime
import time
import shutil
import math

# Tools for computation cluster, could be replaced
from bigr.environmentmodules import EnvironmentModules
from bigr.clustercontrol import ClusterControl


#Environment modules
elastix='elastix/4.6'
matlab='matlab/R2013b'
mcr='mcr/R2013b'
itktools='itktools'

env = EnvironmentModules()
cc = ClusterControl()

def load_elastix():
	env.unload(matlab)
	env.unload(mcr)
	env.load(elastix)
	env.load(itktools)	

def load_matlab():
	env.load(matlab)
	env.load(mcr)
	env.unload(elastix)
	env.unload(itktools)	

performance = False
# Which ROI labelings should be processed: roi_list=['seg','lobe','hemisphere','brain','brain_mask']
roi_list=['seg', 'brain_mask']
	
threads=1  

# Creating a run_id
if performance:
	work_subdir ='atlas_work_temp_performance';
else:
	work_subdir ='atlas_work_temp';

input_dir=os.getcwd()
scripts_dir=os.path.dirname(os.path.realpath(__file__))

# Step 1: Make brain masks & N3 non-uniformity correction
print '# Step 1: Make brain masks & N3 non-uniformity correction'
load_elastix()
import brain_extraction_and_nuc
brain_extraction_and_nuc.main(input_dir, cc,  threads, work_subdir)

# Step 2: Do SPM tissue segmentation
print '# Step 2: Do SPM tissue segmentation'
# Make sure that the templates in run_batch_segment_iris_cluster_job.m are looked for in the correct SPM folder
load_matlab()
import spm_tissue_segmentation
spm_tissue_segmentation.main(input_dir, cc,  threads, work_subdir)

# Step 3: Do registrations of Hammers atlas and make ROI labeling
print '# Step 3: Do registrations of Hammers atlas and make ROI labeling'
load_elastix()
import hammers_registration_subject_mask
hammers_registration_subject_mask.main(input_dir, cc,  threads, work_subdir, performance, roi_list)

# Step 4: Compute ROI-wise features for classification
print '# Step 4: Compute ROI-wise features for classification'	
import construct_features
construct_features.main(input_dir, cc,  threads, work_subdir, performance, roi_list)

# Step 5: Do classification
print '# Step 3: Do classification'	
os.chdir(os.path.join(scripts_dir, 'do_classification' ))

load_matlab()
matlab_command = './do_classification gm region study Cuingnet_data' #Cuingnet_data is the input_dir

command_list=[]
run_id='classification_gm_region_adni'
command_list.append(matlab_command)
logfile=os.path.join(input_dir, 'Features', work_subdir, run_id + '.txt')

# Send jobs
if command_list:
	job_id = cc.send_job_array( command_list, 'day', '03:00:00', run_id, '5G', outputLog=logfile)
	print job_id