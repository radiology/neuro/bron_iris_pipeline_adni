#!/bin/env python

# Do registrations of Hammers atlas and make ROI labeling
# Registration of 30 Hammers subjects to all subject's space, propagation and combination of the atlasses 

# Requires elastix parameter files


# Call from other python script with: brain_extraction_and_nuc.main(data_dir, cc, threads, work_subdir, performance, lobe_list)
# Or from command line with:  python brain_extraction_and_nuc.py input_dir

# July 2013-August 2014
# Esther Bron - e.bron@erasmusmc.nl 


import os
import subprocess
import re
import sys
import datetime
import time
import shutil
import math
import glob
from bigr.clustercontrol import ClusterControl
from optparse import OptionParser

use_cluster = True

def main(data_dir, cc, threads, work_subdir, performance, lobe_list):	
	
	# Store data dir
#	data_dir = os.path.abspath(os.path.expanduser(sys.argv[1]))
	print "Data dir: " + data_dir
	t1w_dir = os.path.join( data_dir, 'T1w' )
	print "T1-weighted dir: " + t1w_dir
	nuc_dir = os.path.join( t1w_dir, 'nuc')
	cbf_dir = os.path.join( data_dir, 'CBF' )
	print "Cerebral Blood Flow map dir: " + cbf_dir
	scripts_dir=os.path.dirname(os.path.realpath(__file__))
	
	# Hammers subjects
	hammers_dir = os.path.join( scripts_dir, 'Hammers_n30r83' ) # Make sure this points to the directory containing the Hammers atlas which is not included in this package.

	# Set binaries
	bin_elastix       = 'elastix -threads ' + str(threads)
	print(bin_elastix)
	bin_transformix   = 'transformix -threads ' + str(threads)
	bin_combine	  	  = 'pxcombinesegmentations'
	bin_convert  	  = 'pxcastconvert'
	bin_morphology    = 'pxmorphology'
	bin_thresh		  = 'pxthresholdimage'
	
	dir_params        = os.path.join(scripts_dir, 'params')
	
	par_file_sim  = 'par_atlas_sim.txt'
	par_file_aff  = 'par_atlas_aff_checknrfalse.txt'
	#par_file_bsp  = 'par_atlas_bsp.txt'
	par_file_bsp  = 'par_atlas_bsp_grid_checknrfalse.txt'
	#par_file_inv  = 'par_atlas_invert.txt'
	par_file_inv  = 'par_atlas_invert_grid.txt'
	par_file_dum  = 'par_dummy.txt'
	par_file_rig  = 'par_atlas_rigid_checknrfalse.txt'
	par_file_rig_mask  = 'par_atlas_rigid_nn.txt'
	
	
	# Create absolute path names
	par_file_sim = os.path.join( dir_params, par_file_sim )
	par_file_aff = os.path.join( dir_params, par_file_aff )
	par_file_bsp = os.path.join( dir_params, par_file_bsp )
	par_file_dum = os.path.join( dir_params, par_file_dum )
	par_file_inv = os.path.join( dir_params, par_file_inv )
	par_file_rig = os.path.join( dir_params, par_file_rig )
	par_file_rig_mask = os.path.join( dir_params, par_file_rig_mask )
	
	# # Read and filter input files
	# hammers_files = [];
	# for filename in os.listdir( hammers_dir ):
		# if re.match('p\d', filename ):
			# hammers_files.append( filename )
			
	# hammers_files.sort()
	
#	if (len( sys.argv ) >= 3):
#		print sys.argv[2]
#		type = str( sys.argv[2] ) 
#		print( 'Type taken from command line: ' + type )
#	else:
#	    type = None
	
	# Read and filter input files
	input_files = [];
	for filename in os.listdir( t1w_dir ):
		if re.match('.*.nii.gz', filename ): #'.*.nii', filename ):
			input_files.append( filename )
			
	input_files.sort()
	
	list = [];		
	for i, filename in enumerate( input_files ):
			(filebase,ext)=os.path.splitext(filename)
			(filebase,ext)=os.path.splitext(filebase) 
#			print( '%03d: %s' % (i, filebase ) )
			list.append( filebase )	
		
	# Creating a run_id
	print 'Hashing %s' % sys.argv[0] + os.path.join( data_dir, work_subdir )
	run_id = (sys.argv[0] + os.path.join( data_dir, work_subdir ) + 'saltnpepper' ).__hash__()
	print 'Hash tag for this session: %d' % run_id
	run_id = str( abs( run_id ) )
	run_id = 'ip_hams' +  run_id[0:3]	
		
	# Set output dir	
	out2_dir = os.path.join( data_dir, 'Hammers', work_subdir)
	if not os.path.exists( os.path.join( data_dir, 'Hammers')):
		os.mkdir( os.path.join( data_dir, 'Hammers') )
	if not os.path.exists( out2_dir ):
		os.mkdir( out2_dir )
	
	force_next=0	
	
	# Read and filter input files
#	lobe_files = [];
#	for filename in os.listdir( os.path.join(hammers_dir, 'lobes' )):
#		if re.match('.*map.txt', filename ): #'.*.nii', filename ):
#			lobe_files.append( filename )
#			
#	lobe_files.sort()
	
#	lobe_list = [];		
#	for i, filename in enumerate( lobe_files ):
#		print filename
#		(filebase,ext)=os.path.splitext(filename)
#		(filebase,ext)=os.path.splitext(filebase)		
#		filebase=filebase[:-3]
#		if filebase in roi_list:
##			print( '%03d: %s' % (i, filebase ) )
#			lobe_list.append( filebase )	

	# Step 0a: Dilate brain masks Hammers
	print '# Step 0: Dilate brain masks Hammers'
	command_list = [];
	for i in range(1, 31):
		k = 'a%.2d' % i
		# print k
		
		if (len( sys.argv ) >= 5):
			force = bool( int( sys.argv[4]  ) )
			print( 'Force taken from command line: ' + str(force) )
		else:
			force = False
		
		mask=glob.glob(os.path.join(hammers_dir, k + '*mask.nii.gz'))
		mask = str(mask).strip('[]')
		
		mask_out = os.path.join(hammers_dir, k + '_brain_mask_r5.nii.gz')
		
		if force or (not os.path.exists( mask_out )):
			force_next = True
			dilate_command = '%s -in %s -op dilation -type binary -r 5 -out %s' % (bin_morphology, mask, mask_out)
			command_list.append(dilate_command)
			
		# input_image=os.path.join(hammers_dir, k + '.nii.gz')	
		# nonzero_input_image = os.path.join(hammers_dir, k + '_nonzero.nii.gz')	
			
		# if force or (not os.path.exists( nonzero_input_image ) ):
			# thresh_command = '%s -in %s -out %s' % (bin_thresh, input_image, nonzero_input_image)
			# command_list.append(thresh_command)	
	
	
	if command_list:		
		job_id = cc.send_job_array( command_list, 'hour', '00:15:00', run_id)		
	
		# Wait until registrations are all done
		cc.wait(job_id)
	
	# Step 0b: Dilate brain masks subjects
	print '# Step 0b: Dilate brain masks subjects'
	command_list=[]
	fixed_mask_dir=os.path.join(t1w_dir, 'brain_mask_r10')
	if not os.path.exists( fixed_mask_dir ):
		os.mkdir( fixed_mask_dir )	
	
	for j in range(0, len( input_files )): #Loop over all subjects
		l =  list[j] 
		# print l
	
		fixed_mask = os.path.join( t1w_dir, 'brain_mask_bet', l + '_mask.nii.gz')
		
		fixed_mask_out = os.path.join(fixed_mask_dir, l + '.nii.gz')
		
		if force or (not os.path.exists( fixed_mask_out ) ):
			print l
			force_next = True
			dilate_command = '%s -in %s -op dilation -type binary -r 10 -out %s' % (bin_morphology, fixed_mask, fixed_mask_out)
			command_list.append(dilate_command)
	
	if command_list:		
		job_id = cc.send_job_array( command_list, 'day', '00:15:00', run_id)		
	
		# Wait until registrations are all done
		cc.wait(job_id)			
		
	# Step 1a: Register mask Hammers subject to the subject mask	
	# fixed: mask
	# moving: mask
	# result: Hammers/work_subdir/subject/mask/a*
	print '# Step 1a: Register Hammers subjects to the subject spaces'
	command_list = [];
	for j in range(0, len( input_files )): #Loop over all subjects
		l =  list[j] 
		# print l
		# Fixed image: Mean T1w image
		fixed_mask = os.path.join( fixed_mask_dir, l + '.nii.gz')
	
		# Set output dir	
		out1_dir = os.path.join( out2_dir, l )#+ '_with_mask')
		if not os.path.exists( out1_dir ):
			os.mkdir( out1_dir )
			
		out1_dir = os.path.join( out2_dir, l,'mask' )#+ '_with_mask')
		if not os.path.exists( out1_dir ):
			os.mkdir( out1_dir )	
	
			
		for i in range(1, 31):
			k = 'a%.2d' % i
			# print k
			moving_mask = os.path.join( hammers_dir, k + '_brain_mask_r5.nii.gz' )
				
			out_dir = os.path.join( out1_dir, k )
			
			if not os.path.exists( out_dir ):
				os.mkdir( out_dir )
			
			if force or (not os.path.exists( os.path.join( out_dir, 'TransformParameters.0.txt' ) ) ):
				force_next = True			
				elx_command = '%s -f0 %s -m0 %s -p %s -out %s'% (bin_elastix, fixed_mask, moving_mask, par_file_rig_mask, out_dir)
				command_list.append(elx_command)
				
			# # Send jobs		
			# if len(command_list)>=10:		
				# job_id = cc.send_job_array( command_list, 'hour', '00:45:00', run_id, '5G')		
	
				# # Wait until registrations are all done
				# cc.wait(job_id)	
				# command_list=[]
				
	if command_list:		
		job_id = cc.send_job_array( command_list, 'week', '01:00:00', run_id, '2G')		
		command_list=[]
	
	if force_next:		
		# Wait until registrations are all done
		cc.wait(job_id)	
		
	# Step 1b: Register Hammers subjects to the subject spaces	
	# fixed: subject T1w, mask
	# moving: a*, mask
	# result: Hammers/work_subdir/subject/a*
	print '# Step 1b: Register Hammers subjects to the subject spaces'		
	command_list = [];	
	for j in range(0, len( input_files )): #Loop over all subjects
		l =  list[j] 
		# Fixed image: Mean T1w image
		fixed_image = os.path.join( nuc_dir, l + '.nii.gz')
		fixed_mask = os.path.join( fixed_mask_dir, l + '.nii.gz')
	
		# Set output dir	
		out1_dir = os.path.join( out2_dir, l )#+ '_with_mask')
		if not os.path.exists( out1_dir ):
			os.mkdir( out1_dir )
	
		
		for i in range(1, 31):
			k = 'a%.2d' % i
			# print k
			moving_image = os.path.join( hammers_dir, k + '.nii.gz' )
			moving_mask = os.path.join( hammers_dir, k + '_brain_mask_r5.nii.gz' )
				
			out_dir = os.path.join( out1_dir, k )
			
			ini_or=os.path.join( out2_dir, l, 'mask',k,'TransformParameters.0.txt')
			trans_file=os.path.join( out2_dir, l, 'mask',k,'TransformParameters.0.new.txt')
			
			if os.path.exists( ini_or) and (force or (not os.path.exists( trans_file ) )):
				o = open( trans_file,"w")
				data = open( ini_or ).read()
				data = re.sub('ResampleInterpolator "FinalNearestNeighborInterpolator"','ResampleInterpolator "FinalBSplineInterpolator"', data)
						
				o.write( data )
				o.close()
			
			if not os.path.exists( out_dir ):
				os.mkdir( out_dir )
			
			if force or (not os.path.exists( os.path.join( out_dir, 'TransformParameters.2.txt' ) ) ):
				force_next = True			
				elx_command = '%s -f0 %s -fMask %s -m0 %s -mMask %s -t0 %s -p %s -p %s -p %s -out %s'% (bin_elastix, fixed_image, fixed_mask, moving_image, moving_mask, trans_file, par_file_rig, par_file_aff, par_file_bsp, out_dir) 
				command_list.append(elx_command)
				
			# # Send jobs		
			# if len(command_list)>=10:		
				# job_id = cc.send_job_array( command_list, 'hour', '00:45:00', run_id, '5G')		
	
				# # Wait until registrations are all done
				# cc.wait(job_id)	
				# command_list=[]
				
	if command_list:		
		job_id = cc.send_job_array( command_list, 'week', '01:00:00', run_id, '2G')		
		command_list=[]		
	
	if force_next:		
		# Wait until registrations are all done
		cc.wait(job_id)			
	
	# force = False	
		
	# Step 2: Change TransformParameter file to Nearest Neighbor Interpolation
	print '# Step 2: Change TransformParameter file to Nearest Neighbor Interpolation'
	command_list = [];
	for j in range(0, len( input_files )): #Loop over all subjects
		l =  list[j]
		# print l
	
		# Set output dir	
		out1_dir = os.path.join( out2_dir, l )#+ '_with_mask')		
			
		all_dir = os.path.join( out1_dir, 'seg')
		brain_mask_dir = os.path.join( out1_dir, 'brain_mask')
			
		if not os.path.exists( all_dir ):
			os.mkdir( all_dir )
		if not os.path.exists( brain_mask_dir ):
			os.mkdir( brain_mask_dir )	
			
		for i in range(1, 31):
			k = 'a%.2d' % i
			
			in_dir = os.path.join( out1_dir, k )
			ini_or = os.path.join( in_dir, 'TransformParameters.2.txt')
			trans_file = os.path.join( in_dir, 'TransformParameters.2.new.txt')
				
			all_file = os.path.join( hammers_dir, k + '-seg.nii.gz')
			brain_mask_file=glob.glob(os.path.join(hammers_dir, k + '*mask.nii.gz'))
			brain_mask_file = str(brain_mask_file).strip('[]')			
			
			if force or (not os.path.exists( trans_file ) ):
				o = open( trans_file,"w")
				data = open( ini_or ).read()
				data = re.sub('ResampleInterpolator "FinalBSplineInterpolator"','ResampleInterpolator "FinalNearestNeighborInterpolator"', data)
					
				o.write( data )
				o.close()			
		
	# Step 3: Transform regions to subject space
	print '# Step 3: Transform regions to subject space'
	# Moving: ROI
	for a in range(0, len( lobe_list )):
		lobe = lobe_list[a] 	
		
		print a
		print lobe
	
		command_list = [];
		process_list = [];
		for j in range(0, len( input_files )): #Loop over all subjects
			l =  list[j]
			# print l
			
			# Set output dir	
			out1_dir = os.path.join( out2_dir, l )#+ '_with_mask')		
			all_dir = os.path.join( out1_dir, lobe + '_temp')
			
			if not os.path.exists( all_dir ):
				os.mkdir( all_dir )
						
			all_out = os.path.join( out1_dir, lobe + '.nii.gz')	
			if os.path.exists(all_out):
				continue
			
			for i in range(1, 31):
				k = 'a%.2d' % i
				
				in_dir = os.path.join( out1_dir, k )
				trans_file = os.path.join( in_dir, 'TransformParameters.2.new.txt')
				
				# force = True
				all_file = os.path.join( hammers_dir, k + '-' + lobe + '.nii.gz')	
	
				if lobe in ['brain_mask']:
					all_file=glob.glob(os.path.join(hammers_dir, k + '*mask.nii.gz'))
					all_file = str(all_file).strip('[]')			
					
				all_dir = os.path.join( out1_dir, lobe + '_temp', k)			
				
				if not os.path.exists( all_dir ):
					os.mkdir( all_dir )
				
				if not os.path.exists( os.path.join( all_dir, 'result.nii.gz')):
					force_next = True
					trans_command = '%s -tp %s -in %s -out %s' % (bin_transformix, trans_file, all_file, all_dir)
					command_list.append(trans_command)
					process_list.append(l)					
					
			if command_list:		
				job_id = cc.send_job_array( command_list, 'day', '01:00:00', run_id, '1G')	
				command_list=[]	
					
					
		if force_next:		
			# Wait until registrations are all done
			cc.wait(job_id)				
	
		# Step 4: Convert registered segmentations to unsigned char
		print '# Step 4: Convert registered segmentations to unsigned char'
		command_list = [];
		for j in range(0, len( input_files )): #Loop over all subjects
			l =  list[j]
			
			out1_dir = os.path.join( out2_dir, l )#+ '_with_mask')	
			
			all_images_list  = []
			for i in range(1, 31):
				k = 'a%.2d' % i
				all_dir = os.path.join( out1_dir, lobe + '_temp', k)							
				all_images_list.append( os.path.join( all_dir, 'result.nii.gz' )) 
			
			if force_next:
				force = True
	
			# if l == 'IRIS_902':
				# force = True
						
			# Input for combine segmentations
	
				
			all_out= os.path.join( out1_dir, lobe + '.nii.gz')	
	
			if not os.path.exists( all_out):
				force_next=True
				for image in all_images_list:
					convert_command = '%s -in %s -opct %s -out %s' % (bin_convert, image, 'unsigned_char',image)
					command_list.append(convert_command)
					
			if command_list:		
				job_id = cc.send_job_array( command_list, 'day', '00:01:00', run_id, '1G')	
				command_list=[]	
					
					
		if force_next:		
			# Wait until registrations are all done
			cc.wait(job_id)			
		
		# Step 5: Make list of images
		print '# Step 5: Make list of images'
		command_list = [];
		for l in process_list: #Loop over all subjects
			
			out1_dir = os.path.join( out2_dir, l )#+ '_with_mask')	
			
			process_images_list  = []
			for i in range(1, 31):
				k = 'a%.2d' % i
				all_dir = os.path.join( out1_dir, lobe + '_temp', k)							
				process_images_list.append( os.path.join( all_dir, 'result.nii.gz' )) 
			
			if force_next:
				force = True		
		
		# Step 6: Combine the segmentations to create an atlas
		print '# Step 6: Combine the segmentations to create an atlas'		
		for j in range(0, len( input_files )): #Loop over all subjects
			l =  list[j]
			# print l		
					
			out1_dir = os.path.join( out2_dir, l )#+ '_with_mask')	
			
			all_images = []	
			for i in range(1, 31):
				k = 'a%.2d' % i
				all_dir = os.path.join( out1_dir, lobe + '_temp', k)							
				all_images.append( os.path.join( all_dir, 'result.nii.gz' )) 
						
			# if j<=9:
				# force=True
			
			if force_next:
				force = True
				
			in_dir = os.path.join( out1_dir, k )
		
			all_out = os.path.join( out1_dir, lobe + '.nii.gz')
#			all_out2= os.path.join( out1_dir, lobe + '.nii.gz')		
			
			if not os.path.exists( all_out):
				print all_out
				#SEG
				# Combine segmentations for all brain regions
	
				if lobe in ['brain_mask']:
					#Brain mask
					n=2
				elif lobe in ['brain']:
					#Brain 
					n=3
				elif lobe in ['hemisphere']:
				    #Hemisphere
					n=4			
				elif lobe in ['lobe']:
					#Lobe
					n=12
				else:
					#seg
					n=84					
				
				#pxcombinesegmentations -n 84 -m VOTE -in ${mask[*]} -outh $TMPDIR/mask.nii.gz 
				combine_command = [ '-n', str(n), '-m', 'VOTE', '-in'] + all_images + ['-outh', all_out]
				print combine_command
				job_id = cc.send_job( bin_combine, combine_command, 'hour', '00:30:00', run_id )
				print job_id	
				cc.wait([job_id])				
				
#				if not os.path.lexists( all_out2):
#					os.symlink(all_out,all_out2)	
					
		# Step 7: Clean up
		print '# Step 7: Clean up'		
		for j in range(0, len( input_files )): #Loop over all subjects
			l =  list[j]
			out1_dir = os.path.join( out2_dir, l )
			# print l	
			all_dir = os.path.join( out1_dir, lobe + '_temp')
				
			if os.path.exists(all_dir):				
				shutil.rmtree(all_dir)
				
if __name__ == '__main__':
	# Parse input arguments
	parser = OptionParser(description="Do registrations of Hammers atlas and make ROI labeling", usage="Usage: python %prog input_dir. Use option -h for help information.")
	(options, args) = parser.parse_args()

	if len(args) != 1:
		parser.error("wrong number of arguments")
		
	data_dir=args[0]	
		
	cc = ClusterControl()	
	work_subdir ='atlas_work_temp';
	threads=1
	performance=False
	lobe_list=['seg','brain','brain_mask']
	
	main(data_dir, cc, threads, work_subdir, performance, lobe_list)