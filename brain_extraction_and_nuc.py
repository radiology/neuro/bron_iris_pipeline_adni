#!/bin/env python

# Make brain masks & N3 non-uniformity correction
# Call from other python script with: brain_extraction_and_nuc.main(input_dir, cc,  threads, work_subdir)
# Or from command line with:  python brain_extraction_and_nuc.py input_dir

# July 2013-August 2014
# Esther Bron - e.bron@erasmusmc.nl 

import os
import subprocess
import re
import sys
import datetime
import time
import shutil
import math
from bigr.clustercontrol import ClusterControl
from optparse import OptionParser

#cc = ClusterControl()

use_cluster = True

def main(input_dir, cc, threads, work_subdir):	
	force=False
	
	# Store data dir
	print "Input dir: " + input_dir
	data_dir = os.path.join( input_dir, 'T1w' )
	print "Data dir: " + data_dir
	
	# Read and filter input files
	input_files = [];
	for filename in os.listdir( data_dir ):
		if re.match('.*.nii.gz', filename ):
			input_files.append( filename )
			
	input_files.sort()
	
	list = [];		
	for i, filename in enumerate( input_files ):
			(filebase,ext)=os.path.splitext(filename) 
			(filebase,ext)=os.path.splitext(filebase) 
			print( '%03d: %s' % (i, filebase ) )
			list.append( filebase )
	
	# Creating a run_id
	print 'Hashing %s' % sys.argv[0] + os.path.join( data_dir, work_subdir )
	run_id = (sys.argv[0] + os.path.join( data_dir, work_subdir ) + 'saltnpepper' ).__hash__()
	print 'Hash tag for this session: %d' % run_id
	run_id = str( abs( run_id ) )
	run_id = 'bet' +  run_id[0:7]
	
	#force = True
	force_next = False
	
	# Set binaries
	bin_nuc           = 'n4 3 -v 2 -s 4 -c [150,00001] -b [50]' #n4tk command: n4 3 -v 2 -s 4 -c [150,00001] -b [50] -i input_image -x brain_mask -o output_image
	bin_morphology    = 'pxmorphology' #itktools
	
	# Create list of filenames
	dir_contents = os.listdir(data_dir)
	
	# Check work directory
	if os.path.exists( os.path.join( data_dir, work_subdir ) ):
	    if not os.path.isdir( os.path.join( os.path.join( data_dir, work_subdir ) ) ):
	        raise ValueError('cannot create work directory')
	    else:
	        work_dir = os.path.join( data_dir, work_subdir )
	else:
	    force_next = True
	    os.mkdir( os.path.join( data_dir, work_subdir ) )
	    work_dir = os.path.join( data_dir, work_subdir )
	
	# Step 1: Brain extraction
	print('# Step 1: Brain extraction')
	mask_dir = os.path.join( data_dir, 'brain_mask_bet' )
	if not os.path.exists( mask_dir ):
		os.mkdir( mask_dir )
	
	command_list=[] 
	job_id_list = []
	for i in range(0, len( input_files )):
		k = list[i]
	
		in_file=os.path.join(data_dir, k+'.nii.gz')
		out_file=os.path.join(mask_dir, k+'.nii.gz')
		mask_file=os.path.join(mask_dir, k +'_mask.nii.gz')
		
		if force or (not os.path.exists( mask_file) ):
			print k
			force_next = True	
		
			fsl_command= 'bet %s %s -m -n -f 0.5 -B -d'% (in_file, out_file)
			command_list.append(fsl_command)
	
	if command_list:
		job_id = cc.send_job_array(command_list, 'day', '10:00:00', run_id, '10G')		
		job_id_list = job_id_list + job_id
	
		# Wait until registrations are all done
		cc.wait(job_id_list)
			
	#Clean-up
	for i in range(0, len( input_files )):
		k = list[i]
		out_file=os.path.join(mask_dir, k+'.nii.gz')
		if os.path.exists( out_file):	
			os.remove(out_file)
	
			
	# Step 2: Non-uniformity correction
	print('# Step 2: Non-uniformity correction')
	if force_next:
	    force = True
	
	force = False
		
	mask_dir = os.path.join( data_dir, 'brain_mask_bet' )
	nuc_dir = os.path.join( data_dir, 'nuc' )
	
	if not os.path.exists( nuc_dir ):
		os.mkdir( nuc_dir )
	  
	command_list=[] 
	job_id_list = []
	for i in range(0, len( input_files )):
		k = list[i]
		print k
		
		t1w_file=os.path.join(data_dir, k +'.nii.gz')
		mask_file=os.path.join(mask_dir, k +'_mask.nii.gz')
		out_file=os.path.join(nuc_dir, k+'.nii.gz')
		
		
		if force or (not os.path.exists( out_file ) ):
			force_next = True		
					
			command=[bin_nuc,'-i',t1w_file,'-x', mask_file, '-o', out_file]
			print ' '.join(command)
			subprocess.call(command, shell=False)

if __name__ == '__main__':
	# Parse input arguments
	parser = OptionParser(description="Make brain masks & N3 non-uniformity correction", usage="Usage: python %prog input_dir. Use option -h for help information.")
	(options, args) = parser.parse_args()

	if len(args) != 1:
		parser.error("wrong number of arguments")
		
	input_dir=args[0]	
		
	cc = ClusterControl()	
	work_subdir ='atlas_work_temp';
	threads=1
	
	main(input_dir, cc, threads, work_subdir)