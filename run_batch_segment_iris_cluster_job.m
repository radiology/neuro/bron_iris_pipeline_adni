%-----------------------------------------------------------------------
% Job configuration created by cfg_util (rev $Rev: 4252 $)
%-----------------------------------------------------------------------
matlabbatch{1}.spm.spatial.normalise.estwrite.subj.source = '<UNDEFINED>';
matlabbatch{1}.spm.spatial.normalise.estwrite.subj.wtsrc = '';
matlabbatch{1}.spm.spatial.normalise.estwrite.subj.resample = '<UNDEFINED>';
matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.template = {'/scratch/ebron/SPM/spm8/templates/T1.nii,1'};
matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.weight = '';
matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.smosrc = 8;
matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.smoref = 0;
matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.regtype = 'mni';
matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.cutoff = 25;
matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.nits = 16;
matlabbatch{1}.spm.spatial.normalise.estwrite.eoptions.reg = 1;
matlabbatch{1}.spm.spatial.normalise.estwrite.roptions.preserve = 0;
matlabbatch{1}.spm.spatial.normalise.estwrite.roptions.bb = [-125 -137 -103
                                                              200 200 200];
matlabbatch{1}.spm.spatial.normalise.estwrite.roptions.vox = [0.9375 0.9375 1];
matlabbatch{1}.spm.spatial.normalise.estwrite.roptions.interp = 1;
matlabbatch{1}.spm.spatial.normalise.estwrite.roptions.wrap = [0 0 0];
matlabbatch{1}.spm.spatial.normalise.estwrite.roptions.prefix = 'w';
matlabbatch{2}.spm.spatial.preproc.data(1) = cfg_dep;
matlabbatch{2}.spm.spatial.preproc.data(1).tname = 'Data';
matlabbatch{2}.spm.spatial.preproc.data(1).tgt_spec{1}(1).name = 'filter';
matlabbatch{2}.spm.spatial.preproc.data(1).tgt_spec{1}(1).value = 'image';
matlabbatch{2}.spm.spatial.preproc.data(1).tgt_spec{1}(2).name = 'strtype';
matlabbatch{2}.spm.spatial.preproc.data(1).tgt_spec{1}(2).value = 'e';
matlabbatch{2}.spm.spatial.preproc.data(1).sname = 'Normalise: Estimate & Write: Normalised Images (Subj 1)';
matlabbatch{2}.spm.spatial.preproc.data(1).src_exbranch = substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1});
matlabbatch{2}.spm.spatial.preproc.data(1).src_output = substruct('()',{1}, '.','files');
matlabbatch{2}.spm.spatial.preproc.output.GM = [0 0 1];
matlabbatch{2}.spm.spatial.preproc.output.WM = [0 0 1];
matlabbatch{2}.spm.spatial.preproc.output.CSF = [0 0 1];
matlabbatch{2}.spm.spatial.preproc.output.biascor = 1;
matlabbatch{2}.spm.spatial.preproc.output.cleanup = 2;
matlabbatch{2}.spm.spatial.preproc.opts.tpm = {
                                               '/scratch/ebron/SPM/spm8/tpm/grey.nii'
                                               '/scratch/ebron/SPM/spm8/tpm/white.nii'
                                               '/scratch/ebron/SPM/spm8/tpm/csf.nii'
                                               };
matlabbatch{2}.spm.spatial.preproc.opts.ngaus = [2
                                                 2
                                                 2
                                                 4];
matlabbatch{2}.spm.spatial.preproc.opts.regtype = 'mni';
matlabbatch{2}.spm.spatial.preproc.opts.warpreg = 1;
matlabbatch{2}.spm.spatial.preproc.opts.warpco = 25;
matlabbatch{2}.spm.spatial.preproc.opts.biasreg = 0.0001;
matlabbatch{2}.spm.spatial.preproc.opts.biasfwhm = 60;
matlabbatch{2}.spm.spatial.preproc.opts.samp = 3;
matlabbatch{2}.spm.spatial.preproc.opts.msk = {''};
matlabbatch{3}.spm.util.defs.comp{1}.inv.comp{1}.sn2def.matname(1) = cfg_dep;
matlabbatch{3}.spm.util.defs.comp{1}.inv.comp{1}.sn2def.matname(1).tname = 'Parameter File';
matlabbatch{3}.spm.util.defs.comp{1}.inv.comp{1}.sn2def.matname(1).tgt_spec{1}(1).name = 'filter';
matlabbatch{3}.spm.util.defs.comp{1}.inv.comp{1}.sn2def.matname(1).tgt_spec{1}(1).value = 'mat';
matlabbatch{3}.spm.util.defs.comp{1}.inv.comp{1}.sn2def.matname(1).tgt_spec{1}(2).name = 'strtype';
matlabbatch{3}.spm.util.defs.comp{1}.inv.comp{1}.sn2def.matname(1).tgt_spec{1}(2).value = 'e';
matlabbatch{3}.spm.util.defs.comp{1}.inv.comp{1}.sn2def.matname(1).sname = 'Normalise: Estimate & Write: Norm Params File (Subj 1)';
matlabbatch{3}.spm.util.defs.comp{1}.inv.comp{1}.sn2def.matname(1).src_exbranch = substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1});
matlabbatch{3}.spm.util.defs.comp{1}.inv.comp{1}.sn2def.matname(1).src_output = substruct('()',{1}, '.','params');
matlabbatch{3}.spm.util.defs.comp{1}.inv.comp{1}.sn2def.vox = [0.9375 0.9375 1];
matlabbatch{3}.spm.util.defs.comp{1}.inv.comp{1}.sn2def.bb = [-125 -137 -103
                                                              200 200 200];
matlabbatch{3}.spm.util.defs.comp{1}.inv.space = '<UNDEFINED>';
matlabbatch{3}.spm.util.defs.ofname = 'result';
matlabbatch{3}.spm.util.defs.fnames(1) = cfg_dep;
matlabbatch{3}.spm.util.defs.fnames(1).tname = 'Apply to';
matlabbatch{3}.spm.util.defs.fnames(1).tgt_spec{1}(1).name = 'filter';
matlabbatch{3}.spm.util.defs.fnames(1).tgt_spec{1}(1).value = 'image';
matlabbatch{3}.spm.util.defs.fnames(1).tgt_spec{1}(2).name = 'strtype';
matlabbatch{3}.spm.util.defs.fnames(1).tgt_spec{1}(2).value = 'e';
matlabbatch{3}.spm.util.defs.fnames(1).sname = 'Segment: c1 Images';
matlabbatch{3}.spm.util.defs.fnames(1).src_exbranch = substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1});
matlabbatch{3}.spm.util.defs.fnames(1).src_output = substruct('()',{1}, '.','c1', '()',{':'});
matlabbatch{3}.spm.util.defs.fnames(2) = cfg_dep;
matlabbatch{3}.spm.util.defs.fnames(2).tname = 'Apply to';
matlabbatch{3}.spm.util.defs.fnames(2).tgt_spec{1}(1).name = 'filter';
matlabbatch{3}.spm.util.defs.fnames(2).tgt_spec{1}(1).value = 'image';
matlabbatch{3}.spm.util.defs.fnames(2).tgt_spec{1}(2).name = 'strtype';
matlabbatch{3}.spm.util.defs.fnames(2).tgt_spec{1}(2).value = 'e';
matlabbatch{3}.spm.util.defs.fnames(2).sname = 'Segment: c2 Images';
matlabbatch{3}.spm.util.defs.fnames(2).src_exbranch = substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1});
matlabbatch{3}.spm.util.defs.fnames(2).src_output = substruct('()',{1}, '.','c2', '()',{':'});
matlabbatch{3}.spm.util.defs.fnames(3) = cfg_dep;
matlabbatch{3}.spm.util.defs.fnames(3).tname = 'Apply to';
matlabbatch{3}.spm.util.defs.fnames(3).tgt_spec{1}(1).name = 'filter';
matlabbatch{3}.spm.util.defs.fnames(3).tgt_spec{1}(1).value = 'image';
matlabbatch{3}.spm.util.defs.fnames(3).tgt_spec{1}(2).name = 'strtype';
matlabbatch{3}.spm.util.defs.fnames(3).tgt_spec{1}(2).value = 'e';
matlabbatch{3}.spm.util.defs.fnames(3).sname = 'Segment: c3 Images';
matlabbatch{3}.spm.util.defs.fnames(3).src_exbranch = substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1});
matlabbatch{3}.spm.util.defs.fnames(3).src_output = substruct('()',{1}, '.','c3', '()',{':'});
matlabbatch{3}.spm.util.defs.savedir.savedef = 1;
matlabbatch{3}.spm.util.defs.interp = 1;
