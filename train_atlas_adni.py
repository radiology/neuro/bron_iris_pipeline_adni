#!/bin/env python
# train_atlas_cuingnet.py
# Script for pairwise registration of subjects. Transform the test/MCI/other subjects to the template space based on the 150 AD/CN subjects. 

# Call from other python script with: train_atlas_cuingnet.main(input_dir, cc,  threads, work_subdir)
# Or from command line with:  python train_atlas_cuingnet.py input_dir 

# Changes (wrt create_atlas_iris_fiat.py): 
# - Looks for .nii.gz files
# - Runs 10 subjects's registration at a time
# - Handles larger subject sets (more memory/time for cluster jobs)
# - Handles direction matrix of Cuingnet data (which has f.e. 1e-10 instead of 0)

# Hakim Achterberg - h.achterberg@erasmusmc.nl
# Esther Bron - e.bron@erasmusmc.nl

# Copyright (c) 2014, Erasmus MC, University Medical Center Rotterdam
# The Software remains the property of the the authors (Achterberg, Bron).  

import os
import subprocess
import re
import sys
import datetime
import time
import shutil
import math
from bigr.clustercontrol import ClusterControl
from optparse import OptionParser

use_cluster = True

def get_boundingbox( bin_bbox, filename ):
    command = [bin_bbox, '-in', filename]
    proc = subprocess.Popen( command, stdout=subprocess.PIPE )
    result = proc.communicate()[0]

    min_point = re.search( "MinimumPoint = \[(-?[\d\.]*), (-?[\d\.]*), (-?[\d\.]*)\]", result).groups()
    max_point = re.search( "MaximumPoint = \[(-?[\d\.]*), (-?[\d\.]*), (-?[\d\.]*)\]", result).groups()

    min_point = map(lambda x: float(x), min_point )
    max_point = map(lambda x: float(x), max_point )

    if min_point[0] > max_point[0]:
        t = min_point[0]
        min_point[0] = max_point[0]
        max_point[0] = t
    if min_point[1] > max_point[1]:
        t = min_point[1]
        min_point[1] = max_point[1]
        max_point[1] = t
    if min_point[2] > max_point[2]:
        t = min_point[2]
        min_point[2] = max_point[2]
        max_point[2] = t

    return (min_point, max_point)

def get_all_mixtures( in1, in2 ):
    if len(in1) is not len(in2):
        raise ValueError('Length of arrays to mix must be equal')

    N = len( in1 )
    paired = (in1, in2)
    
    output = []
    for k in range(0, int( math.pow(2, N) ) ):
        suboutput = []
        
        for m in range(0, N):
            suboutput.append( paired[ (k >> m) & 1 ][m] )

        output.append( suboutput )

    return output

def get_image_corners( bin_bbox, filename ):
    command = [bin_bbox, '-in', filename]
    proc = subprocess.Popen( command, stdout=subprocess.PIPE )
    result = proc.communicate()[0]

    min_index = re.search( "MinimumIndex = \[(-?[\d]*), (-?[\d]*), (-?[\d]*)\]", result).groups()
    max_index = re.search( "MaximumIndex = \[(-?[\d]*), (-?[\d]*), (-?[\d]*)\]", result).groups()

    min_index = map(lambda x: float(x), min_index )
    max_index = map(lambda x: float(x), max_index )
        
    return get_all_mixtures( min_index, max_index )

def get_image_info( bin_iinfo, filename ):
    # Run the pxgetimageinformation command
	command = [bin_iinfo, '-all', '-in', filename]
	proc = subprocess.Popen( command, stdout=subprocess.PIPE )
	
	result = proc.communicate()[0] 
		    
    # Run regular expressions
	size      = re.search( "size:\s*\(([\d]+), ([\d]+), ([\d]+)\)", result ).groups()
	spacing   = re.search( "spacing:\s*\(([\d\.]*), ([\d\.]*), ([\d\.]*)\)", result ).groups()
	origin    = re.search( "origin:\s*\((-?[\d\.]*), (-?[\d\.]*), (-?[\d\.]*)\)", result ).groups()
	direction = re.search( "direction:\s*\((-?[\d\.]*e?-?[\d]?[\d]?[\d]?), (-?[\d\.]*e?-?[\d]?[\d]?[\d]?), (-?[\d\.]*e?-?[\d]?[\d]?[\d]?), (-?[\d\.]*e?-?[\d]?[\d]?[\d]?), (-?[\d\.]*e?-?[\d]?[\d]?[\d]?), (-?[\d\.]*e?-?[\d]?[\d]?[\d]?), (-?[\d\.]*e?-?[\d]?[\d]?[\d]?), (-?[\d\.]*e?-?[\d]?[\d]?[\d]?), (-?[\d\.]*e?-?[\d]?[\d]?[\d]?)\)", result ).groups()
	#direction = re.search( "direction:\s*\((-?[\d\.]*), (-?[\d\.]*), (-?[\d\.]*), (-?[\d\.]*), (-?[\d\.]*), (-?[\d\.]*), (-?[\d\.]*), (-?[\d\.]*), (-?[\d\.]*)\)", result ).groups()

    # Convert to numerics
	size      = map(lambda x: int(x), size )
	spacing   = map(lambda x: float(x), spacing )
	origin    = map(lambda x: float(x), origin )
	direction = map(lambda x: float(x), direction )
    
	return (size, spacing, origin, direction)

def main( input_dir, train_dir, cc, threads, work_subdir, performance, nomask):
	## Check arguments
	#if ( len( sys.argv ) < 2 or len( sys.argv ) > 3):
	#    print 'Usage: %s directory [force]' % sys.argv[0]
	#    exit(1)
	        
	# Store data dir
	scripts_dir=os.path.dirname(os.path.realpath(__file__))
	print "Input dir: " + input_dir
	data_dir = os.path.join( input_dir, 'T1w' )
	print "Data dir: " + data_dir
	mask_dir = os.path.join( input_dir, 'mask' )
	print "Data dir: " + data_dir
	print "Train dir: " + train_dir
	train_data_dir = os.path.join( train_dir, 'T1w' )
	train_mask_dir = os.path.join( train_dir, 'mask' )
    
	
	# Read and filter input files
	input_files = [];
	for filename in os.listdir( data_dir ):
		if train_dir == input_dir:
			if re.match('.*test.*.nii.gz', filename ) or re.match('.*MCI.*.nii.gz', filename ):
				input_files.append( filename )
		else: 
			if re.match('.*.nii.gz', filename ):
				input_files.append( filename )
			
	input_files.sort()
	
	train_files = [];
	for filename in os.listdir( train_data_dir ):
		if re.match('.*train.*.nii.gz', filename ) and (not re.match('.*MCI.*.nii.gz', filename )) :
			train_files.append( filename )
			
	train_files.sort()
	
	list = [];		
	for i, filename in enumerate( input_files ):
			(filebase1,ext)=os.path.splitext(filename) 
			(filebase,ext)=os.path.splitext(filebase1) 
			print( '%03d: %s' % (i, filebase ) )
			list.append( filebase )
			
	train_list = [];		
	for j, filename in enumerate( train_files ):
			(filebase1,ext)=os.path.splitext(filename) 
			(filebase,ext)=os.path.splitext(filebase1) 
			print( 'Train %03d: %s' % (j, filebase ) )
			train_list.append( filebase )
			
	print 'Hashing %s' % sys.argv[0] + os.path.join( data_dir, work_subdir )
	run_id = (sys.argv[0] + os.path.join( data_dir, work_subdir ) + 'saltnpepper' ).__hash__()
	print 'Hash tag for this session: %d' % run_id
	run_id = str( abs( run_id ) )
	run_id = 'challenge' +  run_id[0:1]
	
	if (len( sys.argv ) >= 4):
	    force = bool( int( sys.argv[3]  ) )
	    print( 'Force taken from command line: ' + str(force) )
	else:
	    force = False
	
	#force = True
	force_next = False
	
	# Set binaries
	bin_iinfo         = 'pxgetimageinformation'
	bin_bbox          = 'pxcomputeboundingbox'
	bin_average       = 'pxmeanstdimage'
	bin_elastix       = 'elastix -threads ' + str(threads)
	print(bin_elastix)
	bin_transformix   = 'transformix -threads ' + str(threads)
	bin_threshold     = 'pxthresholdimage'
	bin_morphology    = 'pxmorphology'
	
	dir_params      = os.path.join(scripts_dir, 'params')
	
	par_file_sim  = 'par_atlas_sim.txt' #'par_atlas_sim.txt'
	par_file_aff  = 'par_atlas_aff_checknrfalse.txt' #'par_atlas_aff_checknrfalse.txt'
	#par_file_bsp  = 'par_atlas_bsp.txt'
	par_file_bsp  = 'par_atlas_bsp_grid_checknrfalse.txt' #'par_atlas_bsp_grid_checknrfalse.txt'
	#par_file_inv  = 'par_atlas_invert.txt'
	par_file_inv  = 'par_atlas_invert_grid.txt' #'par_atlas_invert_grid.txt'
	par_file_dum  = 'par_dummy.txt'
	
	
	# Create absolute path names
	par_file_sim = os.path.join( dir_params, par_file_sim )
	par_file_aff = os.path.join( dir_params, par_file_aff )
	par_file_bsp = os.path.join( dir_params, par_file_bsp )
	par_file_dum = os.path.join( dir_params, par_file_dum )
	par_file_inv = os.path.join( dir_params, par_file_inv )
	
	trans_def_field_tpl = os.path.join( dir_params, 'deformation_transform.tpl.txt' )
	          
	# Make sure user is aware of this settings
	print 'Creating atlas of scans'
	
	# Create list of filenames
	dir_contents = os.listdir(data_dir)
	
	# Check work directory
	if os.path.exists( os.path.join( data_dir, work_subdir ) ):
	    if not os.path.isdir( os.path.join( os.path.join( data_dir, work_subdir ) ) ):
	        raise ValueError('cannot create work directory')
	    else:
	        work_dir = os.path.join( data_dir, work_subdir )
	else:
	    force_next = True
	    os.mkdir( os.path.join( data_dir, work_subdir ) )
	    work_dir = os.path.join( data_dir, work_subdir )
		
	# Step 1: align all shapes pairwise with elastix, create mean
	print('# Step 1: align all shapes pairwise with elastix, create mean')
	if force_next:
	    force = True
	
	counter = 0	
	counter2 = 0	
	job_id_list = [];
	job_id_list_use = [];
	for i in range(0, len( input_files )):
		k = list[i]
		print k
		base_dir = os.path.join( work_dir, 'registration_' + k )
		if not os.path.exists( base_dir ):
			force_next = True
			os.mkdir( base_dir )
        
		command_list = [];	
		
		for j in range(0, len( train_files )):
			l = train_list[j]
			fix_file0 = os.path.join( data_dir, input_files[i] )
			mov_file0 = os.path.join( train_data_dir, train_files[j] )
        
			out_dir =  os.path.join( base_dir, l)
			
			if  force or (not os.path.exists( os.path.join( out_dir, 'TransformParameters.2.txt' ) ) ):
				force_next = True
				if not os.path.exists( out_dir ):
					os.mkdir( out_dir )
					
				elx_command = '%s -f0 %s -m0 %s -p %s -p %s -p %s -out %s'% (bin_elastix, fix_file0, mov_file0, par_file_sim, par_file_aff, par_file_bsp, out_dir)
				command_list.append(elx_command)	
				counter = counter + 1	
				
			out_dir =  os.path.join( base_dir, k)			
			if j == 0 and (force or (not os.path.exists( os.path.join( out_dir, 'TransformParameters.2.txt' ) ) )):
				if not os.path.exists( out_dir ):
					os.mkdir( out_dir )
				fix_file0 = os.path.join( data_dir, input_files[i] )
				mov_file0 = os.path.join( data_dir, input_files[i] )
				elx_command = '%s -f0 %s -m0 %s -p %s -p %s -p %s -out %s'% (bin_elastix, fix_file0, mov_file0, par_file_dum, par_file_dum, par_file_dum, out_dir)				
				command_list.append(elx_command)
				counter = counter + 1		
			
		if command_list: 
			job_id = cc.send_job_array(command_list, 'day', '00:59:00', run_id,'4G',hold_job=job_id_list_use)		
			print len (job_id)
			if len(job_id) >1:
				job_id_1=job_id[1] 
				job_id_list = job_id_list + [job_id_1[:-2]]	
				
			
				
			if counter2 > 5 and counter > 1500:
				print 'Waiting...'
				# Wait until registrations are all done
				cc.wait(job_id_list)
				job_id_list_use = [];
				job_id_list = [];
				counter2 = 0
				counter = 0
			elif counter > 1500:				
				job_id_list_use = job_id_list	
				counter2=counter2 + 1 
				counter = 0
				job_id_list = []

	if job_id_list:
		print job_id_list
		cc.wait(job_id_list)
		
	# Step 2: Create mean transformation
	print('# Step 2: Create mean transformation')
	if force_next:
	    force = True
	
	trans_param_str = ''
	trans_param_val = 1.0 / len( train_files )
	for j in range(0, len( train_files)):
			l = train_list[j] 
			trans_param_str += (' ' + str( trans_param_val ) )
	
	mean_trans_files = []    
	for i in range(0, len( input_files )):
			k = list[i] 
			in_file  = os.path.join( work_dir, 'registration_' + k, k, 'TransformParameters.0.txt' )
			out_file = os.path.join( work_dir, 'registration_' + k, 'MeanTransformParameters.txt' )
	 
			# print "Setting transform parameters to '%s'" % trans_param_str
	        
			
			o = open( out_file,"w")
			data = open( in_file ).read()
			data = re.sub('Transform "BSplineTransform"','Transform "WeightedCombinationTransform"', data)
			data = re.sub('TransformParameters.*','TransformParameters %s)' % trans_param_str, data)
			data = re.sub('NumberOfParameters \d*', 'NumberOfParameters %d' % len( train_files ), data )
			data = re.sub('InitialTransformParametersFileName ".*"','InitialTransformParametersFileName "NoInitialTransform"', data)
			data = re.sub('ResultImagePixelType "short"', 'ResultImagePixelType "float"', data )
			data += '\n(SubTransforms'
	    
			for j in range(0, len( train_files )):
					l = train_list[j] 
					data += (' "' + os.path.join( work_dir, 'registration_' + k, l, 'TransformParameters.2.txt' ) + '"')
	
			data += ")"
	        
			o.write( data )
			o.close()
	
			mean_trans_files.append( out_file )	
	  
	# Step 3: Change the mean transform to a deformation transform
	print('# Step 3: Change the mean transform to a deformation transform')
	if force_next:
	    force = True
	
	
	command_list = []
	gzip_list = []
	trans_files = []
	for i in range(0, len( input_files )):
			k = list[i] 
			base_dir   = os.path.join( work_dir, 'registration_' + k )
			trans_file = os.path.join( base_dir, 'MeanTransformParameters.txt' )
			out_file   = os.path.join( base_dir, 'DefFieldTransformParameters.txt' )		
	        
			if force or (not os.path.exists( os.path.join( base_dir, 'deformationField.nii.gz' ) ) ) :
				force_next = True
				trans_command = '%s -tp %s -out %s -def all' % (bin_transformix, trans_file, base_dir)
				command_list.append(trans_command)
				
				o = open( out_file, "w")
				data = open( trans_def_field_tpl ).read()
				data = re.sub('__inputfile__', os.path.join( base_dir, 'deformationField.nii.gz' ), data)
				o.write( data )
				o.close()
	
			trans_files.append( out_file )
						
	# Send jobs	
	if command_list:
		job_id = cc.send_job_array( command_list, 'week', '99:00:00', run_id, '4G' )
	
		# Wait until transformations are all done
		cc.wait(job_id)
		
	# Step 4: Invert transforms
	print('# Step 4: Invert the transforms')
	if force_next:
	    force = True
	
	inverted_files = [] 
	command_list = []   
	for i, trans_file in enumerate( trans_files ):
			k = list[i] 
			print( '%03d: %s' % (i, trans_file ) ) 
			base_dir   = os.path.join( work_dir, 'registration_' + k )
			out_dir = os.path.join( base_dir, 'invert' )
			out_file = os.path.join( out_dir, 'TransformParameters.0.txt' )
			image_file = os.path.join( data_dir, input_files[i] )
	
			if not os.path.exists( out_dir ):
				os.mkdir( out_dir )	
	
			if force or (not os.path.exists( out_file ) ) :
				force_next = True
				invert_command = '%s -m %s -f %s -p %s -t0 %s -out %s' % ( bin_elastix, image_file, image_file, par_file_inv, trans_file, out_dir )
				command_list.append(invert_command)
	
			inverted_files.append( out_file )
				
	# Send jobs	
	if command_list:
		job_id = cc.send_job_array( command_list, 'day', '02:00:00', run_id, '6G' )		
			
		# Wait until transformations are all done
		cc.wait(job_id)	
		
	# Step 5a: Determine output image settings for transform file
	print('# Step 5a: Determine output image settings for transform file')
	
	corners_mean_space = [];
	command_list=[];
	for j, filename in enumerate( train_files ):
			print( '%03d: %s' % (j, filename ) )
			l =  train_list[j] 	
			image = os.path.join( train_data_dir, filename )
			out_file = os.path.join( work_dir, filename + '_corners.txt' )
	    
			print image
			corners = get_image_corners( bin_bbox, image )
	
			data = 'index\n%d\n' % len( corners )
			for index in corners:
				data += '%d %d %d\n' % (index[0], index[1], index[2])
	        
			o = open( out_file,"w")
			o.write( data )
			o.close()
	
			out_dir  = os.path.join( train_data_dir, work_subdir, 'mean_space_' + l )
	
			if not os.path.exists( out_dir ):
				os.mkdir( out_dir )	
	    
			if force or (not os.path.exists( os.path.join( out_dir, 'outputpoints.txt' ) ) ):
				force_next = True
				trans_command = '%s -def %s -tp %s -out %s' % (bin_transformix, out_file, mean_trans_files[i], out_dir) # Was inverted_files[i]
				command_list.append(trans_command)
				
			corners_mean_space.append( os.path.join( out_dir, 'outputpoints.txt' ) )
	
	# Send jobs
	if command_list:
		job_id = cc.send_job_array( command_list, 'day', '00:30:00', run_id, '10G')
			
		# Wait until transformations are all done
		cc.wait(job_id)
	
	corner_points = [];
	spacings = []
	for j, corner_file in enumerate( corners_mean_space ):
			print( '%03d: %s' % (j, corner_file) )
			l = train_list[j]  
			print os.path.join( train_data_dir, train_files[j] )
			info = get_image_info( bin_iinfo, os.path.join( train_data_dir, train_files[j] ) )
			spacings.append( info[1] )
	        
			i = open( corner_file,  "r")
	
			for line in i:
					point = re.search('^.*OutputPoint = \[ (-?[\d\.]+) (-?[\d\.]+) (-?[\d\.]+) \].*$', line).groups()
					point   = map(lambda x: float(x), point )
					corner_points.append( point )
	
	
	bound_lower = [min( corner_points, key=lambda x: x[0] )[0], min( corner_points, key=lambda x: x[1] )[1], min( corner_points, key=lambda x: x[2] )[2]]
	bound_upper = [max( corner_points, key=lambda x: x[0] )[0], max( corner_points, key=lambda x: x[1] )[1], max( corner_points, key=lambda x: x[2] )[2]]
	best_spacing = min( min( spacings, key=lambda x: min(x) ) )
	
	scaling_factor = pow( 10, math.ceil( -math.log10(best_spacing) ) );
	best_spacing = math.floor( best_spacing * scaling_factor ) / scaling_factor
	best_spacing = [best_spacing, best_spacing, best_spacing]
	
	origin = bound_lower;
	size_voxels   = map( lambda x, y, z: math.ceil((x-y) / z), bound_upper, bound_lower, best_spacing )
	
	
	# print ("Lower bound:", bound_lower)       
	# print ("Upper bound:", bound_upper)
	# print ("Origin:", origin)
	# print ("Size (voxel):", size_voxels)
	# print ("Spacing:", best_spacing)
	
	# Step 5b: Change output image settings in transform file
	print('# Step 5b: Change output image settings in transform file')
	if force_next:
	    force = True
	
	inverse_transform_files = []
	for in_file in inverted_files:
	    out_file = re.sub('TransformParameters.0.txt', 'InverseTransformParameters.txt', in_file )
	 
	    o = open( out_file,"w")
	    data = open( in_file ).read()
	    data = re.sub('\(Origin -?[\d\.]+ -?[\d\.]+ -?[\d\.]+\)','(Origin %f %f %f)' % ( origin[0], origin[1], origin[2] ), data)
	    data = re.sub('\(Index -?[\d\.]+ -?[\d\.]+ -?[\d\.]+\)','(Index 0 0 0)', data)
	    data = re.sub('\(Size [\d\.]+ [\d\.]+ [\d\.]+\)','(Size %f %f %f)' % ( size_voxels[0],  size_voxels[1],  size_voxels[2]  ), data)
	    data = re.sub('\(Spacing [\d\.]+ [\d\.]+ [\d\.]+\)','(Spacing %f %f %f)' % ( best_spacing[0], best_spacing[1], best_spacing[2]  ), data)
	    data = re.sub('\(Direction -?[\d\.]+ -?[\d\.]+ -?[\d\.]+ -?[\d\.]+ -?[\d\.]+ -?[\d\.]+ -?[\d\.]+ -?[\d\.]+ -?[\d\.]+\)','(Direction %f %f %f %f %f %f %f %f %f)' % ( 1, 0, 0, 0, 1, 0, 0, 0, 1  ), data)
	    data = re.sub('\(InitialTransformParametersFileName ".*"\)', '(InitialTransformParametersFileName "NoInitialTransform")', data)
	        
	    o.write( data )
	    o.close()
	    
	    inverse_transform_files.append( out_file )
	
	
	# Step 6: Transform the images
	print('# Step 6: Transform the images')
	if force_next:
	    force = True
	
	images_mean_space = []
	command_list = []
	for i, tr_file in enumerate( inverse_transform_files ):
			k =  list[i] 
			# in_file = input_files[k]
			in_file = os.path.join( data_dir, input_files[i] )
			out_dir  = os.path.join( work_dir, 'mean_space_' + k )
	
			if not os.path.exists( out_dir ):
				os.mkdir( out_dir )	
	    
			if force or (not os.path.exists( os.path.join( out_dir, 'result.nii.gz' ) ) ):
				force_next = True
				trans_command = '%s -in %s -tp %s -out %s -jac all' % (bin_transformix, in_file, tr_file, out_dir)
				command_list.append(trans_command)
	    
			images_mean_space.append( os.path.join( out_dir, 'result.nii.gz' ) )
	
	# Send jobs
	if command_list:
		job_id = cc.send_job_array( command_list, 'day', '00:20:00', run_id, '4G')
			
		# Wait until transformations are all done
		cc.wait(job_id)
	
	mean_shape_image = os.path.join( work_dir, 'mean_shape.nii.gz' )
	mean_shape_image_thresh = os.path.join( work_dir, 'pre_mask_mean_space_1.nii.gz' )
	mask_mean_space = os.path.join( work_dir, 'mask_mean_space.nii.gz' )
		
	if nomask:
		exit()
		
	# Step 10: Change output image settings in transform file
	print('# Step 10: Change output image settings in transform file')
	if force_next:
	    force = True
	
	back_transform_files = []
	for in_file in mean_trans_files:
	    in_dir_file = os.path.split( in_file )
	    in_file_ext = os.path.splitext( in_dir_file[1] )
	    out_file = os.path.join( in_dir_file[0], 'BackTransformParameters' + in_file_ext[1] )
	 
	    o = open( out_file,"w")
	    data = open( in_file ).read()
	    data = re.sub('\(FinalBSplineInterpolationOrder \d\)', '(FinalBSplineInterpolationOrder 0)', data)
	    data = re.sub('\(ResultImagePixelType ".*"\)', '(ResultImagePixelType "unsigned char")', data)
	        
	    o.write( data )
	    o.close()
	    
	    back_transform_files.append( out_file )
	
	
	
	# Step 11: Transform mask back to image spaces
	print('# Step 11: Transform mask back to image spaces')
	masks = []
	command_list = []
	for i, tr_file in enumerate( back_transform_files ):
			k =  list[i] 
			in_file = mask_mean_space
			out_dir  = os.path.join( work_dir, 'mask_' + k )
	
			if not os.path.exists( out_dir ):
				os.mkdir( out_dir )	
	    
			if force or (not os.path.exists( os.path.join( out_dir, 'result.nii.gz' ) ) ):
				force_next = True
				trans_command = '%s -in %s -tp %s -out %s' % (bin_transformix, in_file, tr_file, out_dir)
				command_list.append(trans_command)
				
			masks.append( os.path.join( out_dir, 'result.nii.gz' ) )
	
	# Send jobs
	if command_list:
		job_id = cc.send_job_array( command_list, 'day', '06:00:00', run_id, '4G' )		
			
		# Wait until jobs are all done
		cc.wait(job_id)
	
	
		
	# Step 12: align all shapes pairwise with elastix, create mean (masked)
	print('# Step 12: align all shapes pairwise with elastix, create mean (masked)')
	if force_next:
	    force = True
	    
	force=False
	
	counter = 0	
	counter2 = 0
	job_id_list = []; 
	job_id_list_use =[]
	for i in range(0, len( input_files )):
		k =  list[i] 
		print k
		base_dir = os.path.join( work_dir, 'registration_mask_' + k )
		if not os.path.exists( base_dir ):
			force_next = True
			os.mkdir( base_dir )
        
		command_list = [];	
		for j in range(0, len( train_files )):
			l =  train_list[j] 
			fix_file0 = os.path.join( data_dir, input_files[i] )
			mov_file0 = os.path.join( data_dir, train_files[j] )
			mask_file = masks[i]
        
			out_dir =  os.path.join( base_dir, l)

			if force or (not os.path.exists( os.path.join( out_dir, 'TransformParameters.2.txt' ) ) ):
				force_next = True
				if not os.path.exists( out_dir ):
					os.mkdir( out_dir )

				counter = counter + 1	
					
				elx_command = '%s -f0 %s -fMask %s -m0 %s -p %s -p %s -p %s -out %s'% (bin_elastix, fix_file0, mask_file, mov_file0, par_file_sim, par_file_aff, par_file_bsp, out_dir)
				command_list.append(elx_command)
					
		print counter
		print counter2
			
		if command_list: 
			job_id = cc.send_job_array(command_list, 'week', '00:35:00', run_id,hold_job=job_id_list_use)		
			
			job_id_1=job_id[1] 
			job_id_list = job_id_list + [job_id_1[:-2]]	
				
			if counter2 > 5 and counter > 1500:
				print 'Waiting...'
				# Wait until registrations are all done
				cc.wait(job_id_list)
				job_id_list_use = [];
				job_id_list = [];
				counter2 = 0
				counter = 0
			elif counter > 1500:				
				job_id_list_use = job_id_list	
				counter2=counter2 + 1 
				counter = 0
				job_id_list = []

	if job_id_list:
		print job_id_list
		cc.wait(job_id_list)
	
	# Step 13: Create mean transformation (masked)
	print('# Step 13: Create mean transformation (masked)')
	if force_next:
	    force = True
	
	trans_param_str = ''
	trans_param_val = 1.0 / len( train_files )
	for j in range(0, len( train_files)):
			l =  train_list[j] 
			trans_param_str += (' ' + str( trans_param_val ) )
	    
	for i in range(0, len( input_files )):
			k =  list[i] 
			in_file  = os.path.join( work_dir, 'registration_mask_' + k, k, 'TransformParameters.0.txt' )
			out_file = os.path.join( work_dir, 'registration_mask_' + k, 'MeanTransformParameters.txt' )
	 
			# print "Setting transform parameters to '%s'" % trans_param_str
	        
			o = open( out_file,"w")
			data = open( in_file ).read()
			data = re.sub('Transform "BSplineTransform"','Transform "WeightedCombinationTransform"', data)
			data = re.sub('TransformParameters.*','TransformParameters %s)' % trans_param_str, data)
			data = re.sub('NumberOfParameters \d*', 'NumberOfParameters %d' % len( train_files ), data )
			data = re.sub('InitialTransformParametersFileName ".*"','InitialTransformParametersFileName "NoInitialTransform"', data)
			data = re.sub('ResultImagePixelType "short"', 'ResultImagePixelType "float"', data )
			data += '\n(SubTransforms'
	    
			for j in range(0, len( train_files )):
					l =  train_list[j] 
					data += (' "' + os.path.join( work_dir, 'registration_mask_' + k, l, 'TransformParameters.2.txt' ) + '"')
	
			data += ")"
	        
			o.write( data )
			o.close()
	  
	# Step 14: Change the mean transform to a deformation transform (masked)
	print('# Step 14: Change the mean transform to a deformation transform (masked)')
	if force_next:
	    force = True
	
	trans_files = []
	command_list = []
	for i in range(0, len( input_files )):
			k =  list[i] 
			base_dir   = os.path.join( work_dir, 'registration_mask_' + k )
			trans_file = os.path.join( base_dir, 'MeanTransformParameters.txt' )
			out_file   = os.path.join( base_dir, 'DefFieldTransformParameters.txt' )
	    
	        
			if force or (not os.path.exists( os.path.join( base_dir, 'deformationField.nii.gz' ) ) ) :
				force_next = True
				trans_command = '%s -tp %s -out %s -def all' % (bin_transformix, trans_file, base_dir)
				command_list.append(trans_command)
				
				o = open( out_file, "w")
				data = open( trans_def_field_tpl ).read()
				data = re.sub('__inputfile__', os.path.join( base_dir, 'deformationField.nii.gz' ), data)
				o.write( data )
				o.close()
	
			trans_files.append( out_file )
	
	# Send jobs
	if command_list:
		job_id = cc.send_job_array( command_list, 'day', '08:00:00', run_id, '4G')		
			
		# Wait until transformations are all done
		cc.wait(job_id)
			
	# Step 15: Invert transforms (masked)
	print('# Step 15: Invert the transforms (masked)')
	if force_next:
	    force = True
	
	inverted_files = []    
	command_list = []
	for i, trans_file in enumerate( trans_files ):
			k =  list[i] 
			base_dir   = os.path.join( work_dir, 'registration_mask_' + k )
			out_dir = os.path.join( base_dir, 'invert' )
			out_file = os.path.join( out_dir, 'TransformParameters.0.txt' )
			image_file = os.path.join( data_dir, input_files[i] )
	
			if not os.path.exists( out_dir ):
				os.mkdir( out_dir )	
	
			if force or (not os.path.exists( out_file ) ) :
				force_next = True
				invert_command = '%s -m %s -f %s -p %s -t0 %s -out %s' % ( bin_elastix, image_file, image_file, par_file_inv, trans_file, out_dir )
				command_list.append(invert_command)
	
			inverted_files.append( out_file )
	
	# Send jobs
	if command_list:
		job_id = cc.send_job_array( command_list, 'day', '00:30:00', run_id, '10G')		
			
		# Wait until transformations are all done
		cc.wait(job_id)
	
	force = True
		
	# Step 16a: Determine output image settings for transform file
	print('# Step 16a: Determine output image settings for transform file')
	corners_mean_space = [];
	command_list = [];
	for j, filename in enumerate( train_files ):
			l =  train_list[j] 
			# image = os.path.join( data_dir, filename )
			# out_file = os.path.join( work_dir, filename + '_corners.txt' )
	    
			# corners = get_image_corners( bin_bbox, image )
	
			# data = 'index\n%d\n' % len( corners )
			# for index in corners:
				# data += '%d %d %d\n' % (index[0], index[1], index[2])
	        
			# o = open( out_file,"w")
			# o.write( data )
			# o.close()
	
			out_dir  = os.path.join( work_dir, 'mean_space_mask_' + l )
	
			# if not os.path.exists( out_dir ):
				# os.mkdir( out_dir )	
	    
			# if force or (not os.path.exists( os.path.join( out_dir, 'outputpoints.txt' ) ) ):
				# force_next = True
				# trans_command = '%s -def %s -tp %s -out %s' % (bin_transformix, out_file, inverted_files[i], out_dir)
				# command_list.append(trans_command)
	    
			corners_mean_space.append( os.path.join( out_dir, 'outputpoints.txt' ) )
	
	# # Send jobs
	# if command_list:
		# job_id = cc.send_job_array( command_list, 'day', '00:20:00', run_id, '10G')		
			
		# # Wait until transformations are all done
		# cc.wait(job_id)
	
	
	corner_points = [];
	spacings = []
	for j, corner_file in enumerate( corners_mean_space ):
			l = train_list[j] 
			info = get_image_info( bin_iinfo, os.path.join( train_data_dir, train_files[j] ) )
			spacings.append( info[1] )
	        
			i = open( corner_file,  "r")
	
			for line in i:
				point = re.search('^.*OutputPoint = \[ (-?[\d\.]+) (-?[\d\.]+) (-?[\d\.]+) \].*$', line).groups()
				point   = map(lambda x: float(x), point )
				corner_points.append( point )
	
	
	bound_lower = [min( corner_points, key=lambda x: x[0] )[0], min( corner_points, key=lambda x: x[1] )[1], min( corner_points, key=lambda x: x[2] )[2]]
	bound_upper = [max( corner_points, key=lambda x: x[0] )[0], max( corner_points, key=lambda x: x[1] )[1], max( corner_points, key=lambda x: x[2] )[2]]
	best_spacing = min( min( spacings, key=lambda x: min(x) ) )
	
	scaling_factor = pow( 10, math.ceil( -math.log10(best_spacing) ) );
	best_spacing = math.floor( best_spacing * scaling_factor ) / scaling_factor
	best_spacing = [best_spacing, best_spacing, best_spacing]
	
	origin = bound_lower;
	size_voxels   = map( lambda x, y, z: math.ceil((x-y) / z), bound_upper, bound_lower, best_spacing )
	
	
	print ("Lower bound:", bound_lower)       
	print ("Upper bound:", bound_upper)
	print ("Origin:", origin)
	print ("Size (voxel):", size_voxels)
	print ("Spacing:", best_spacing)
	
	# Step 16b: Change output image settings in transform file
	print('# Step 16b: Change output image settings in transform file')
	if force_next:
	    force = True
	
	inverse_transform_files = []
	for in_file in inverted_files:
	    out_file = re.sub('TransformParameters.0.txt', 'InverseTransformParameters.txt', in_file )
	 
	    o = open( out_file,"w")
	    data = open( in_file ).read()
	    data = re.sub('\(Origin -?[\d\.]+ -?[\d\.]+ -?[\d\.]+\)','(Origin %f %f %f)' % ( origin[0], origin[1], origin[2] ), data)
	    data = re.sub('\(Index -?[\d\.]+ -?[\d\.]+ -?[\d\.]+\)','(Index 0 0 0)', data)
	    data = re.sub('\(Size [\d\.]+ [\d\.]+ [\d\.]+\)','(Size %f %f %f)' % ( size_voxels[0],  size_voxels[1],  size_voxels[2]  ), data)
	    data = re.sub('\(Spacing [\d\.]+ [\d\.]+ [\d\.]+\)','(Spacing %f %f %f)' % ( best_spacing[0], best_spacing[1], best_spacing[2]  ), data)
	    data = re.sub('\(Direction -?[\d\.]+ -?[\d\.]+ -?[\d\.]+ -?[\d\.]+ -?[\d\.]+ -?[\d\.]+ -?[\d\.]+ -?[\d\.]+ -?[\d\.]+\)','(Direction %f %f %f %f %f %f %f %f %f)' % ( 1, 0, 0, 0, 1, 0, 0, 0, 1  ), data)
	    data = re.sub('\(InitialTransformParametersFileName ".*"\)', '(InitialTransformParametersFileName "NoInitialTransform")', data)
	        
	    o.write( data )
	    o.close()
	    
	    inverse_transform_files.append( out_file )
	
	# Step 17: Transform the images (masked)
	print('# Step 17: Transform the images (masked)')
	if force_next:
	    force = True
	
	force = True	
		
	images_mean_space = []
	command_list = []
	for i, tr_file in enumerate( inverse_transform_files ):
			k =list[i] 
			# in_file = input_files[k]
			in_file = os.path.join( data_dir, input_files[i] )
			out_dir  = os.path.join( work_dir, 'mean_space_mask_' + k )
	
			if not os.path.exists( out_dir ):
				os.mkdir( out_dir )	
	    
			if force or (not os.path.exists( os.path.join( out_dir, 'result.nii.gz' ) ) ):
				force_next = True
				trans_command = '%s -in %s -tp %s -out %s -jac all -def all' % (bin_transformix, in_file, tr_file, out_dir)
				command_list.append(trans_command)
	    
			images_mean_space.append( os.path.join( out_dir, 'result.nii.gz' ) )
			
	# Send jobs
	if command_list:
		job_id = cc.send_job_array( command_list, 'day', '00:20:00', run_id, '4G' )
	
		# Wait until transformations are all done
		cc.wait(job_id)
	    
	### THE END ###
    
if __name__ == '__main__':
    # Parse input arguments
    parser = OptionParser(description="Script for pairwise registration of subjects. The pairwise registrations are averaged to an atlas template.", usage="Usage: python %prog input_dir train_dir. Use option -h for help information.")
    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("wrong number of arguments")
        
    input_dir=args[0] 
    train_dir=args[1]    
        
    cc = ClusterControl()    
    work_subdir ='atlas_work_temp';
    threads=1
    performance=False
    nomask=True
    
    main( input_dir, train_dir, cc, threads, work_subdir, performance, nomask)
    
