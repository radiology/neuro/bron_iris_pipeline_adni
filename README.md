Pipeline for imaging processing and classification of structural MRI scans of AD, MCI and controls from the ADNI data base.

Esther E. Bron (Erasmus MC, Rotterdam, the Netherlands) - e.bron@erasmusmc.nl - September 2011-August 2014 

This pipeline processes imaging data (structural MRI) with the aim of classification of dementia. It uses features based on 1) atlas/region volumetry (pipeline_roi.py) and 2) voxel-based morphometry (pipeline_voxel.py). Classification is performed using an hard-margin linear SVM classifier. Classification scripts are not yet included but will be added.

Prerequisites:
-	Hammers atlas (Hammers et al, 2003; Gousias et al, 2008), see http://www.brain-development.org/ for download of the atlas with 20 scans
-	Elastix: http://elastix.isi.uu.nl/
-	Matlab
-	SPM8 standalone: http://www.fil.ion.ucl.ac.uk/spm/software/spm8/
-	ITK tools: https://github.com/ITKTools/ITKTools
-	FSL: http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/
-	LibSVM: http://www.csie.ntu.edu.tw/~cjlin/libsvm/
-	PRtools: http://prtools.org/
-	Matlab Nifti tools: http://www.mathworks.com/matlabcentral/fileexchange/8797-tools-for-nifti-and-analyze-image
-	Environmentmodules.py and clustercontrol.py by Hakim Achterberg, these parts are specific for our computation cluster and could be replaces.

Contact:
If you use this methodology or any of the scripts we would appreciate if you cite the following article:
E.E. Bron, R.M.E Steketee, G. Houston, R.A. Oliver, H.C. Achterberg, M. Loog, J.C. van Swieten, A. Hammers, W.J. Niessen, M. Smits and S. Klein (2014): Diagnostic classification of arterial spin labeling and structural MRI in presenile early-stage dementia. Hum Brain Mapp 35:4916-4931 (http://onlinelibrary.wiley.com/doi/10.1002/hbm.22522/full).

For questions on the methodology or any bugs, please don't hesitate to contact Esther Bron, e.bron@erasmusmc.nl