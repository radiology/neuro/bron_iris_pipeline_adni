#!/bin/env python

# Do SPM tissue segmentation
# Runs matlab function ~/Matlab/run_batch_segment_cuingnet_standalone.m
# Note: make sure matlab mcr module is loaded and elastix module is unloaded

# Call from other python script with: spm_tissue_segmentation.main(input_dir, cc,  threads, work_subdir)
# Or from command line with:  python spm_tissue_segmentation.py input_dir

# July 2013-August 2014
# Esther Bron - e.bron@erasmusmc.nl 

import os
import subprocess
import re
import sys
import datetime
import time
import shutil
import math
from bigr.clustercontrol import ClusterControl
from optparse import OptionParser

#cc = ClusterControl()

use_cluster = True

def main(input_dir, cc, threads, work_subdir):	
	force=False
	
	# Store data dir
# 	input_dir = os.path.abspath(os.path.expanduser(sys.argv[1]))
	print "Input dir: " + input_dir
	data_dir = os.path.join( input_dir, 'T1w' )
	print "Data dir: " + data_dir
	# Make SPM directory
	spm_dir=os.path.join(data_dir,'spm')
	scripts_dir=os.path.dirname(os.path.realpath(__file__))
	if not os.path.exists(spm_dir):
		os.mkdir(spm_dir)
	
	# Read and filter input files
	input_files = [];
	for filename in os.listdir( data_dir ):
		if re.match('.*.nii.gz', filename ):
			input_files.append( filename )
			
	input_files.sort()
	
	list = [];		
	for i, filename in enumerate( input_files ):
			(filebase,ext)=os.path.splitext(filename) 
			(filebase,ext)=os.path.splitext(filebase) 
			print( '%03d: %s' % (i, filebase ) )
			list.append( filebase )
	
	# Creating a run_id
	print 'Hashing %s' % sys.argv[0] + os.path.join( data_dir, work_subdir )
	run_id = (sys.argv[0] + os.path.join( data_dir, work_subdir ) + 'saltnpepper' ).__hash__()
	print 'Hash tag for this session: %d' % run_id
	run_id = str( abs( run_id ) )
	run_id = 'spm' +  run_id[0:7]
	
	#force = True
	force_next = False
	
	# Set binaries
	bin_spm='/SPM/spm_exec/run_spm8.sh' # Make sure this points to spm8
	bin_mcr='/cm/shared/apps/mcr/R2013b' # Make sure this points to the matlab mcr
	batch_script=os.path.join(scripts_dir, 'run_batch_segment_cuingnet_standalone.m')
	
	
	# Copy nifti files to SPM directory
	# Unzip nifti files
	command_list = []
	for i in list:
		if not ( os.path.exists(os.path.join(spm_dir, 'wc1w' + i + '.nii.gz' )) or os.path.exists(os.path.join(spm_dir, i + '.nii' ))):
			src=os.path.join(data_dir, i + '.nii.gz')
			dst=os.path.join(spm_dir, i + '.nii.gz')
			if not os.path.exists(dst):
				shutil.copyfile(src,dst)
			gunzip_command='gunzip %s'% (dst)
			command_list.append(gunzip_command)
		
	if command_list:		
		job_id = cc.send_job_array( command_list, 'hour', '00:15:00', run_id)		
	
		# Wait until registrations are all done
		cc.wait(job_id)
		command_list=[]
	
	# Check origin? -> SPM display (90,-126,-72)
	# Run SPM
	spm_command=['bash %s %s script %s'% (bin_spm, bin_mcr, batch_script)]
	print spm_command
	job_id = cc.send_job_array(spm_command, queue='week', job_name=run_id, memory='5G')
	
	cc.wait(job_id)
	
	# Zip nifti files and clear spm folder
	command_list = []
	for i in list:
		if not  os.path.exists(os.path.join(spm_dir, 'wc1w' + i + '.nii.gz' )):
			if os.path.exists(os.path.join(spm_dir, i + '.nii')):
				os.remove(os.path.join(spm_dir, i + '.nii'))
			if os.path.exists(os.path.join(spm_dir, 'y_result.nii')):
				os.remove(os.path.join(spm_dir, 'y_result.nii'))
			for prefix in ['c1w','c2w','c3w','mw','w']:
				if os.path.exists(os.path.join(spm_dir, prefix + i + '.nii')):
					os.remove(os.path.join(spm_dir, prefix + i + '.nii'))
			for prefix in ['wc1w','wc2w','wc3w']:
				dst=os.path.join(spm_dir, prefix + i + '.nii')
				if os.path.exists(dst):
					gzip_command='gzip %s'% (dst)
					command_list.append(gzip_command)
				else:
					print 'SPM failed for ' + i
		
	if command_list:		
		job_id = cc.send_job_array( command_list, 'hour', '00:15:00', run_id)		
	
		# Wait until registrations are all done
		cc.wait(job_id)
		command_list=[]
		
if __name__ == '__main__':
	# Parse input arguments
	parser = OptionParser(description="Do SPM tissue segmentation", usage="Usage: python %prog input_dir. Use option -h for help information.")
	(options, args) = parser.parse_args()

	if len(args) != 1:
		parser.error("wrong number of arguments")
		
	input_dir=args[0]	
		
	cc = ClusterControl()	
	work_subdir ='atlas_work_temp';
	threads=1
	
	main(input_dir, cc, threads, work_subdir)