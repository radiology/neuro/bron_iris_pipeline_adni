#!/bin/env python

# Pipeline for processing data for voxel-wise classification
# Requires running pipeline_roi_cuingnet first (step 1, 2, 3)

# Step 1: Create atlas based on 150 training AD/CN subjects
# Step 2: Transform test images to atlas
# Step 3: Make binary GM segmentation from probabilistic SPM output
# Step 4: Transform tissue segmentation to template space
# Step 5: Calculate features

# Extra:
# Step 6: Do classification

# Prerequisites 
# - Elastix
# - Matlab
# - itktools
# - SPM
# - FSL (bet)

# Folder structure
# Project folder (scripts/pipeline are ran from here)
#	T1w
#		subjects
#			*.nii.gz
#		brain_mask_bet (made in pipeline_roi_cuingnet step 1)
#		spm (made in step pipeline_roi_cuingnet 2, used in step 3)
#		atlas_work_temp (made in step 1)
#			spm_template_space (made in step 4)
#	Hammers (made in step pipeline_roi_cuingnet 3)
#	Features (made in step pipeline_roi_cuingnet 4, used in step 5)

# July 2013-August 2014
# Esther Bron - e.bron@erasmusmc.nl 

import os
import subprocess
import re
import sys
import datetime
import time
import shutil
import math
from bigr.environmentmodules import EnvironmentModules
from bigr.clustercontrol import ClusterControl

env = EnvironmentModules()
cc = ClusterControl()

performance = False
nomask = True

# Elastix performance branch
if performance:
	elastix='elastix/elastixperf_01_2/'
	threads=4
else:
	elastix='elastix/4.6'  #/elastix_04_6'
	threads=1  

#Environment modules
matlab='matlab/R2013b'
mcr='mcr/R2013b'
itktools='itktools'

def load_elastix():
	env.unload('elastix')
	env.unload(matlab)
	env.unload(mcr)
	env.load(elastix)
	env.load(itktools)
	print env.loaded_modules	

def load_matlab():
	env.load(matlab)
	env.load(mcr)
	env.unload(elastix)
	env.unload(itktools)	
	print env.loaded_modules	



# Which ROI labelings should be transformed to template space: roi_list=['seg','lobe','hemisphere','brain', 'brain_mask']
# Seg is required to segment cerebellum.
roi_list=['seg', 'brain_mask']

load_elastix()

# Creating a run_id
if performance:
	work_subdir ='atlas_work_temp_performance'
else:
	work_subdir ='atlas_work_temp' #_4_6

input_dir=os.getcwd()
train_dir = os.path.join(os.pardir, 'Cuingnet_data')
scripts_dir=os.path.dirname(os.path.realpath(__file__))
print input_dir

# Step 1: Create atlas based on 150 training AD/CN subjects
print '# Step 1: Create atlas based on 150 training AD/CN subjects'
import create_atlas_adni
create_atlas_adni.main(input_dir, cc,  threads, work_subdir, performance,nomask)
 		
# Step 2: Transform test images to atlas
print '# Step 2: Transform test images to atlas'
import train_atlas_adni
train_atlas_adni.main(input_dir, train_dir, cc,  threads, work_subdir, performance,nomask)
 
# Step 3: Make binary GM segmentation from probabilistic SPM output
print '# Step 3: Make binary GM segmentation from probabilistic SPM output'
os.chdir(os.path.join(scripts_dir, 'Matlab' ))
load_matlab()
 
matlab_command = './hard_gm_segmentation_cluster %s 0' % (input_dir)
 
# Send jobs to cluster
run_id='hard_segm'
command_list=[]
command_list.append(matlab_command)
if command_list:
	job_id = cc.send_job_array( command_list, 'day', '05:00:00', run_id, '10G')	
 	
	cc.wait(job_id)
 
os.chdir(input_dir)
load_elastix()
 
# Step 4: Transform tissue segmentation to template space
print '# Step 4: Transform tissue segmentation to template space'
roi_list=['seg', 'brain_mask']
 
import transform_tissue_segm
for use_hard_segmentation in [1, 0]:
	print 'use_hard_segmentation = ' + str(use_hard_segmentation)
	transform_tissue_segm.main( input_dir, cc, threads, work_subdir, performance, nomask, use_hard_segmentation, roi_list )
 
if train_dir != input_dir and (not os.path.lexists(os.path.join(input_dir,'Hammers','atlas_work_temp'))):
	os.symlink(os.path.join(train_dir,'Hammers','atlas_work_temp'),os.path.join(input_dir,'Hammers','atlas_work_temp'))
	
# Step 5: Calculate features
print '# Step 5: Calculate features'	
import construct_voxelwise_features
for use_hard_segmentation in [1, 0]:
	print use_hard_segmentation
	construct_voxelwise_features.main( input_dir, cc, threads, work_subdir, performance, nomask, use_hard_segmentation )

# Step 6: Do classification
print '# Step 6: Do classification'	
os.chdir(os.path.join(scripts_dir, 'do_classification' ))

load_matlab()

command_list=[]
if performance:
	elastix_version='performance'
else:
	elastix_version='4.6'

	
matlab_command = './do_classification gm vbm study Cuingnet_data elastix_version %s no_mask %i' % (elastix_version, int(nomask))
run_id='class_vox_adni_' + elastix_version

command_list.append(matlab_command)

logfile=os.path.join(input_dir, 'Features', work_subdir, run_id + '.txt')
	    
# Send jobs
if command_list:
	job_id = cc.send_job_array( command_list, 'day', '03:00:00', run_id, '30G', outputLog=logfile)				