#!/bin/env python
# Transforms tissue segmentation to the mean space for each subject.
# Call from other python script with: transform_tissue_segm.main( data_dir, cc, threads, work_subdir, performance, nomask, use_hard_segmentation, seg_list )
# Or from command line with:  python transform_tissue_segm.py input_dir use_hard_segmentation

# September 2011-August 2014
# Esther Bron - e.bron@erasmusmc.nl 


import os
import subprocess
import re
import sys
import datetime
import time
import shutil
import math
from bigr.environmentmodules import EnvironmentModules
from bigr.clustercontrol import ClusterControl
from optparse import OptionParser

use_cluster = True

def main( data_dir, cc, threads, work_subdir, performance, nomask, use_hard_segmentation, seg_list ):
#	# Check arguments
#	if ( len( sys.argv ) < 2 or len( sys.argv ) > 3):
#	    print 'Usage: %s directory [force]' % sys.argv[0]
#	    exit(1) 
#	
	# Store data dir
#	data_dir = os.path.abspath(os.path.expanduser(sys.argv[1]))
	print "Data dir: " + data_dir
	t1w_dir = os.path.join( data_dir, 'T1w' )
	print "T1-weighted dir: " + t1w_dir
	hammers_dir = os.path.join( data_dir, 'Hammers' )	
	scripts_dir=os.path.dirname(os.path.realpath(__file__))	
	    
	work_dir = os.path.join(t1w_dir, work_subdir)
	if not os.path.exists( work_dir ):
		os.mkdir(work_dir ) 
		
	# Read and filter input files
	input_files = [];
	for filename in os.listdir( t1w_dir ):
		if re.match('.*.nii.gz', filename ):
			input_files.append( filename )
			
	input_files.sort()
	
	#for k, filename in enumerate( input_files ):
	#    print( '%03d: %s' % (k, filename ) )
	
	list = [];		
	for i, filename in enumerate( input_files ):
			(filebase1,ext)=os.path.splitext(filename) 
			(filebase,ext)=os.path.splitext(filebase1) 
			#(iris, filebase)=filebase.split('_')
			print( '%03d: %s' % (i, filebase ) )
			list.append( filebase )
			
	
	# Creating a run_id
	print 'Hashing %s' % sys.argv[0] + os.path.join( data_dir, work_subdir )
	run_id = (sys.argv[0] + os.path.join( data_dir, work_subdir ) + 'saltnpepper' ).__hash__()
	print 'Hash tag for this session: %d' % run_id
	run_id = str( abs( run_id ) )
	run_id = 'ip_tis' +  run_id[0:4]
	
	if (len( sys.argv ) >= 4):
	    force = bool( int( sys.argv[3]  ) )
	    print( 'Force taken from command line: ' + str(force) )
	else:
	    force = False
	
	force = False
	force_next = False
	
	# Set binaries
	bin_transformix   = 'transformix -threads ' + str(threads)
	bin_combine		  = 'pxcombinesegmentations'
	bin_convert = 'pxcastconvert'
	
	dir_params      = os.path.join(scripts_dir, 'params')
	
	par_file_sim  = 'par_atlas_sim.txt'
	par_file_aff  = 'par_atlas_aff.txt'
	par_file_aff_output  = 'par_atlas_aff_output.txt'
	par_file_rigid_output  = 'par_atlas_rigid_output.txt'
	#par_file_bsp  = 'par_atlas_bsp.txt'
	par_file_bsp  = 'par_atlas_bsp_grid.txt'
	#par_file_inv  = 'par_atlas_invert.txt'
	par_file_inv  = 'par_atlas_invert_grid.txt'
	par_file_dum  = 'par_dummy.txt'
	
	# Create absolute path names
	par_file_sim = os.path.join( dir_params, par_file_sim )
	par_file_aff = os.path.join( dir_params, par_file_aff )
	par_file_aff_output = os.path.join( dir_params, par_file_aff_output )
	par_file_rigid_output = os.path.join( dir_params, par_file_rigid_output )
	par_file_bsp = os.path.join( dir_params, par_file_bsp )
	par_file_dum = os.path.join( dir_params, par_file_dum )
	par_file_inv = os.path.join( dir_params, par_file_inv )
	
	trans_def_field_tpl = os.path.join( dir_params, 'deformation_transform.tpl.txt' )
	
		
	# Step 1: Transform tissue segmentations to mean space
	print('# Step 1: Transform tissue segmentations to mean space')
	if force_next:
	    force = True
	
	if use_hard_segmentation:
		out1_dir  = os.path.join( t1w_dir, work_subdir, 'spm_template_space' )	
	else:
		out1_dir  = os.path.join( t1w_dir, work_subdir, 'spm_prob_template_space' )
	if nomask:
		out1_dir = out1_dir + '_nomask'
	if not os.path.exists( out1_dir ):
		os.mkdir(out1_dir ) 
		
	feature_dir  = os.path.join( data_dir, 'Features' )	
	if not os.path.exists( out1_dir ):
		os.mkdir(out1_dir ) 	
		
	feature_work_dir  = os.path.join( feature_dir, work_subdir )	
	if not os.path.exists( feature_work_dir ):
		os.mkdir(feature_work_dir ) 	
		
	feature_work_dir_nomask = os.path.join( feature_dir, work_subdir + '_nomask')	
	if not os.path.exists( feature_work_dir_nomask ):
		os.mkdir(feature_work_dir_nomask )
		
	images_mean_space = []
	command_list = [];	
	for i in range(0, len( input_files )):
		k = list[i] 
		# in_file = input_files[k]
		
		if use_hard_segmentation:	
			in_file = os.path.join(  t1w_dir, 'spm', k + '_gm.nii' )
			if not os.path.exists( in_file ):
				in_file = os.path.join ( t1w_dir, 'spm', k + '_gm.nii.gz')
				
				
			if nomask:
				tr_file = os.path.join( t1w_dir, work_subdir ,'registration_' + k, 'invert', 'InverseTransformParameters.nn.txt' )
				ini_file = os.path.join( t1w_dir, work_subdir ,'registration_' + k, 'invert', 'InverseTransformParameters.txt' )
			else:		
				tr_file = os.path.join( t1w_dir, work_subdir ,'registration_mask_' + k, 'invert', 'InverseTransformParameters.nn.txt' )
				ini_file = os.path.join( t1w_dir, work_subdir ,'registration_mask_' + k, 'invert', 'InverseTransformParameters.txt' )	
			
			if (not os.path.exists( tr_file ) ):
				o = open( tr_file,"w")
				data = open( ini_file ).read()
				data = re.sub('ResampleInterpolator "FinalBSplineInterpolator"','ResampleInterpolator "FinalNearestNeighborInterpolator"', data)
						
				o.write( data )
				o.close()
		else:
			in_file = os.path.join(  t1w_dir, 'spm', 'wc1w' + k + '.nii.gz' )
			if nomask:
				tr_file = os.path.join( t1w_dir, work_subdir ,'registration_' + k, 'invert', 'InverseTransformParameters.txt' )
			else:		
				tr_file = os.path.join( t1w_dir, work_subdir ,'registration_mask_' + k, 'invert', 'InverseTransformParameters.txt' )
		
		out_dir  = os.path.join( out1_dir, k )	
			
		if not os.path.exists( out_dir ):
			os.mkdir(out_dir ) 
		
		if force or (not os.path.exists( os.path.join( out_dir, 'result.nii.gz' ) ) ):
			force_next = True
			trans_command = '%s -in %s -tp %s -out %s' % (bin_transformix, in_file, tr_file, out_dir)
			command_list.append(trans_command)
				
		images_mean_space.append ( os.path.join( out_dir, 'result.nii.gz' ) )
		
	# Send jobs		
	if command_list:		
		job_id = cc.send_job_array( command_list, 'hour', '00:30:00', run_id)		
	
		# Wait until registrations are all done
		cc.wait(job_id)
		
	if use_hard_segmentation:
		# Step 2a: Create majority vote mask
		print('# Step 2a: Create majority vote mask')
		if nomask:
			mask_out = os.path.join( feature_work_dir_nomask, 'gm_vote_mask.nii.gz' )
		else:
			mask_out = os.path.join( feature_work_dir, 'gm_vote_mask.nii.gz' )
	
		if force or (not os.path.exists( mask_out ) ):
			gm_masks=[]
			command_list=[]
			for i in range(0, len( input_files )): #Loop over all subjects
				k = list[i]
				gm_mask_ms  = os.path.join(out1_dir, k, 'result.nii.gz')
				
				convert_command = '%s -in %s -opct %s -out %s' % (bin_convert, gm_mask_ms, 'unsigned_char',gm_mask_ms)
				command_list.append(convert_command)
				gm_masks.append(gm_mask_ms)	
	
			# Send jobs		
			if command_list:	
				print 'Convert to char'	
				job_id = cc.send_job_array( command_list, 'day', '00:10:00', run_id, '5G')		
	
				# Wait until registrations are all done
				cc.wait(job_id) 	
				
			combine_command = [ '-n', '2', '-m', 'VOTE','-in'] + gm_masks + ['-outh', mask_out + '.nii.gz', '-outs', mask_out + '_0.nii.gz' , mask_out + '_1.nii.gz']
			print combine_command
			job_id = cc.send_job( bin_combine, combine_command, 'hour', '00:30:00', run_id, '30G' )	
			print job_id
			cc.wait([job_id])	
	
		# Step 2b: Create OR-mask
		print('# Step 2b: Create OR-mask')
		if nomask:
			gm_or_mask = os.path.join( feature_work_dir_nomask, 'gm_or_mask.nii.gz' )
		else:
			gm_or_mask = os.path.join( feature_work_dir, 'gm_or_mask.nii.gz' )
	
		if force or (not os.path.exists( gm_or_mask ) ):	
			gm_masks=[]
			command_list=[]
			for i in range(0, len( input_files )): #Loop over all subjects
				k = list[i]
				gm_mask_ms  = os.path.join(out1_dir, k, 'result.nii.gz')
				
				convert_command = '%s -in %s -opct %s -out %s' % (bin_convert, gm_mask_ms, 'unsigned_char',gm_mask_ms)
				# command_list.append(convert_command)
				gm_masks.append(gm_mask_ms)	
				
			# Send jobs		
			if command_list:		
				job_id = cc.send_job_array( command_list, 'day', '00:10:00', run_id, '5G')		
	
				# Wait until registrations are all done
				cc.wait(job_id) 				
	
				
			sum_command = ['-in', gm_masks[0], gm_masks[1], '-ops', 'OR', '-out', gm_or_mask ]
			print sum_command
			job_id = cc.send_job( 'pxlogicalimageoperator' , sum_command, 'hour', '00:10:00', run_id, '10G' )
			# Wait until last job is done	
			cc.wait([job_id])
			
			for b in range(2, len(gm_masks)):
				sum_command = ['-in', gm_masks[b], gm_or_mask, '-ops', 'OR', '-out', gm_or_mask ]
				print sum_command
				job_id = cc.send_job( 'pxlogicalimageoperator' , sum_command, 'hour', '00:10:00', run_id, '10G' )
				# Wait until last job is done	
				cc.wait([job_id])
	
	if nomask==False and os.path.exists(os.path.join(data_dir, 'DTI')):	
		# Step 3: Transform WM segmentations to mean space
		print('# Step 3: Transform WM segmentations to mean space')
		if force_next:
		    force = True
		
		if use_hard_segmentation:
			out1_dir  = os.path.join( t1w_dir, work_subdir, 'wm_template_space' )	
		else:
			out1_dir  = os.path.join( t1w_dir, work_subdir, 'wm_prob_template_space' )
		if not os.path.exists( out1_dir ):
			os.mkdir(out1_dir ) 
			
			
		images_mean_space = []
		command_list = [];	
		for i in range(0, len( input_files )):
			k = list[i] 
			# in_file = input_files[k]
			
			if use_hard_segmentation:	
				in_file = os.path.join(  t1w_dir, 'spm', k + '_wm.nii' )
				if not os.path.exists( in_file ):
					in_file = os.path.join ( t1w_dir, 'spm', k + '_wm.nii.gz')
					
				tr_file = os.path.join( t1w_dir, work_subdir ,'registration_mask_' + k, 'invert', 'InverseTransformParameters.nn.txt' )
				ini_file = os.path.join( t1w_dir, work_subdir ,'registration_mask_' + k, 'invert', 'InverseTransformParameters.txt' )	
				
				if (not os.path.exists( tr_file ) ):
					o = open( tr_file,"w")
					data = open( ini_file ).read()
					data = re.sub('ResampleInterpolator "FinalBSplineInterpolator"','ResampleInterpolator "FinalNearestNeighborInterpolator"', data)
							
					o.write( data )
					o.close()
			else:
				in_file = os.path.join(  t1w_dir, 'spm', 'wc2w' + k + '.nii.gz' )
				tr_file = os.path.join( t1w_dir, work_subdir ,'registration_mask_' + k, 'invert', 'InverseTransformParameters.txt' )
			
			out_dir  = os.path.join( out1_dir, k )	
				
			if not os.path.exists( out_dir ):
				os.mkdir(out_dir ) 
			
			if force or (not os.path.exists( os.path.join( out_dir, 'result.nii.gz' ) ) ):
				force_next = True
				trans_command = '%s -in %s -tp %s -out %s' % (bin_transformix, in_file, tr_file, out_dir)
				command_list.append(trans_command)
					
			images_mean_space.append ( os.path.join( out_dir, 'result.nii.gz' ) )
			
		# Send jobs		
		if command_list:		
			job_id = cc.send_job_array( command_list, 'hour', '00:10:00', run_id)		
		
			# Wait until registrations are all done
			cc.wait(job_id)
		
		if use_hard_segmentation :
			# Step 4a: Create majority vote mask
			print('# Step 4a: Create majority vote mask')
			mask_out = os.path.join( feature_dir, work_subdir, 'wm_vote_mask.nii.gz' )
			print mask_out
		
			if force or (not os.path.exists( mask_out ) ):
				gm_masks=[]
				command_list=[]
				for i in range(0, len( input_files )): #Loop over all subjects
					k = list[i]
					gm_mask_ms  = os.path.join(out1_dir, k, 'result.nii.gz')
					
					convert_command = '%s -in %s -opct %s -out %s' % (bin_convert, gm_mask_ms, 'unsigned_char',gm_mask_ms)
					command_list.append(convert_command)
					gm_masks.append(gm_mask_ms)	
		
				# Send jobs		
				if command_list:		
					job_id = cc.send_job_array( command_list, 'hour', '00:10:00', run_id, '5G')		
		
					# Wait until registrations are all done
					cc.wait(job_id) 	
					
				combine_command = [ '-n', '2', '-m', 'VOTE', '-in'] + gm_masks + ['-outh', mask_out]
				job_id = cc.send_job( bin_combine, combine_command, 'hour', '00:30:00', run_id, '10G' )	
				print combine_command
				print job_id
				cc.wait([job_id])	
	
		
	# Step 3: Transform Hammers atlases to mean space
	print('# Step 3: Transform Hammers atlases to mean space')
	if os.path.exists(os.path.join( hammers_dir, work_subdir)):
		if force_next:
		    force = True
	#	seg_list=['seg','lobe','hemisphere','brain','brain_mask']
	
		for seg in seg_list:
	#		seg = seg_list[roi_nr]	
	#		nr = nr_list[roi_nr]
			
			if seg in ['brain_mask']:
				#Brain mask
				nr=2
			elif seg in ['brain']:
				#Brain 
				nr=3
			elif seg in ['hemisphere']:
			    #Hemisphere
				nr=4			
			elif seg in ['lobe']:
				#Lobe
				nr=12
			else:
				#seg
				nr=84
			
			print seg
					
			seg_dir  = os.path.join( hammers_dir, work_subdir, 'template_space_' + seg )	
			if nomask:
				seg_dir = seg_dir + '_nomask' 
			if not os.path.exists( seg_dir ):
				os.mkdir(seg_dir ) 
			
			command_list = [];	
			for i in range(0, len( input_files )):
				k = list[i] 
				# in_file = input_files[k]
				if nomask:
					tr_file = os.path.join( t1w_dir, work_subdir ,'registration_' + k, 'invert', 'InverseTransformParameters.nn.txt' )
				else:
					tr_file = os.path.join( t1w_dir, work_subdir ,'registration_mask_' + k, 'invert', 'InverseTransformParameters.nn.txt' )
				
				
				in_file = os.path.join(  hammers_dir, work_subdir, k, seg + '.nii.gz' )
				if not os.path.exists(in_file):
					print('ERROR: ' + in_file + 'does not exist! Run pipeline_roi first to the region labeling in subject space.')
					exit()
						
				out_dir  = os.path.join( seg_dir, k )	
				if not os.path.exists( out_dir ):
					os.mkdir(out_dir ) 
				
				if force or (not os.path.exists( os.path.join( out_dir, 'result.nii.gz' ) ) ):
					force_next = True
					trans_command = '%s -in %s -tp %s -out %s' % (bin_transformix, in_file, tr_file, out_dir)
					command_list.append(trans_command)
				
			# mask_dir  = os.path.join( hammers_dir, work_subdir, 'template_space_brain_mask' )	
			# if not os.path.exists( mask_dir ):
				# os.mkdir(mask_dir ) 
				
			# for i in range(0, len( input_files )):
				# k = list[i] 
				# # in_file = input_files[k]
				# tr_file = os.path.join( t1w_dir, work_subdir ,'registration_mask_' + k, 'invert', 'InverseTransformParameters.nn.txt' )
				# in_file = os.path.join(  hammers_dir, work_subdir, k, 'brain_mask.nii.gz' )
				# out_dir  = os.path.join( mask_dir, k )	
					
				# if not os.path.exists( out_dir ):
					# os.mkdir(out_dir ) 
				
				# if force or (not os.path.exists( os.path.join( out_dir, 'result.nii.gz' ) ) ):
					# force_next = True
					# trans_command = '%s -in %s -tp %s -out %s' % (bin_transformix, in_file, tr_file, out_dir)
					# command_list.append(trans_command)		
			
			# Send jobs		
			if command_list:		
				job_id = cc.send_job_array( command_list, 'day', '00:10:00', run_id)		
		
				# Wait until registrations are all done
				cc.wait(job_id)  
					
			force = True		
			# Step 4: Create majority vote mask
			print('# Step 4: Create majority vote mask')
			seg_out = os.path.join( feature_work_dir, seg + '_template.nii.gz' )
			# mask_out = os.path.join( feature_dir, 'brain_mask_template.nii.gz' )	
		
			if force or (not os.path.exists( seg_out ) ):
				k = list[i]
				segs=[]
				# masks=[]
				command_list=[]
				os.chdir(seg_dir)
				for i in range(0, len( input_files )): #Loop over all subjects
					k = list[i] 
					seg  = os.path.join(k, 'result.nii.gz')
		
					# mask  = os.path.join(mask_dir, k, 'result.nii.gz')
					
					segs.append(seg)
					# masks.append(mask)
					
					convert_command = '%s -in %s -opct %s -out %s' % (bin_convert, seg, 'unsigned_char',seg)
					command_list.append(convert_command)
					# convert_command = '%s -in %s -opct %s -out %s' % (bin_convert, mask, 'unsigned_char',mask)
					# command_list.append(convert_command)
					
				# Send jobs		
				if command_list:		
					job_id = cc.send_job_array( command_list, 'day', '00:15:00', run_id, '10G')		
		
					# Wait until registrations are all done
					cc.wait(job_id) 	
		
	#			combine_command = [ '-n', nr, '-m', 'VOTE', '-in'] + segs + ['-outh', seg_out]
	
				combine_command2 = [bin_combine, '-n', str(nr), '-m', 'VOTE', '-in'] + segs + ['-outh', seg_out]
				command_list=[]
				combine_command2 = ' '.join(combine_command2)	
				command_list.append(combine_command2)
	
				if command_list:		
					job_id = cc.send_job_array( command_list, 'day', '02:00:00', run_id, '20G')		
					print job_id
					# Wait until registrations are all done
					cc.wait(job_id)  
				
				os.chdir(data_dir)
	#			job_id = cc.send_job( bin_combine, combine_command, 'day', '00:30:00', run_id, '20G' )	
	#			print job_id
	#			cc.wait([job_id])
		
				# combine_command = [ '-n', '2', '-m', 'VOTE', '-in'] + masks + ['-outh', mask_out]
				# job_id = cc.send_job( bin_combine, combine_command, 'hour', '00:30:00', run_id, '10G' )	
				# print job_id
				# cc.wait([job_id])
				
if __name__ == '__main__':
    # Parse input arguments
    parser = OptionParser(description="Transforms tissue segmentation to the mean space for each subject.", usage="Usage: python %prog input_dir use_hard_segmentation. Use option -h for help information.")
    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("wrong number of arguments")
        
    data_dir=args[0]    
    use_hard_segmentation=args[1]
        
    cc = ClusterControl()    
    work_subdir ='atlas_work_temp';
    threads=1
    performance=False
    nomask=True
    lobe_list=['seg','brain','brain_mask']
    
    main( data_dir, cc, threads, work_subdir, performance, nomask, use_hard_segmentation, lobe_list )		