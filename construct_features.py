#!/bin/env python
# Computes ROI-wise features for classification
# 1) mean CBF in GM Hammers regions
# 2) GM volume based on probabilistic segmentations
#
#
# ROI sets
# A) region (nickname: seg)
# B) selection (not constructed here, selected from region in matlab functions)
# C) lobe
# D) hemisphere
# E) brain

# folders:
# gm_mask: Gray Matter segmentation mask for all subjects
# region_mask: Mask for 83 Hammers regions
# region_gm_mask: The mask for 83 Hammers regions for all subjects combined with the GM segmentation mask
# region_gm_cbf: The CBF in the GM in 83 Hammers regions for all subjects 

# Call from other python script with: construct_features.main(input_dir, cc,  threads, work_subdir)
# Or from command line with:  python construct_features.py input_dir

# 13 March 2013
# Esther Bron - e.bron@erasmusmc.nl 

import os
import subprocess
import re
import sys
import datetime
import time
import shutil
import math
import numpy
import operator
from pylab import *
from bigr.clustercontrol import ClusterControl
from optparse import OptionParser

use_cluster = True
check = False

use_hard_segmentation = False
use_mean_probability = False
				
def flatten(x):
    """flatten(sequence) -> list

    Returns a single, flat list which contains all elements retrieved
    from the sequence and all recursively contained sub-sequences
    (iterables).

    Examples:
    >>> [1, 2, [3,4], (5,6)]
    [1, 2, [3, 4], (5, 6)]
    >>> flatten([[[1,2,3], (42,None)], [4,5], [6], 7, MyVector(8,9,10)])
    [1, 2, 3, 42, None, 4, 5, 6, 7, 8, 9, 10]"""

    result = []
    for el in x:
        #if isinstance(el, (list, tuple)):
        if hasattr(el, "__iter__") and not isinstance(el, basestring):
            result.extend(flatten(el))
        else:
            result.append(el)
    return result

def main(data_dir, cc, threads, work_subdir, performance, seg_list):

	# Store data dir
#	data_dir = os.path.abspath(os.path.expanduser(sys.argv[1]))
	print "Data dir: " + data_dir
	t1w_dir = os.path.join( data_dir, 'T1w' )
	cbf_dir = os.path.join( data_dir, 'CBF_pvc' )
	if not os.path.exists(cbf_dir):
		cbf_dir = os.path.join( data_dir, 'CBF' )
	feature_dir = os.path.join( data_dir, 'Features' )
	hammers_dir = os.path.join( data_dir, 'Hammers' )
	scripts_dir=os.path.dirname(os.path.realpath(__file__))
	
	if not os.path.exists( feature_dir ):
		os.mkdir( feature_dir )	
	
	# Read and filter input files
	input_files = [];
	for filename in os.listdir( t1w_dir ):
		if re.match('.*.nii.gz', filename ):
			input_files.append( filename )
			
	input_files.sort()
	
	#for k, filename in enumerate( input_files ):
	#    print( '%03d: %s' % (k, filename ) )
	
	lists = [];		
	for i, filename in enumerate( input_files ):
			(filebase,ext)=os.path.splitext(filename) 
			(filebase,ext)=os.path.splitext(filebase) 
			#(iris, filebase)=filebase.split('_')
			print( '%03d: %s' % (i, filebase ) )
			lists.append( filebase )
	
	#input_files = [input_files[0], input_files[1], input_files[2]]
	#print('Only using %d input files: ' % len( input_files ), input_files )
	
	work_dir = os.path.join(feature_dir, work_subdir)
	
	if not os.path.exists( work_dir ):
		os.mkdir( work_dir )	
		
	# Creating a run_id
	print 'Hashing %s' % sys.argv[0] + os.path.join( data_dir, work_subdir )
	run_id = (sys.argv[0] + os.path.join( data_dir, work_subdir ) + 'saltnpepper' ).__hash__()
	print 'Hash tag for this session: %d' % run_id
	run_id = str( abs( run_id ) )
	run_id = 'feat' +  run_id[0:6]
	
	if (len( sys.argv ) >= 4):
	    force = bool( int( sys.argv[3]  ) )
	    print( 'Force taken from command line: ' + str(force) )
	else:
	    force = False
	
	#force = True
	force_next = False
	
	# Set binaries
	bin_threshold  = 'pxthresholdimage'
	bin_imoperator = 'pxbinaryimageoperator'
	bin_nonzero = 'pxcountnonzerovoxels'
	bin_convert = 'pxcastconvert'
	bin_stat = 'pxstatisticsonimage'
	
	for roi_nr in range(0, len(seg_list)):
		seg = seg_list[roi_nr]
		if seg in ['brain_mask']:
			continue
		elif seg in ['seg']:
			roi='region'
			hammers_txt = os.path.join( scripts_dir, 'Hammers_n30r83', 'Hammers_mith_atlases_structure_names_r83.txt')
		else:
			roi = seg
			hammers_txt  = os.path.join( feature_dir, 'hammers_' + seg + '.txt')
			
		print roi							
		out1_dir = os.path.join ( work_dir, roi + '_mask')
		if not os.path.exists( out1_dir ):
			os.mkdir( out1_dir )
	
		# Step 2: Threshold region mask 
		print('# Step 2: Threshold region mask ')		
		for i in range(0, len( input_files )):
			k =  lists[i] 
			print k
			command_list = []
			
			out_dir = os.path.join ( out1_dir, k)
			if not os.path.exists( out_dir ):
				os.mkdir( out_dir )
			
			f = open(hammers_txt, 'r')
			
			job_id=[]
			
			# Read Hammers objects from file
			for line in f:
				[l, sep, rest]  = line.partition('	')
				
				region_file = os.path.join( hammers_dir, work_subdir, k, seg + '.nii.gz')
				region_mask = os.path.join ( out_dir, str(l) + '.nii.gz')
				
				
				if force or (not os.path.exists( region_mask ) ):
					thresh_command = '%s -in %s -out %s  -inside 1 -outside 0 -t1 %s -t2 %s' % ( bin_threshold, region_file, region_mask, str(l), str(l))
					command_list.append(thresh_command)
	
			# Send jobs		
			if command_list:			
				job_id = cc.send_job_array( command_list, 'day', '00:10:00', run_id)		
	
			f.close()
	
			
		if job_id:	
			# Wait until registrations are all done
			cc.wait(job_id)	
				
				
				
		# Step 3: Combine GM/WM/CSF mask with Hammers regions for each subject 
		print('# Step 3: Combine GM/WM/CSF mask with Hammers regions for each subject')	
	
		if use_hard_segmentation:
			# region_gm_dir = os.path.join ( work_dir, roi + '_gm_mask')
			file_dir =  os.path.join ( work_dir, roi + '_gm_volume')
			output_file = os.path.join ( work_dir, roi + '_gm_volume.txt')	
		elif use_mean_probability:
			file_dir =  os.path.join ( work_dir, roi + '_gm_probabilities')
			output_file = os.path.join ( work_dir, roi + '_gm_probabilities.txt')	
		else:		
			# region_gm_dir = os.path.join ( work_dir, roi + '_probabilistic_gm_mask')
			file_dir =  os.path.join ( work_dir, roi + '_gm_probabilistic_volume')
			output_file = os.path.join ( work_dir, roi + '_gm_probabilistic_volume.txt')
		if not os.path.exists( file_dir ):
			os.mkdir( file_dir )
	
		
		c=0
		if force or (not os.path.exists( output_file ) ):
			for i in range(0, len( input_files )): #Loop over all subjects
				k =  lists[i] 
				print k
				print c
				# gmcount=[]
				if use_hard_segmentation:
					gm_mask = os.path.join ( t1w_dir, 'spm', k + '_gm.nii.gz')
				else:
					gm_mask = os.path.join(  t1w_dir, 'spm', 'wc1w' + k + '.nii.gz' )
				
				output_file_subject = os.path.join ( file_dir, k + '.txt') 
				out_dir = os.path.join ( out1_dir, k)
					
				if force or (not os.path.exists( output_file_subject ) ):	
					f = open(hammers_txt, 'r')
					if ~use_mean_probability:					
						output_string_subject='Subject\tROI Number\tROI name\tVolume\n'
						
						# Calculate intracranial volume
						brain_mask = os.path.join( hammers_dir, work_subdir, k, 'brain_mask.nii.gz')
					
						nonzero_command = '%s -in %s' % ( bin_nonzero, brain_mask)
						icv = os.popen(nonzero_command).read()
						[p1,icv,p3,p4]=icv.split(None);	
												
					else:
						output_string_subject='Subject\tROI Number\tROI name\tMean probability\n'
						
					f = open(hammers_txt, 'r')	
					for line in f:
						[l, sep, region_name]  = line.partition('	')
						[region_name, rest,rest] = region_name.partition('\n')		
						region_mask = os.path.join ( out_dir, str(l) + '.nii.gz')				
						
						if ~use_mean_probability:
							if use_hard_segmentation:
								gm_mask = os.path.join ( t1w_dir, 'spm', k + '_gm.nii.gz')
							else:
								gm_mask = os.path.join(  t1w_dir, 'spm', 'wc1w' + k + '.nii.gz' )											
					
							# region_gm_k_dir = os.path.join ( region_gm_dir, k)
							# region_gm_mask = os.path.join ( region_gm_k_dir, str(l) + '.nii.gz')				
							
							# nonzero_command = '%s -in %s' % ( bin_nonzero, region_gm_mask)
							# nz_gm = os.popen(nonzero_command).read()
							# [p1,nz_gm,p3,p4]=nz_gm.split(None);
							# # gmcount.append(float(nz_gm))
							
							mean_command = '%s -in %s -mask %s' % ( bin_stat, gm_mask, region_mask)
							output = os.popen(mean_command).read()
							[rest, sum] = output.split('sum             : ')
							[sum, x, rest] = sum.partition('\n')
						
							item = float(sum)/float(icv)
							output_string_subject += k + '\t' + l + '\t' + region_name + '\t' + str(item) + '\n'
						else:
							gm_mask = os.path.join(  t1w_dir, 'spm', 'wc1w' + k + '.nii.gz' )
							region_mask = os.path.join ( out_dir, str(l) + '.nii.gz')
							
							mean_command = '%s -in %s -mask %s -s arithmetic' % ( bin_stat, gm_mask, region_mask)
							output = os.popen(mean_command).read()
							
							[rest, global_mean] = output.split('arithmetic mean : ')
							[global_mean, x, rest] = global_mean.partition('\n')
								
							output_string_subject += k + '\t' + l + '\t' + region_name + '\t' + str(global_mean) + '\n'							
							
					print output_string_subject
					f_output = open(output_file_subject, 'w')
					f_output.write(output_string_subject)	
					f_output.close()
					f.close()
	
			output_string=[]		
			for i in range(0, len( input_files )): #Loop over all subjects
				k =  lists[i] 
				print k
				
				output_file_subject = os.path.join ( file_dir, k + '.txt') 
				
				f_output_subject = open(output_file_subject, 'r')
				subject_lines = f_output_subject.readlines()	
				
				output_string += subject_lines
				f_output_subject.close()
					
			output_string = ''.join(output_string)		
			print output_string		
			f_output = open(output_file, 'w')
			f_output.write(output_string)	
			f_output.close()
				
		# Step 4: Calculate mean per ROI
		print('# Step 4: Calculate mean per ROI')
	
		out_dir = os.path.join ( work_dir, roi + '_cbf_values')
		if not os.path.exists( out_dir ):
			os.mkdir( out_dir ) 		
	
		output_file = os.path.join ( work_dir, roi + '_cbf_values_0.txt')
		output_file1 = os.path.join ( work_dir, roi + '_cbf_values_1.txt')
		if os.path.exists( cbf_dir ) and ( force or (not os.path.exists( output_file) )):	
			for i in range(0, len( input_files )): #Loop over all subjects
				k =  lists[i] 
				print k
	
				# mean_array = []
				# std_array = []
				# min_array = []
				# max_array = []
				# median_array = []
				
				# label_array = []
				# region_name_array = []
				
				output_file_subject = os.path.join ( out_dir, k + '.txt') 
					
				output_string_cbf_subject = 'Subject\tROI Number\tROI name\tMean\tStdev\tMin\tMax\tMedian\tNormalized mean\n'
			
				input_image = os.path.join( cbf_dir, work_subdir, 'registrations_cbfmap', k, 'cbf_gm_hard.nii.gz')
				gm_mask = os.path.join ( t1w_dir, 'spm', k + '_gm.nii.gz')
				
				
				region_gm_dir = os.path.join ( work_dir, roi + '_gm_mask')	
				if not os.path.exists(region_gm_dir):
					os.mkdir(region_gm_dir)				
				region_gm_k_dir = os.path.join ( region_gm_dir, k)
				if not os.path.exists(region_gm_k_dir):
					os.mkdir(region_gm_k_dir)	

				command_list = []
				f = open(hammers_txt, 'r')
				if force or (not os.path.exists( output_file_subject) ):							
					for line in f:
						[l, sep, region_name]  = line.partition('	')
						[region_name, rest,rest] = region_name.partition('\n')
						region_mask = os.path.join ( work_dir, roi + '_mask', k, str(l) + '.nii.gz')									
					
						region_gm_mask = os.path.join ( region_gm_k_dir, str(l) + '.nii.gz')
						
						if force or (not os.path.exists( region_gm_mask ) ):
							mask_command = '%s -in %s %s -ops %s -out %s' % ( bin_imoperator, region_mask, gm_mask, 'TIMES', region_gm_mask)
							command_list.append(mask_command)
								
						# Send jobs			 
				if command_list:				
						job_id = cc.send_job_array( command_list, 'day', '00:10:00', run_id)			

						# Wait until registrations are all done
						cc.wait(job_id) 
								
				command_list = []
				f = open(hammers_txt, 'r')
				if force or (not os.path.exists( output_file_subject) ):					
					mean_command = '%s -in %s -mask %s -s arithmetic' % ( bin_stat, input_image, gm_mask)
					output = os.popen(mean_command).read()
					[rest, global_mean] = output.split('arithmetic mean : ')
					[global_mean, x, rest] = global_mean.partition('\n')	
					for line in f:
						[l, sep, region_name]  = line.partition('	')
						[region_name, rest,rest] = region_name.partition('\n')									
					
						region_gm_mask = os.path.join ( region_gm_k_dir, str(l) + '.nii.gz')						
										
						mean_command = '%s -in %s -mask %s -s arithmetic' % ( bin_stat, input_image, region_gm_mask)
						output = os.popen(mean_command).read()						
						[rest, mean] = output.split('arithmetic mean : ')
						[mean, x, rest] = mean.partition('\n')
						if mean == 'nan' or mean == '-nan':
							mean = 0
						norm_mean = float(mean)/float(global_mean)	
						# mean_array.append( float(mean) )
						
						[rest, stdev] = output.split('arithmetic stdev: ')
						[stdev, x, rest] = stdev.partition('\n')
						if stdev == 'nan' or stdev == '-nan':
							stdev = 0
						# stdev_array.append( float(stdev) )
						
						[rest, min] = output.split('min             : ')
						[min, x, rest] = min.partition('\n')
						if min == 'nan' or min == '-nan':
							min = 0
						# stdev_array.append( float(min) )
						
						[rest, max] = output.split('max             : ')
						[max, x, rest] = max.partition('\n')
						if max == 'nan' or max == '-nan':
							max = 0
						# stdev_array.append( float(max) )
	
						mean_command = '%s -in %s -mask %s -s histogram' % ( bin_stat, input_image, region_gm_mask)
						output = os.popen(mean_command).read()
						
						[rest, median] = output.split('median:')
						[median, x, rest] = median.partition('\n')
						if median == 'nan' or median == '-nan':
							median = 0
						# stdev_array.append( float(median) )
						
						# label_array.append( l )
						# regions_name_array.append( region_name )
						
						output_string_cbf_subject += k + '\t' + l + '\t' + region_name + '\t' + str(mean) + '\t' + str(stdev) + '\t' + str(min) + '\t' + str(max) + str(median) + '\t' + str(norm_mean) + '\n'
						
					print output_string_cbf_subject
					f_output = open(output_file_subject, 'w')		
					f_output.write(output_string_cbf_subject)	
					f_output.close()	
					
					f.close()	
	
			output_string_cbf0=[]
			output_string_cbf1=[]
			for i in range(0, len( input_files )): #Loop over all subjects
				k =  lists[i] 
				print k
				
				output_file_subject = os.path.join ( out_dir, k + '.txt') 
				
				f_output_subject = open(output_file_subject, 'r')
				subject_lines = f_output_subject.read()	
				
				
				if i <= len( input_files )/2: 
					output_string_cbf0 += subject_lines
				else:
					output_string_cbf1 += subject_lines
					
			output_string_cbf0 = ''.join(output_string_cbf0)	
			print output_string_cbf0
			output_string_cbf1 = ''.join(output_string_cbf1)	
			print output_string_cbf1
			
			f_output = open(output_file, 'w')		
			a=f_output.write(output_string_cbf0)	
			f_output.close()
	
			f_output = open(output_file1, 'w')		
			a=f_output.write(output_string_cbf1)	
			f_output.close()
			
		if os.path.exists(os.path.join(data_dir, 'DTI')):
			# Step 3: Combine GM/WM/CSF mask with Hammers regions for each subject 
			print('# Step 3: Combine GM/WM/CSF mask with Hammers regions for each subject')	
		
			if use_hard_segmentation:
				# region_gm_dir = os.path.join ( work_dir, roi + '_gm_mask')
				file_dir =  os.path.join ( work_dir, roi + '_wm_volume')
				output_file = os.path.join ( work_dir, roi + '_wm_volume.txt')	
			elif use_mean_probability:
				file_dir =  os.path.join ( work_dir, roi + '_wm_probabilities')
				output_file = os.path.join ( work_dir, roi + '_wm_probabilities.txt')	
			else:		
				# region_gm_dir = os.path.join ( work_dir, roi + '_probabilistic_gm_mask')
				file_dir =  os.path.join ( work_dir, roi + '_wm_probabilistic_volume')
				output_file = os.path.join ( work_dir, roi + '_wm_probabilistic_volume.txt')
			if not os.path.exists( file_dir ):
				os.mkdir( file_dir )
		
			
			c=0
			if force or (not os.path.exists( output_file ) ):
				for i in range(0, len( input_files )): #Loop over all subjects
					k =  lists[i] 
					print k
					print c
					# gmcount=[]
					if use_hard_segmentation:
						gm_mask = os.path.join ( t1w_dir, 'spm', k + '_wm.nii.gz')
					else:
						gm_mask = os.path.join(  t1w_dir, 'spm', 'wc2w' + k + '.nii.gz' )
					
					output_file_subject = os.path.join ( file_dir, k + '.txt') 
					out_dir = os.path.join ( out1_dir, k)
						
					if force or (not os.path.exists( output_file_subject ) ):	
						f = open(hammers_txt, 'r')
						if ~use_mean_probability:					
							output_string_subject='Subject\tROI Number\tROI name\tVolume\n'
							
							# Calculate intracranial volume
							brain_mask = os.path.join( hammers_dir, work_subdir, k, 'brain_mask.nii.gz')
						
							nonzero_command = '%s -in %s' % ( bin_nonzero, brain_mask)
							icv = os.popen(nonzero_command).read()
							[p1,icv,p3,p4]=icv.split(None);	
													
						else:
							output_string_subject='Subject\tROI Number\tROI name\tMean probability\n'
							
						f = open(hammers_txt, 'r')	
						for line in f:
							[l, sep, region_name]  = line.partition('	')
							[region_name, rest,rest] = region_name.partition('\n')		
							region_mask = os.path.join ( out_dir, str(l) + '.nii.gz')				
							
							if ~use_mean_probability:
								if use_hard_segmentation:
									gm_mask = os.path.join ( t1w_dir, 'spm', k + '_wm.nii.gz')
								else:
									gm_mask = os.path.join(  t1w_dir, 'spm', 'wc2w' + k + '.nii.gz' )											
						
								# region_gm_k_dir = os.path.join ( region_gm_dir, k)
								# region_gm_mask = os.path.join ( region_gm_k_dir, str(l) + '.nii.gz')				
								
								# nonzero_command = '%s -in %s' % ( bin_nonzero, region_gm_mask)
								# nz_gm = os.popen(nonzero_command).read()
								# [p1,nz_gm,p3,p4]=nz_gm.split(None);
								# # gmcount.append(float(nz_gm))
								
								mean_command = '%s -in %s -mask %s' % ( bin_stat, gm_mask, region_mask)
								output = os.popen(mean_command).read()
								
								[rest, sum] = output.split('sum             : ')
								[sum, x, rest] = sum.partition('\n')
							
								item = float(sum)/float(icv)
								output_string_subject += k + '\t' + l + '\t' + region_name + '\t' + str(item) + '\n'
							else:
								gm_mask = os.path.join(  t1w_dir, 'spm', 'wc2w' + k + '.nii.gz' )
								region_mask = os.path.join ( out_dir, str(l) + '.nii.gz')
								
								mean_command = '%s -in %s -mask %s -s arithmetic' % ( bin_stat, gm_mask, region_mask)
								output = os.popen(mean_command).read()
								
								[rest, global_mean] = output.split('arithmetic mean : ')
								[global_mean, x, rest] = global_mean.partition('\n')
									
								output_string_subject += k + '\t' + l + '\t' + region_name + '\t' + str(global_mean) + '\n'							
								
						print output_string_subject
						f_output = open(output_file_subject, 'w')
						f_output.write(output_string_subject)	
						f_output.close()
						f.close()
		
				output_string=[]		
				for i in range(0, len( input_files )): #Loop over all subjects
					k =  lists[i] 
					print k
					
					output_file_subject = os.path.join ( file_dir, k + '.txt') 
					
					f_output_subject = open(output_file_subject, 'r')
					subject_lines = f_output_subject.readlines()	
					
					output_string += subject_lines
					f_output_subject.close()
						
				output_string = ''.join(output_string)		
				print output_string		
				f_output = open(output_file, 'w')
				f_output.write(output_string)	
				f_output.close()
				

if __name__ == '__main__':
	# Parse input arguments
	parser = OptionParser(description="Compute ROI-wise features for classification", usage="Usage: python %prog input_dir. Use option -h for help information.")
	(options, args) = parser.parse_args()

	if len(args) != 1:
		parser.error("wrong number of arguments")
		
	data_dir=args[0]	
		
	cc = ClusterControl()	
	work_subdir ='atlas_work_temp';
	threads=1
	performance=False
	lobe_list=['seg','brain']
	
	main(data_dir, cc, threads, work_subdir, performance, lobe_list)		