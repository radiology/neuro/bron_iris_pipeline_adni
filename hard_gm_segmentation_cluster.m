function hard_gm_segmentation_cluster(rootDirectory, do_wm)
do_wm=str2num(do_wm);
remain=ls(fullfile(rootDirectory, 'T1w', '*.nii.gz'))
force=1;

% remain=dir(fullfile(rootDirectory, 'T1w'))

allSubjects={};
while isempty(remain)==0
	[file1, remain] = strtok(remain);
    allSubjects{numel(allSubjects)+1}=file1;
end

% remain.name
% allSubjects=remain.name

for i=1:numel(allSubjects)
    [dir, subjectName]=fileparts(allSubjects{i});
    subjectName=strtok(subjectName,'.');
    display(['Performing hard segmentation for ' subjectName])

	s=size(subjectName,2);
    outputFile= fullfile( rootDirectory,'T1w', 'spm', [subjectName '_gm.nii.gz']);
    outputFile_wm= fullfile( rootDirectory,'T1w', 'spm', [subjectName '_wm.nii.gz']);
    if exist(outputFile,'file')~=2 || (exist(outputFile_wm,'file')~=2 && do_wm)
        hardSegmentation_subject(rootDirectory, subjectName, subjectName,'do_wm',do_wm);      
    else        
        display([subjectName ' has been processed before. Skipped.'])
    end
       
end