% List of open inputs
% Normalise: Estimate & Write: Source Image - cfg_files
% Normalise: Estimate & Write: Images to Write - cfg_files
% Deformations: Image to base inverse on - cfg_files
inputdir=pwd
file=[dir(fullfile(inputdir,'T1w','spm','*.nii'))]
file.name
nrun = length(file) % enter the number of runs here
jobfile = {fullfile(fileparts(which(run_batch_segment_cuingnet_standalone)),'run_batch_segment_iris_cluster_job.m')};
% Make sure that the template in run_batch_segment_iris_cluster_job.m are looked for in the correct SPM folder

jobs = repmat(jobfile, 1, nrun);
inputs = cell(3, nrun);
for crun = 1:nrun
    inputs{1, crun} = {[inputdir, filesep,'T1w', filesep, 'spm', filesep, strtrim(file(crun,:).name)]}; % Normalise: Estimate & Write: Source Image - cfg_files
    inputs{2, crun} = {[inputdir, filesep,'T1w', filesep, 'spm', filesep, strtrim(file(crun,:).name)]}; % Normalise: Estimate & Write: Images to Write - cfg_files
    inputs{3, crun} = {[inputdir, filesep,'T1w', filesep, 'spm', filesep, strtrim(file(crun,:).name)]}; % Deformations: Image to base inverse on - cfg_files
end
jobs{1}
inputs{:,1}

spm('defaults', 'PET')
spm_jobman('initcfg');
spm_jobman('serial', jobs, '', inputs{:});

